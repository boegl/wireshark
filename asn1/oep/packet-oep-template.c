/* c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
/* packet-oep.c
 * Routines for OEP packet dissection
 *
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
# include "config.h"

#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include <glib.h>
#include <epan/packet.h>
#include <epan/asn1.h>
#include <epan/prefs.h>
#include <epan/conversation.h>

#include <asn1.h>

#include "packet-per.h"
#include "packet-ber.h"
#include "packet-mder.h"
#include "packet-oep.h"

#define PNAME  "Optimized Exchange Protocol"
#define PSNAME "OEP"
#define PFNAME "oep"
#define PORT_OEP 5001	/* TCP port */

/* Initialize the protocol and registered fields */
static gint proto_oep = -1;
static gint oep_port = PORT_OEP;

void proto_reg_handoff_oep(void);

static gint ett_oep = -1;

static const value_string oep_Apdu_vals[] = {
  { 57856, "Association Request" },
  { 58112, "Association Response" },
  { 58368, "Association Release Request" },
  { 58624, "Association Release Response" },
  { 58880, "Association Abort" },
  { 59136, "Presentation PDU" },
  { 59392, "Association Trigger" },
  { 0, NULL }
};

/* Initialize the oep and registered fields */
#include "packet-oep-hf.c"

#include "packet-oep-val.h"

/* Initialize the subtree pointers */
#include "packet-oep-ett.c"

#include "packet-oep-fn.c"

static void
dissect_oep(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree) {
    proto_item *item = NULL;
    proto_tree *oep_tree = NULL;
    asn1_ctx_t asn1_ctx;
    asn1_ctx_init(&asn1_ctx, ASN1_ENC_MDER, TRUE, pinfo);
    
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PSNAME);
    /* Clear out stuff in the info column */
    col_clear(pinfo->cinfo,COL_INFO);

    if (parent_tree) { /* we are being asked for details */
        gint offset = 0;
        gint length = 1;
        
        item = proto_tree_add_item(parent_tree, proto_oep, tvb, 0, -1, ENC_NA);
        oep_tree = proto_item_add_subtree(item, ett_oep);
    } 
    dissect_ApduType_PDU(tvb, pinfo, oep_tree);
    
}

/*--- proto_register_oep ----------------------------------------------*/
void proto_register_oep(void) {

    module_t *oep_module;
    
  /* List of fields */
  static hf_register_info hf[] = {
  #include "packet-oep-hfarr.c"
  };

  /* List of subtrees */
  static gint *ett[] = {
    &ett_oep,
#include "packet-oep-ettarr.c"
  };

  /* Register protocol */
  proto_oep = proto_register_protocol(PNAME, PSNAME, PFNAME);

  /* Register fields and subtrees */
  proto_register_field_array(proto_oep, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));

  oep_module = prefs_register_protocol(proto_oep, proto_reg_handoff_oep);
  prefs_register_uint_preference(oep_module, "tcp.port",
                                   "TCP Port",
                                   "Set the TCP port for OEP",
                                   10,
                                   &oep_port);
}


/*--- proto_reg_handoff_oep -------------------------------------------*/
void proto_reg_handoff_oep(void) {

    static gboolean initialized = FALSE;

    static int port = 0;

    static dissector_handle_t oep_handle;

    if (initialized)
    {
        dissector_delete_uint("tcp.port", port, oep_handle);
    }
    else
    {
        oep_handle = create_dissector_handle(dissect_oep, proto_oep);
        initialized = TRUE;
    }

    port = oep_port;
    dissector_add_uint("tcp.port", port, oep_handle);
}

/*
 * Editor modelines  -  http://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
 