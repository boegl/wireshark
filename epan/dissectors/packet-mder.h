/* c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
 /* packet-mder.h
 * Helpers for ASN.1/MDER dissection
 * Sebastian Boegl (C) 2013
 * Originally created for BER by Ronnie Sahlmderg in 2004
 *
 * $Id$
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __PACKET_MDER_H__
#define __PACKET_MDER_H__

#include <epan/proto.h>
#include <epan/to_str.h>
#include <epan/asn1.h>
#include "ws_symbol_export.h"
#include <epan/dissectors/packet-ber.h> /* define asn_namedbit */

#define MDER_NOT_DECODED_YET(x) \
proto_tree_add_text(tree, tvb, offset, 0, "something unknown here [%s]",x); \
fprintf(stderr,"Not decoded yet in packet : %d  [%s]\n", pinfo->fd->num,x); \
col_append_fstr(pinfo->cinfo, COL_INFO, "[UNKNOWN MDER: %s]", x); \
tvb_get_guint8(tvb, 9999);

/* value for value and size constraints */
#ifndef NO_BOUND
#define NO_BOUND -1
#endif

typedef int (*mder_callback)(gboolean imp_tag, tvbuff_t *tvb, int offset, asn1_ctx_t *actx, proto_tree *tree, int hf_index);
typedef int (*mder_type_fn)(gboolean, tvbuff_t*, int, asn1_ctx_t *actx, proto_tree*, int);

typedef enum Size 
{/* length of bit in byte */
		BIT_8 = 1,
		BIT_16 = 2,
		BIT_24 = 3,
		BIT_32 = 4,
		BIT_64 = 8
} Size;

#define MDER_CLASS_UNI					0
#define MDER_CLASS_CON					2
#define MDER_CLASS_ANY   				99	/* dont check class nor tag */
#define MDER_UNI_TAG_INTEGER			2
#define MDER_UNI_TAG_BITSTRING			3
#define MDER_UNI_TAG_OCTETSTRING		4
#define MDER_UNI_TAG_NULL				5
#define MDER_UNI_TAG_SEQUENCE			16	/* SEQUENCE, SEQUENCE OF */
#define MDNF_FLOAT_NAN					8388607	/* 0x007FFFFF */
#define MDNF_FLOAT_NRES					8388608	/* 0x00800000 */
#define MDNF_FLOAT_INFINITY_PLUS		8388606	/* 0x007FFFFE */
#define MDNF_FLOAT_INFINITY_MINUS		8388610	/* 0x00800002 */
#define MDNF_FLOAT_RESERVED				8388609	/* 0x00800001 */
#define MDNF_SFLOAT_NAN					2047	/* 0x07FF */
#define MDNF_SFLOAT_NRES				2048	/* 0x0800 */
#define MDNF_SFLOAT_INFINITY_PLUS		2046	/* 0x07FE */
#define MDNF_SFLOAT_INFINITY_MINUS		2050	/* 0x0802 */
#define MDNF_SFLOAT_RESERVED			2049	/* 0x0801 */

WS_DLL_PUBLIC int get_mder_length(tvbuff_t *tvb, int offset, guint32 *length);
WS_DLL_PUBLIC int get_mder_tag(tvbuff_t *tvb, int offset, guint32 *tag);
WS_DLL_PUBLIC int get_mder_count(tvbuff_t *tvb, int offset, guint32 *count);
WS_DLL_PUBLIC int get_mder_intu32(tvbuff_t *tvb, int offset, guint32 *value);
WS_DLL_PUBLIC int get_mder_intu16(tvbuff_t *tvb, int offset, guint32 *value);
WS_DLL_PUBLIC int dissect_mder_length(proto_tree *tree, tvbuff_t *tvb, int offset, guint32 *length);
WS_DLL_PUBLIC int dissect_mder_tag(proto_tree *tree, tvbuff_t *tvb, int offset, guint32 *tag);
WS_DLL_PUBLIC int dissect_mder_count(proto_tree *tree, tvbuff_t *tvb, int offset, guint32 *count);

WS_DLL_PUBLIC int dissect_mder_octet_string(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, tvbuff_t **out_tvb);
WS_DLL_PUBLIC int dissect_mder_octet_string_size(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, tvbuff_t **out_tvb, guint32 *size);
WS_DLL_PUBLIC int dissect_mder_octet_string_length(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint32 *length);

WS_DLL_PUBLIC int dissect_mder_integer64(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint64 *value, Size size);
WS_DLL_PUBLIC int dissect_mder_intu32(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint32 *value);
WS_DLL_PUBLIC int dissect_mder_intu16(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint16 *value);
WS_DLL_PUBLIC int dissect_mder_intu8(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint8 *value);
WS_DLL_PUBLIC int dissect_mder_inti32(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint32 *value);
WS_DLL_PUBLIC int dissect_mder_inti16(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint16 *value);
WS_DLL_PUBLIC int dissect_mder_inti8(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint8 *value);
extern int dissect_mder_constrained_integer64(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint64 min_len, gint64 max_len, gint hf_id, gint64 *value);

WS_DLL_PUBLIC int dissect_mder_integer(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint32 *value);
extern int dissect_mder_constrained_integer(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint32 min_len, gint32 max_len, gint hf_id, guint32 *value);

WS_DLL_PUBLIC int dissect_mder_float(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gdouble *value);
WS_DLL_PUBLIC int dissect_mder_sfloat(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gfloat *value);

WS_DLL_PUBLIC int dissect_mder_null(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id);

#define MDER_FLAGS_OPTIONAL	0x00000001
#define MDER_FLAGS_IMPLTAG	0x00000002
#define MDER_FLAGS_NOOWNTAG	0x00000004
#define MDER_FLAGS_NOTCHKTAG	0x00000008
typedef struct _mder_sequence_t {
	const int *p_id;
	gint8	mder_class;
	gint32	tag;
	guint32	flags;
	mder_callback	func;
} mder_sequence_t;
/*
 * This function dissects a MDER sequence
 */
WS_DLL_PUBLIC int dissect_mder_sequence(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_sequence_t *seq, gint hf_id, gint ett_id);

typedef struct _mder_choice_t {
	guint32	value;
	const int *p_id;
	gint8	mder_class;
	gint32	tag;
	guint32	flags;
	mder_callback	func;
} mder_choice_t;
/*
 * This function dissects a MDER choice
 */
WS_DLL_PUBLIC int dissect_mder_choice(asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_choice_t *choice, gint hf_id, gint ett_id, gint *branch_taken);
WS_DLL_PUBLIC int dissect_mder_any(asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_choice_t *choice, gint hf_id, gint ett_id, gint *branch_taken);

/* this function dissects a MDER sequence of
 */
WS_DLL_PUBLIC int dissect_mder_sequence_of(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_sequence_t *seq, gint hf_id, gint ett_id);

/* this function dissects a MDER BIT-STRING
 */
WS_DLL_PUBLIC int dissect_mder_bitstring(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb);
WS_DLL_PUBLIC int dissect_mder_bitstring_size(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb, Size size);
WS_DLL_PUBLIC int dissect_mder_bitstring8(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb);
WS_DLL_PUBLIC int dissect_mder_bitstring16(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb);
WS_DLL_PUBLIC int dissect_mder_bitstring32(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb);

WS_DLL_PUBLIC
int call_mder_oid_callback(const char *oid, tvbuff_t *tvb, int offset, packet_info *pinfo, proto_tree *tree);
WS_DLL_PUBLIC
void register_mder_oid_dissector_handle(const char *oid, dissector_handle_t dissector, int proto, const char *name);
WS_DLL_PUBLIC
void register_mder_oid_dissector(const char *oid, dissector_t dissector, int proto, const char *name);
WS_DLL_PUBLIC
void register_mder_syntax_dissector(const char *oid, int proto, dissector_t dissector);
void register_mder_oid_name(const char *oid, const char *name);
WS_DLL_PUBLIC
void register_mder_oid_syntax(const char *oid, const char *name, const char *syntax);
void dissect_mder_oid_NULL_callback(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);

WS_DLL_PUBLIC
void mder_decode_as_foreach(GHFunc func, gpointer user_data); /* iterate through known syntaxes */
WS_DLL_PUBLIC
void mder_decode_as(const gchar *syntax); /* decode the current capture as this syntax */
WS_DLL_PUBLIC
void mder_set_filename(gchar *filename); /* name of current MDER-encoded file */

#endif  /* __PACKET_MDER_H__ */

/*
 * Editor modelines  -  http://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
