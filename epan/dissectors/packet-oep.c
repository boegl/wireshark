/* Do not modify this file. Changes will be overwritten.                      */
/* Generated automatically by the ASN.1 to Wireshark dissector compiler       */
/* packet-oep.c                                                               */
/* ../../tools/asn2wrs.py -b -p oep -c ./oep.cnf -s ./packet-oep-template -D . -O ../../epan/dissectors IEEE_20601_d20_defs.asn */

/* Input file: packet-oep-template.c */

#line 1 "../../asn1/oep/packet-oep-template.c"
/* c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
/* packet-oep.c
 * Routines for OEP packet dissection
 *
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
# include "config.h"

#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include <glib.h>
#include <epan/packet.h>
#include <epan/asn1.h>
#include <epan/prefs.h>
#include <epan/conversation.h>

#include <asn1.h>

#include "packet-per.h"
#include "packet-ber.h"
#include "packet-mder.h"
#include "packet-oep.h"

#define PNAME  "Optimized Exchange Protocol"
#define PSNAME "OEP"
#define PFNAME "oep"
#define PORT_OEP 5001	/* TCP port */

/* Initialize the protocol and registered fields */
static gint proto_oep = -1;
static gint oep_port = PORT_OEP;

void proto_reg_handoff_oep(void);

static gint ett_oep = -1;

static const value_string oep_Apdu_vals[] = {
  { 57856, "Association Request" },
  { 58112, "Association Response" },
  { 58368, "Association Release Request" },
  { 58624, "Association Release Response" },
  { 58880, "Association Abort" },
  { 59136, "Presentation PDU" },
  { 59392, "Association Trigger" },
  { 0, NULL }
};

/* Initialize the oep and registered fields */

/*--- Included file: packet-oep-hf.c ---*/
#line 1 "../../asn1/oep/packet-oep-hf.c"
static int hf_oep_ApduType_PDU = -1;              /* ApduType */
static int hf_oep_partition = -1;                 /* NomPartition */
static int hf_oep_code = -1;                      /* OID_Type */
static int hf_oep_attribute_id = -1;              /* AVAType_attributeId */
static int hf_oep_attribute_value = -1;           /* AVAAttributeValue */
static int hf_oep_confirmMode = -1;               /* ConfirmMode */
static int hf_oep_confirmTimeout = -1;            /* RelativeTime */
static int hf_oep_idHandle = -1;                  /* HANDLE */
static int hf_oep_idInstNumber = -1;              /* InstNumber */
static int hf_oep_idLabelString = -1;             /* OCTET_STRING */
static int hf_oep_idModel = -1;                   /* SystemModel */
static int hf_oep_idPhysio = -1;                  /* OID_Type */
static int hf_oep_idProdSpecn = -1;               /* ProductionSpec */
static int hf_oep_idType = -1;                    /* TYPE */
static int hf_oep_metricStoreUsageCount = -1;     /* INT_U32 */
static int hf_oep_measurementStatus = -1;         /* MeasurementStatus */
static int hf_oep_nuAccurMsmt = -1;               /* FLOAT_Type */
static int hf_oep_nuObsValueCmp = -1;             /* NuObsValueCmp */
static int hf_oep_nuObsValue = -1;                /* NuObsValue */
static int hf_oep_segmentNumber = -1;             /* INT_U16 */
static int hf_oep_operationalState = -1;          /* OperationalState */
static int hf_oep_powerStatus = -1;               /* PowerStatus */
static int hf_oep_saSpec = -1;                    /* SaSpec */
static int hf_oep_scaleRangeSpec16 = -1;          /* ScaleRangeSpec16 */
static int hf_oep_scaleRangeSpec32 = -1;          /* ScaleRangeSpec32 */
static int hf_oep_scaleRangeSpec8 = -1;           /* ScaleRangeSpec8 */
static int hf_oep_scanRepPD = -1;                 /* RelativeTime */
static int hf_oep_segmentUsageCount = -1;         /* INT_U32 */
static int hf_oep_systemId = -1;                  /* OCTET_STRING */
static int hf_oep_systemType = -1;                /* TYPE */
static int hf_oep_absoluteTime = -1;              /* AbsoluteTime */
static int hf_oep_batMeasure = -1;                /* BatMeasure */
static int hf_oep_endTimeSegment = -1;            /* AbsoluteTime */
static int hf_oep_pdTimeSample = -1;              /* RelativeTime */
static int hf_oep_relativeTime = -1;              /* RelativeTime */
static int hf_oep_timeStampAbsolute = -1;         /* AbsoluteTime */
static int hf_oep_timeStampRelative = -1;         /* RelativeTime */
static int hf_oep_startTimeSegment = -1;          /* AbsoluteTime */
static int hf_oep_transmitWIndow = -1;            /* INT_U16 */
static int hf_oep_unitCode = -1;                  /* OID_Type */
static int hf_oep_unitLable = -1;                 /* OCTET_STRING */
static int hf_oep_batChargeValue = -1;            /* INT_U16 */
static int hf_oep_enumObsValue = -1;              /* EnumObsValue */
static int hf_oep_highResRelativeTime = -1;       /* HighResRelativeTime */
static int hf_oep_timeStampRelativeHighRes = -1;  /* HighResRelativeTime */
static int hf_oep_authChallenge = -1;             /* OCTET_STRING */
static int hf_oep_authResponse = -1;              /* OCTET_STRING */
static int hf_oep_configId = -1;                  /* ConfigId */
static int hf_oep_mdsTimeInfo = -1;               /* MdsTimeInfo */
static int hf_oep_metricSpecSmall = -1;           /* MetricSpecSmall */
static int hf_oep_sourceHandleReference = -1;     /* HANDLE */
static int hf_oep_simpleSaObsValue = -1;          /* OCTET_STRING */
static int hf_oep_observedValueSimpleOID = -1;    /* OID_Type */
static int hf_oep_observedValueSimpleString = -1;  /* EnumPrintableString */
static int hf_oep_regCertDataList = -1;           /* RegCertDataList */
static int hf_oep_basicNuObsValue = -1;           /* BasicNuObsValue */
static int hf_oep_pmSegmentEntryMap = -1;         /* PmSegmentEntryMap */
static int hf_oep_personId = -1;                  /* PersonId */
static int hf_oep_segmentStatistics = -1;         /* SegmentStatistics */
static int hf_oep_scanHandleAttrValMap = -1;      /* HandleAttrValMap */
static int hf_oep_scanRepPDMin = -1;              /* RelativeTime */
static int hf_oep_attrValMap = -1;                /* AttrValMap */
static int hf_oep_simpleNuObsValue = -1;          /* SimpleNuObsValue */
static int hf_oep_pmStoreLabel = -1;              /* OCTET_STRING */
static int hf_oep_pmSegmentLabel = -1;            /* OCTET_STRING */
static int hf_oep_pdTimeMsmtActive = -1;          /* FLOAT_Type */
static int hf_oep_systemTypeVerList = -1;         /* TypeVerList */
static int hf_oep_metricIdPartition = -1;         /* NomPartition */
static int hf_oep_observedValuePartition = -1;    /* NomPartition */
static int hf_oep_supplementalTypeList = -1;      /* SupplementalTypeList */
static int hf_oep_absoluteTimeAdjust = -1;        /* AbsoluteTimeAdjust */
static int hf_oep_clearTimeout = -1;              /* RelativeTime */
static int hf_oep_transferTimeout = -1;           /* RelativeTime */
static int hf_oep_observedValueSimple = -1;       /* BITS_32 */
static int hf_oep_observedValueBasic = -1;        /* BITS_16 */
static int hf_oep_metricStructureSmall = -1;      /* MetricStructureSmall */
static int hf_oep_simpleNuObsValueCmp = -1;       /* SimpleNuObsValueCmp */
static int hf_oep_basicNuObsValueCmp = -1;        /* BasicNuObsValueCmp */
static int hf_oep_idPhysioList = -1;              /* MetricIdList */
static int hf_oep_scanHandleList = -1;            /* HANDLEList */
static int hf_oep_AttributeList_item = -1;        /* AVA_Type */
static int hf_oep_AttributeIdList_item = -1;      /* OID_Type */
static int hf_oep_century = -1;                   /* INT_U8 */
static int hf_oep_year = -1;                      /* INT_U8 */
static int hf_oep_month = -1;                     /* INT_U8 */
static int hf_oep_day = -1;                       /* INT_U8 */
static int hf_oep_hour = -1;                      /* INT_U8 */
static int hf_oep_minute = -1;                    /* INT_U8 */
static int hf_oep_second = -1;                    /* INT_U8 */
static int hf_oep_sec_fractions = -1;             /* INT_U8 */
static int hf_oep_bo_seconds = -1;                /* INT_U32 */
static int hf_oep_bo_fraction = -1;               /* INT_U16 */
static int hf_oep_bo_time_offset = -1;            /* INT_I16 */
static int hf_oep_manufacturer = -1;              /* OCTET_STRING */
static int hf_oep_model_number = -1;              /* OCTET_STRING */
static int hf_oep_ProductionSpec_item = -1;       /* ProdSpecEntry */
static int hf_oep_prodSpecEntry_specType = -1;    /* ProdSpecEntry_specType */
static int hf_oep_component_id = -1;              /* PrivateOid */
static int hf_oep_prod_spec = -1;                 /* OCTET_STRING */
static int hf_oep_batMeasure_value = -1;          /* FLOAT_Type */
static int hf_oep_unit = -1;                      /* OID_Type */
static int hf_oep_metric_id = -1;                 /* OID_Type */
static int hf_oep_state = -1;                     /* MeasurementStatus */
static int hf_oep_unit_code = -1;                 /* OID_Type */
static int hf_oep_nuObsValue_value = -1;          /* FLOAT_Type */
static int hf_oep_NuObsValueCmp_item = -1;        /* NuObsValue */
static int hf_oep_array_size = -1;                /* INT_U16 */
static int hf_oep_sample_type = -1;               /* SampleType */
static int hf_oep_flags = -1;                     /* SaFlags */
static int hf_oep_sample_size = -1;               /* INT_U8 */
static int hf_oep_sampleType_significantBits = -1;  /* SampleType_significantBits */
static int hf_oep_lower_absolute_value = -1;      /* FLOAT_Type */
static int hf_oep_upper_absolute_value = -1;      /* FLOAT_Type */
static int hf_oep_scaleRangeSpec8_lowerScaledValue = -1;  /* INT_U8 */
static int hf_oep_scaleRangeSpec8_upperScaledValue = -1;  /* INT_U8 */
static int hf_oep_scaleRangeSpec16_lowerScaledValue = -1;  /* INT_U16 */
static int hf_oep_scaleRangeSpec16_upperScaledValue = -1;  /* INT_U16 */
static int hf_oep_scaleRangeSpec32_lowerScaledValue = -1;  /* INT_U32 */
static int hf_oep_scaleRangeSpec32_upperScaledValue = -1;  /* INT_U32 */
static int hf_oep_enumObsValue_value = -1;        /* EnumVal */
static int hf_oep_enum_obj_id = -1;               /* OID_Type */
static int hf_oep_enum_text_string = -1;          /* OCTET_STRING */
static int hf_oep_enum_bit_str = -1;              /* BITS_16 */
static int hf_oep_setTimeInvoke_dateTime = -1;    /* AbsoluteTime */
static int hf_oep_accuracy = -1;                  /* FLOAT_Type */
static int hf_oep_setBOTimeInvoke_dateTime = -1;  /* BaseOffsetTime */
static int hf_oep_all_segments = -1;              /* INT_U16 */
static int hf_oep_segm_id_list = -1;              /* SegmIdList */
static int hf_oep_abs_time_range = -1;            /* AbsTimeRange */
static int hf_oep_bo_time_range = -1;             /* BOTimeRange */
static int hf_oep_SegmIdList_item = -1;           /* InstNumber */
static int hf_oep_absTimeRange_fromTime = -1;     /* AbsoluteTime */
static int hf_oep_absTimeRange_toTime = -1;       /* AbsoluteTime */
static int hf_oep_bOTimeRange_fromTime = -1;      /* BaseOffsetTime */
static int hf_oep_bOTimeRange_toTime = -1;        /* BaseOffsetTime */
static int hf_oep_SegmentInfoList_item = -1;      /* SegmentInfo */
static int hf_oep_seg_inst_no = -1;               /* InstNumber */
static int hf_oep_seg_info = -1;                  /* AttributeList */
static int hf_oep_obj_handle = -1;                /* HANDLE */
static int hf_oep_attributes = -1;                /* AttributeList */
static int hf_oep_aarq = -1;                      /* AarqApdu */
static int hf_oep_aare = -1;                      /* AareApdu */
static int hf_oep_rlrq = -1;                      /* RlrqApdu */
static int hf_oep_rlre = -1;                      /* RlreApdu */
static int hf_oep_abrt = -1;                      /* AbrtApdu */
static int hf_oep_prst = -1;                      /* PrstApdu */
static int hf_oep_atrg = -1;                      /* AtrgApdu */
static int hf_oep_assoc_version = -1;             /* AssociationVersion */
static int hf_oep_data_proto_list = -1;           /* DataProtoList */
static int hf_oep_DataProtoList_item = -1;        /* DataProto */
static int hf_oep_data_proto_id = -1;             /* DataProtoId */
static int hf_oep_data_proto_info = -1;           /* DataProtoInfo */
static int hf_oep_phdAssociationInformation = -1;  /* PhdAssociationInformation */
static int hf_oep_manufSpecAssociationInformation = -1;  /* ManufSpecAssociationInformation */
static int hf_oep_empty = -1;                     /* NULL */
static int hf_oep_result = -1;                    /* AssociateResult */
static int hf_oep_selected_data_proto = -1;       /* DataProto */
static int hf_oep_rlrqApdu_reason = -1;           /* ReleaseRequestReason */
static int hf_oep_rlreApdu_reason = -1;           /* ReleaseResponseReason */
static int hf_oep_abrtApdu_reason = -1;           /* Abort_reason */
static int hf_oep_protocol_version = -1;          /* ProtocolVersion */
static int hf_oep_encoding_rules = -1;            /* EncodingRules */
static int hf_oep_nomenclature_version = -1;      /* NomenclatureVersion */
static int hf_oep_functional_units = -1;          /* FunctionalUnits */
static int hf_oep_system_type = -1;               /* SystemType */
static int hf_oep_system_id = -1;                 /* OCTET_STRING */
static int hf_oep_dev_config_id = -1;             /* ConfigId */
static int hf_oep_data_req_mode_capab = -1;       /* DataReqModeCapab */
static int hf_oep_option_list = -1;               /* AttributeList */
static int hf_oep_data_proto_id_ext = -1;         /* UuidIdent */
static int hf_oep_data_proto_info_ext = -1;       /* UuidIdent */
static int hf_oep_optionList = -1;                /* AttributeList */
static int hf_oep_invoke_id = -1;                 /* InvokeIDType */
static int hf_oep_dataApdu_message = -1;          /* DataApdu_message */
static int hf_oep_roiv_cmip_event_report = -1;    /* EventReportArgumentSimple */
static int hf_oep_roiv_cmip_confirmed_event_report = -1;  /* EventReportArgumentSimple */
static int hf_oep_roiv_cmip_get = -1;             /* GetArgumentSimple */
static int hf_oep_roiv_cmip_set = -1;             /* SetArgumentSimple */
static int hf_oep_roiv_cmip_confirmed_set = -1;   /* SetArgumentSimple */
static int hf_oep_roiv_cmip_action = -1;          /* ActionArgumentSimple */
static int hf_oep_roiv_cmip_confirmed_action = -1;  /* ActionArgumentSimple */
static int hf_oep_rors_cmip_confirmed_event_report = -1;  /* EventReportResultSimple */
static int hf_oep_rors_cmip_get = -1;             /* GetResultSimple */
static int hf_oep_rors_cmip_confirmed_set = -1;   /* SetResultSimple */
static int hf_oep_rors_cmip_confirmed_action = -1;  /* ActionResultSimple */
static int hf_oep_roer = -1;                      /* ErrorResult */
static int hf_oep_rorj = -1;                      /* RejectResult */
static int hf_oep_error_value = -1;               /* RoerErrorValue */
static int hf_oep_parameter = -1;                 /* RoerErrorValue */
static int hf_oep_problem = -1;                   /* RorjProblem */
static int hf_oep_event_time = -1;                /* RelativeTime */
static int hf_oep_eventReportArgumentSimple_eventType = -1;  /* EventReportArgumentSimplet_eventType */
static int hf_oep_eventReportArgumentSimple_eventInfo = -1;  /* EventReportArgumentSimple_eventInfo */
static int hf_oep_configReport = -1;              /* ConfigReport */
static int hf_oep_scanReportInfoFixed = -1;       /* ScanReportInfoFixed */
static int hf_oep_scanReportInfoVar = -1;         /* ScanReportInfoVar */
static int hf_oep_scanReportInfoMPFixed = -1;     /* ScanReportInfoMPFixed */
static int hf_oep_scanReportInfoMPVar = -1;       /* ScanReportInfoMPVar */
static int hf_oep_segmentDataEvent = -1;          /* SegmentDataEvent */
static int hf_oep_currentTime = -1;               /* RelativeTime */
static int hf_oep_eventReportResultSimple_eventType = -1;  /* EventReportResultSimple_eventType */
static int hf_oep_event_reply_info = -1;          /* EventReplyInfo */
static int hf_oep_configReportRsp = -1;           /* ConfigReportRsp */
static int hf_oep_segmentDataResult = -1;         /* SegmentDataResult */
static int hf_oep_attribute_id_list = -1;         /* AttributeIdList */
static int hf_oep_attribute_list = -1;            /* AttributeList */
static int hf_oep_TypeVerList_item = -1;          /* TypeVer */
static int hf_oep_type = -1;                      /* OID_Type */
static int hf_oep_version = -1;                   /* INT_U16 */
static int hf_oep_modification_list = -1;         /* ModificationList */
static int hf_oep_ModificationList_item = -1;     /* AttributeModEntry */
static int hf_oep_modify_operator = -1;           /* ModifyOperator */
static int hf_oep_attribute = -1;                 /* AVA_Type */
static int hf_oep_action_type = -1;               /* OID_Type */
static int hf_oep_actionArgumentSimple_actionInfoArgs = -1;  /* ActionArgumentSimple_actionInfoArgs */
static int hf_oep_dataRequest = -1;               /* DataRequest */
static int hf_oep_setTimeInvoke = -1;             /* SetTimeInvoke */
static int hf_oep_setBOTimeInvoke = -1;           /* SetBOTimeInvoke */
static int hf_oep_segmSelection = -1;             /* SegmSelection */
static int hf_oep_trigSegmDataXferReq = -1;       /* TrigSegmDataXferReq */
static int hf_oep_actionResultSimple_actionInfoArgs = -1;  /* ActionResultSimple_actionInfoArgs */
static int hf_oep_dataResponse = -1;              /* DataResponse */
static int hf_oep_segmentInfoList = -1;           /* SegmentInfoList */
static int hf_oep_trigSegmDataXferRsp = -1;       /* TrigSegmDataXferRsp */
static int hf_oep_AttrValMap_item = -1;           /* AttrValMapEntry */
static int hf_oep_attribute_len = -1;             /* INT_U16 */
static int hf_oep_mds_time_cap_state = -1;        /* MdsTimeCapState */
static int hf_oep_time_sync_protocol = -1;        /* TimeProtocolId */
static int hf_oep_time_sync_accuracy = -1;        /* RelativeTime */
static int hf_oep_time_resolution_abs_time = -1;  /* INT_U16 */
static int hf_oep_time_resolution_rel_time = -1;  /* INT_U16 */
static int hf_oep_time_resolution_high_res_time = -1;  /* INT_U32 */
static int hf_oep_RegCertDataList_item = -1;      /* RegCertData */
static int hf_oep_auth_body_and_struc_type = -1;  /* AuthBodyAndStrucType */
static int hf_oep_auth_body_data = -1;            /* AuthBodyAndStrucType */
static int hf_oep_auth_body = -1;                 /* AuthBody */
static int hf_oep_auth_body_struc_type = -1;      /* AuthBodyStrucType */
static int hf_oep_SupplementalTypeList_item = -1;  /* TYPE */
static int hf_oep_metricStructureSmall_msStruct = -1;  /* MetricStructureSmall_msStruct */
static int hf_oep_ms_comp_no = -1;                /* INT_U8 */
static int hf_oep_MetricIdList_item = -1;         /* OID_Type */
static int hf_oep_HandleAttrValMap_item = -1;     /* HandleAttrValMapEntry */
static int hf_oep_attr_val_map = -1;              /* AttrValMap */
static int hf_oep_HANDLEList_item = -1;           /* HANDLE */
static int hf_oep_scanReportInfoVar_dataReqId = -1;  /* DataReqId */
static int hf_oep_scan_report_no = -1;            /* INT_U16 */
static int hf_oep_obs_scan_var = -1;              /* SEQUENCE_OF_ObservationScan */
static int hf_oep_obs_scan_var_item = -1;         /* ObservationScan */
static int hf_oep_scanReportInfoFixed_dataReqId = -1;  /* DataReqId */
static int hf_oep_obs_scan_fixed = -1;            /* SEQUENCE_OF_ObservationScanFixed */
static int hf_oep_obs_scan_fixed_item = -1;       /* ObservationScanFixed */
static int hf_oep_obs_val_data = -1;              /* ObservationValueData */
static int hf_oep_observed_val = -1;              /* SimpleNuObsValue */
static int hf_oep_absolute_time = -1;             /* AbsoluteTime */
static int hf_oep_scanReportInfoGrouped_dataReqId = -1;  /* DataReqId */
static int hf_oep_scanReportInfoGrouped_obsScanGrouped = -1;  /* SEQUENCE_OF_ObservationScanGrouped */
static int hf_oep_scanReportInfoGrouped_obsScanGrouped_item = -1;  /* ObservationScanGrouped */
static int hf_oep_scanReportInfoMPVar_dataReqId = -1;  /* DataReqId */
static int hf_oep_scan_per_var = -1;              /* SEQUENCE_OF_ScanReportPerVar */
static int hf_oep_scan_per_var_item = -1;         /* ScanReportPerVar */
static int hf_oep_person_id = -1;                 /* PersonId */
static int hf_oep_scanReportInfoMPFixed_dataReqId = -1;  /* DataReqId */
static int hf_oep_scan_per_fixed = -1;            /* SEQUENCE_OF_ScanReportPerFixed */
static int hf_oep_scan_per_fixed_item = -1;       /* ScanReportPerFixed */
static int hf_oep_scanReportInfoMPGrouped_dataReqId = -1;  /* DataReqId */
static int hf_oep_scan_per_grouped = -1;          /* SEQUENCE_OF_ScanReportPerGrouped */
static int hf_oep_scan_per_grouped_item = -1;     /* ScanReportPerGrouped */
static int hf_oep_scanReportPerGrouped_obsScanGrouped = -1;  /* ObservationScanGrouped */
static int hf_oep_config_report_id = -1;          /* ConfigId */
static int hf_oep_config_obj_list = -1;           /* ConfigObjectList */
static int hf_oep_ConfigObjectList_item = -1;     /* ConfigObject */
static int hf_oep_obj_class = -1;                 /* ConfigObject_objClass */
static int hf_oep_config_result = -1;             /* ConfigResult */
static int hf_oep_dataRequest_dataReqId = -1;     /* DataReqId */
static int hf_oep_data_req_mode = -1;             /* DataReqMode */
static int hf_oep_data_req_time = -1;             /* RelativeTime */
static int hf_oep_data_req_person_id = -1;        /* INT_U16 */
static int hf_oep_data_req_class = -1;            /* OID_Type */
static int hf_oep_data_req_obj_handle_list = -1;  /* HANDLEList */
static int hf_oep_data_req_mode_flags = -1;       /* DataReqModeFlags */
static int hf_oep_data_req_init_agent_count = -1;  /* INT_U8 */
static int hf_oep_data_req_init_manager_count = -1;  /* INT_U8 */
static int hf_oep_rel_time_stamp = -1;            /* RelativeTime */
static int hf_oep_data_req_result = -1;           /* DataReqResult */
static int hf_oep_event_type = -1;                /* DateResponse_eventType */
static int hf_oep_dataResponse_eventInfo = -1;    /* DataResponse_eventInfo */
static int hf_oep_SimpleNuObsValueCmp_item = -1;  /* SimpleNuObsValue */
static int hf_oep_BasicNuObsValueCmp_item = -1;   /* BasicNuObsValue */
static int hf_oep_segm_entry_header = -1;         /* SegmEntryHeader */
static int hf_oep_segm_entry_elem_list = -1;      /* SegmEntryElemList */
static int hf_oep_SegmEntryElemList_item = -1;    /* SegmEntryElem */
static int hf_oep_class_id = -1;                  /* OID_Type */
static int hf_oep_metric_type = -1;               /* TYPE */
static int hf_oep_handle = -1;                    /* HANDLE */
static int hf_oep_trig_segm_xfer_rsp = -1;        /* TrigSegmXferRsp */
static int hf_oep_segm_data_event_descr = -1;     /* SegmDataEventDescr */
static int hf_oep_segm_data_event_entries = -1;   /* OCTET_STRING */
static int hf_oep_segm_instance = -1;             /* InstNumber */
static int hf_oep_segm_evt_entry_index = -1;      /* INT_U32 */
static int hf_oep_segm_evt_entry_count = -1;      /* INT_U32 */
static int hf_oep_segm_evt_status = -1;           /* SegmEvtStatus */
static int hf_oep_SegmentStatistics_item = -1;    /* SegmentStatisticEntry */
static int hf_oep_segm_stat_type = -1;            /* SegmStatType */
static int hf_oep_segm_stat_entry = -1;           /* OCTET_STRING */
/* named bits */
static int hf_oep_PowerStatus_onMains = -1;
static int hf_oep_PowerStatus_onBattery = -1;
static int hf_oep_PowerStatus_chargingFull = -1;
static int hf_oep_PowerStatus_chargingTrickle = -1;
static int hf_oep_PowerStatus_chargingOff = -1;
static int hf_oep_MeasurementStatus_invalid = -1;
static int hf_oep_MeasurementStatus_questionable = -1;
static int hf_oep_MeasurementStatus_not_available = -1;
static int hf_oep_MeasurementStatus_calibration_ongoing = -1;
static int hf_oep_MeasurementStatus_test_data = -1;
static int hf_oep_MeasurementStatus_demo_data = -1;
static int hf_oep_MeasurementStatus_validated_data = -1;
static int hf_oep_MeasurementStatus_early_indication = -1;
static int hf_oep_MeasurementStatus_msmt_ongoing = -1;
static int hf_oep_SaFlags_smooth_curve = -1;
static int hf_oep_SaFlags_delayed_curve = -1;
static int hf_oep_SaFlags_static_scale = -1;
static int hf_oep_SaFlags_sa_ext_val_range = -1;
static int hf_oep_AssociationVersion_assoc_version1 = -1;
static int hf_oep_ProtocolVersion_protocol_version1 = -1;
static int hf_oep_ProtocolVersion_protocol_version2 = -1;
static int hf_oep_EncodingRules_mder = -1;
static int hf_oep_EncodingRules_xer = -1;
static int hf_oep_EncodingRules_per = -1;
static int hf_oep_NomenclatureVersion_nom_version1 = -1;
static int hf_oep_FunctionalUnits_fun_units_unidirectional = -1;
static int hf_oep_FunctionalUnits_fun_units_havetestcap = -1;
static int hf_oep_FunctionalUnits_fun_units_createtestassoc = -1;
static int hf_oep_SystemType_sys_type_manager = -1;
static int hf_oep_SystemType_sys_type_agent = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_real_time_clock = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_set_clock = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_relative_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_high_res_relative_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_sync_abs_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_sync_rel_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_sync_hi_res_relative_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_bo_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_state_abs_time_synced = -1;
static int hf_oep_MdsTimeCapState_mds_time_state_rel_time_synced = -1;
static int hf_oep_MdsTimeCapState_mds_time_state_hi_res_relative_time_synced = -1;
static int hf_oep_MdsTimeCapState_mds_time_mgr_set_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_capab_sync_bo_time = -1;
static int hf_oep_MdsTimeCapState_mds_time_state_bo_time_synced = -1;
static int hf_oep_MdsTimeCapState_mds_time_state_bo_time_UTC_aligned = -1;
static int hf_oep_MetricSpecSmall_mss_avail_intermittent = -1;
static int hf_oep_MetricSpecSmall_mss_avail_stored_data = -1;
static int hf_oep_MetricSpecSmall_mss_upd_aperiodic = -1;
static int hf_oep_MetricSpecSmall_mss_msmt_aperiodic = -1;
static int hf_oep_MetricSpecSmall_mss_msmt_phys_ev_id = -1;
static int hf_oep_MetricSpecSmall_mss_msmt_btb_metric = -1;
static int hf_oep_MetricSpecSmall_mss_acc_manager_initiated = -1;
static int hf_oep_MetricSpecSmall_mss_acc_agent_initiated = -1;
static int hf_oep_MetricSpecSmall_mss_cat_manual = -1;
static int hf_oep_MetricSpecSmall_mss_cat_setting = -1;
static int hf_oep_MetricSpecSmall_mss_cat_calculation = -1;
static int hf_oep_DataReqMode_data_req_start_stop = -1;
static int hf_oep_DataReqMode_data_req_continuation = -1;
static int hf_oep_DataReqMode_data_req_scope_all = -1;
static int hf_oep_DataReqMode_data_req_scope_class = -1;
static int hf_oep_DataReqMode_data_req_scope_handle = -1;
static int hf_oep_DataReqMode_data_req_mode_single_rsp = -1;
static int hf_oep_DataReqMode_data_req_mode_time_period = -1;
static int hf_oep_DataReqMode_data_req_mode_time_no_limit = -1;
static int hf_oep_DataReqMode_data_req_person_id = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_stop = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_scope_all = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_scope_class = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_scope_handle = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_mode_single_rsp = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_mode_time_period = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_mode_time_no_limit = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_person_id = -1;
static int hf_oep_DataReqModeFlags_data_req_supp_init_agent = -1;
static int hf_oep_PmStoreCapab_pmsc_var_no_of_segm = -1;
static int hf_oep_PmStoreCapab_pmsc_segm_id_list_select = -1;
static int hf_oep_PmStoreCapab_pmsc_epi_seg_entries = -1;
static int hf_oep_PmStoreCapab_pmsc_peri_seg_entries = -1;
static int hf_oep_PmStoreCapab_pmsc_abs_time_select = -1;
static int hf_oep_PmStoreCapab_pmsc_clear_segm_by_list_sup = -1;
static int hf_oep_PmStoreCapab_pmsc_clear_segm_by_time_sup = -1;
static int hf_oep_PmStoreCapab_pmsc_clear_segm_remove = -1;
static int hf_oep_PmStoreCapab_pmsc_clear_segm_all_sup = -1;
static int hf_oep_PmStoreCapab_pmsc_multi_person = -1;
static int hf_oep_SegmEntryHeader_seg_elem_hdr_absolute_time = -1;
static int hf_oep_SegmEntryHeader_seg_elem_hdr_relative_time = -1;
static int hf_oep_SegmEntryHeader_seg_elem_hdr_hires_relative_time = -1;
static int hf_oep_SegmEntryHeader_seg_elem_hdr_bo_time = -1;
static int hf_oep_SegmEvtStatus_sevtsta_first_entry = -1;
static int hf_oep_SegmEvtStatus_sevtsta_last_entry = -1;
static int hf_oep_SegmEvtStatus_sevtsta_agent_abort = -1;
static int hf_oep_SegmEvtStatus_sevtsta_manager_confirm = -1;
static int hf_oep_SegmEvtStatus_sevtsta_manager_abort = -1;

/*--- End of included file: packet-oep-hf.c ---*/
#line 72 "../../asn1/oep/packet-oep-template.c"


/*--- Included file: packet-oep-val.h ---*/
#line 1 "../../asn1/oep/packet-oep-val.h"

/*--- End of included file: packet-oep-val.h ---*/
#line 74 "../../asn1/oep/packet-oep-template.c"

/* Initialize the subtree pointers */

/*--- Included file: packet-oep-ett.c ---*/
#line 1 "../../asn1/oep/packet-oep-ett.c"
static gint ett_oep_TYPE = -1;
static gint ett_oep_AVA_Type = -1;
static gint ett_oep_AVAAttributeValue = -1;
static gint ett_oep_AttributeList = -1;
static gint ett_oep_AttributeIdList = -1;
static gint ett_oep_AbsoluteTime = -1;
static gint ett_oep_BaseOffsetTime = -1;
static gint ett_oep_SystemModel = -1;
static gint ett_oep_ProductionSpec = -1;
static gint ett_oep_ProdSpecEntry = -1;
static gint ett_oep_PowerStatus = -1;
static gint ett_oep_BatMeasure = -1;
static gint ett_oep_MeasurementStatus = -1;
static gint ett_oep_NuObsValue = -1;
static gint ett_oep_NuObsValueCmp = -1;
static gint ett_oep_SaSpec = -1;
static gint ett_oep_SampleType = -1;
static gint ett_oep_SaFlags = -1;
static gint ett_oep_ScaleRangeSpec8 = -1;
static gint ett_oep_ScaleRangeSpec16 = -1;
static gint ett_oep_ScaleRangeSpec32 = -1;
static gint ett_oep_EnumObsValue = -1;
static gint ett_oep_EnumVal = -1;
static gint ett_oep_SetTimeInvoke = -1;
static gint ett_oep_SetBOTimeInvoke = -1;
static gint ett_oep_SegmSelection = -1;
static gint ett_oep_SegmIdList = -1;
static gint ett_oep_AbsTimeRange = -1;
static gint ett_oep_BOTimeRange = -1;
static gint ett_oep_SegmentInfoList = -1;
static gint ett_oep_SegmentInfo = -1;
static gint ett_oep_ObservationScan = -1;
static gint ett_oep_ApduType = -1;
static gint ett_oep_AarqApdu = -1;
static gint ett_oep_DataProtoList = -1;
static gint ett_oep_DataProto = -1;
static gint ett_oep_DataProtoInfo = -1;
static gint ett_oep_AareApdu = -1;
static gint ett_oep_RlrqApdu = -1;
static gint ett_oep_RlreApdu = -1;
static gint ett_oep_AbrtApdu = -1;
static gint ett_oep_PhdAssociationInformation = -1;
static gint ett_oep_ManufSpecAssociationInformation = -1;
static gint ett_oep_AssociationVersion = -1;
static gint ett_oep_ProtocolVersion = -1;
static gint ett_oep_EncodingRules = -1;
static gint ett_oep_NomenclatureVersion = -1;
static gint ett_oep_FunctionalUnits = -1;
static gint ett_oep_SystemType = -1;
static gint ett_oep_AtrgApdu = -1;
static gint ett_oep_DataApdu = -1;
static gint ett_oep_DataApdu_message = -1;
static gint ett_oep_ErrorResult = -1;
static gint ett_oep_RejectResult = -1;
static gint ett_oep_EventReportArgumentSimple = -1;
static gint ett_oep_EventReportInfo = -1;
static gint ett_oep_EventReportResultSimple = -1;
static gint ett_oep_EventReplyInfo = -1;
static gint ett_oep_GetArgumentSimple = -1;
static gint ett_oep_GetResultSimple = -1;
static gint ett_oep_TypeVerList = -1;
static gint ett_oep_TypeVer = -1;
static gint ett_oep_SetArgumentSimple = -1;
static gint ett_oep_ModificationList = -1;
static gint ett_oep_AttributeModEntry = -1;
static gint ett_oep_SetResultSimple = -1;
static gint ett_oep_ActionArgumentSimple = -1;
static gint ett_oep_ActionArgumentSimpleInfoArgs = -1;
static gint ett_oep_ActionResultSimple = -1;
static gint ett_oep_ActionResultSimpleInfoArgs = -1;
static gint ett_oep_AttrValMap = -1;
static gint ett_oep_AttrValMapEntry = -1;
static gint ett_oep_MdsTimeInfo = -1;
static gint ett_oep_MdsTimeCapState = -1;
static gint ett_oep_RegCertDataList = -1;
static gint ett_oep_RegCertData = -1;
static gint ett_oep_AuthBodyAndStrucType = -1;
static gint ett_oep_SupplementalTypeList = -1;
static gint ett_oep_MetricSpecSmall = -1;
static gint ett_oep_MetricStructureSmall = -1;
static gint ett_oep_MetricIdList = -1;
static gint ett_oep_HandleAttrValMap = -1;
static gint ett_oep_HandleAttrValMapEntry = -1;
static gint ett_oep_HANDLEList = -1;
static gint ett_oep_ScanReportInfoVar = -1;
static gint ett_oep_SEQUENCE_OF_ObservationScan = -1;
static gint ett_oep_ScanReportInfoFixed = -1;
static gint ett_oep_SEQUENCE_OF_ObservationScanFixed = -1;
static gint ett_oep_ObservationScanFixed = -1;
static gint ett_oep_ObservationValueData = -1;
static gint ett_oep_ScanReportInfoGrouped = -1;
static gint ett_oep_SEQUENCE_OF_ObservationScanGrouped = -1;
static gint ett_oep_ScanReportInfoMPVar = -1;
static gint ett_oep_SEQUENCE_OF_ScanReportPerVar = -1;
static gint ett_oep_ScanReportPerVar = -1;
static gint ett_oep_ScanReportInfoMPFixed = -1;
static gint ett_oep_SEQUENCE_OF_ScanReportPerFixed = -1;
static gint ett_oep_ScanReportPerFixed = -1;
static gint ett_oep_ScanReportInfoMPGrouped = -1;
static gint ett_oep_SEQUENCE_OF_ScanReportPerGrouped = -1;
static gint ett_oep_ScanReportPerGrouped = -1;
static gint ett_oep_ConfigReport = -1;
static gint ett_oep_ConfigObjectList = -1;
static gint ett_oep_ConfigObject = -1;
static gint ett_oep_ConfigReportRsp = -1;
static gint ett_oep_DataRequest = -1;
static gint ett_oep_DataReqMode = -1;
static gint ett_oep_DataReqModeCapab = -1;
static gint ett_oep_DataReqModeFlags = -1;
static gint ett_oep_DataResponse = -1;
static gint ett_oep_DataResponseEventInfo = -1;
static gint ett_oep_SimpleNuObsValueCmp = -1;
static gint ett_oep_BasicNuObsValueCmp = -1;
static gint ett_oep_PmStoreCapab = -1;
static gint ett_oep_PmSegmentEntryMap = -1;
static gint ett_oep_SegmEntryHeader = -1;
static gint ett_oep_SegmEntryElemList = -1;
static gint ett_oep_SegmEntryElem = -1;
static gint ett_oep_TrigSegmDataXferReq = -1;
static gint ett_oep_TrigSegmDataXferRsp = -1;
static gint ett_oep_SegmentDataEvent = -1;
static gint ett_oep_SegmentDataResult = -1;
static gint ett_oep_SegmDataEventDescr = -1;
static gint ett_oep_SegmEvtStatus = -1;
static gint ett_oep_SegmentStatistics = -1;
static gint ett_oep_SegmentStatisticEntry = -1;

/*--- End of included file: packet-oep-ett.c ---*/
#line 77 "../../asn1/oep/packet-oep-template.c"


/*--- Included file: packet-oep-fn.c ---*/
#line 1 "../../asn1/oep/packet-oep-fn.c"


static int
dissect_oep_INT_U8(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 314 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_intu8(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_INT_I8(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 323 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_inti8(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_INT_U16(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 317 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_intu16(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_INT_I16(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 326 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_inti16(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_INT_U32(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 320 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_intu32(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_INT_I32(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 329 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_inti32(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_BITS_8(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 332 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring8(implicit_tag, actx, tree, tvb, offset, NULL, hf_index, -1, NULL);


  return offset;
}



static int
dissect_oep_BITS_16(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 335 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, NULL, hf_index, -1, NULL);


  return offset;
}



static int
dissect_oep_BITS_32(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 338 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring32(implicit_tag, actx, tree, tvb, offset, NULL, hf_index, -1, NULL);


  return offset;
}



static int
dissect_oep_OID_Type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_INT_U16(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}



static int
dissect_oep_PrivateOid(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_INT_U16(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}



static int
dissect_oep_HANDLE(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_INT_U16(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}



static int
dissect_oep_InstNumber(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_INT_U16(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const value_string oep_NomPartition_vals[] = {
  {   0, "nom-part-unspec" },
  {   1, "nom-part-obj" },
  {   2, "nom-part-metric" },
  {   3, "nom-part-alert" },
  {   4, "nom-part-dim" },
  {   5, "nom-part-vattr" },
  {   6, "nom-part-pgrp" },
  {   7, "nom-part-sites" },
  {   8, "nom-part-infrastruct" },
  {   9, "nom-part-fef" },
  {  10, "nom-part-ecg-extn" },
  {  11, "nom-part-idco-extn" },
  { 128, "nom-part-phd-dm" },
  { 129, "nom-part-phd-hf" },
  { 130, "nom-part-phd-ai" },
  { 255, "nom-part-ret-code" },
  { 256, "nom-part-ext-nom" },
  { 1024, "nom-part-priv" },
  { 0, NULL }
};


static int
dissect_oep_NomPartition(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t TYPE_sequence[] = {
  { &hf_oep_partition       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_NomPartition },
  { &hf_oep_code            , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_TYPE(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   TYPE_sequence, hf_index, ett_oep_TYPE);

  return offset;
}



static int
dissect_oep_AVAType_attributeId(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_OID_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


const value_string oep_ConfirmMode_vals[] = {
  {   0, "unconfirmed" },
  {   1, "confirmed" },
  { 0, NULL }
};


int
dissect_oep_ConfirmMode(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_oep_RelativeTime(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 379 "../../asn1/oep/oep.cnf"
    guint32            val;
    int                start_offset = offset;
    
    offset = get_mder_intu32(tvb, offset, &val);
    if (hf_index > 0) {
        if (tree) {
            if (val == (guint32)0xFFFFFFFF) {
                proto_registrar_get_nth(hf_index)->type = FT_STRING;
                actx->created_item = proto_tree_add_string(tree, hf_index, tvb, start_offset, offset - start_offset, "relative time clock not supported");
            } else {
                proto_registrar_get_nth(hf_index)->type = FT_UINT32;
                actx->created_item = proto_tree_add_uint(tree, hf_index, tvb, start_offset, offset - start_offset, val);
            }
        }
    }


  return offset;
}



static int
dissect_oep_OCTET_STRING(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const mder_sequence_t SystemModel_sequence[] = {
  { &hf_oep_manufacturer    , MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_OCTET_STRING },
  { &hf_oep_model_number    , MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_SystemModel(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SystemModel_sequence, hf_index, ett_oep_SystemModel);

  return offset;
}


static const value_string oep_ProdSpecEntry_specType_vals[] = {
  {   0, "unspecified" },
  {   1, "serial-number" },
  {   2, "part-number" },
  {   3, "hw-revision" },
  {   4, "sw-revision" },
  {   5, "fw-revision" },
  {   6, "protocol-revision" },
  {   7, "prod-spec-gmdn" },
  { 0, NULL }
};


static int
dissect_oep_ProdSpecEntry_specType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t ProdSpecEntry_sequence[] = {
  { &hf_oep_prodSpecEntry_specType, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ProdSpecEntry_specType },
  { &hf_oep_component_id    , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_PrivateOid },
  { &hf_oep_prod_spec       , MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ProdSpecEntry(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ProdSpecEntry_sequence, hf_index, ett_oep_ProdSpecEntry);

  return offset;
}


static const mder_sequence_t ProductionSpec_sequence_of[1] = {
  { &hf_oep_ProductionSpec_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ProdSpecEntry },
};

int
dissect_oep_ProductionSpec(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      ProductionSpec_sequence_of, hf_index, ett_oep_ProductionSpec);

  return offset;
}


static const asn_namedbit MeasurementStatus_bits[] = {
  {  0, &hf_oep_MeasurementStatus_invalid, -1, -1, "invalid", NULL },
  {  1, &hf_oep_MeasurementStatus_questionable, -1, -1, "questionable", NULL },
  {  2, &hf_oep_MeasurementStatus_not_available, -1, -1, "not-available", NULL },
  {  3, &hf_oep_MeasurementStatus_calibration_ongoing, -1, -1, "calibration-ongoing", NULL },
  {  4, &hf_oep_MeasurementStatus_test_data, -1, -1, "test-data", NULL },
  {  5, &hf_oep_MeasurementStatus_demo_data, -1, -1, "demo-data", NULL },
  {  8, &hf_oep_MeasurementStatus_validated_data, -1, -1, "validated-data", NULL },
  {  9, &hf_oep_MeasurementStatus_early_indication, -1, -1, "early-indication", NULL },
  { 10, &hf_oep_MeasurementStatus_msmt_ongoing, -1, -1, "msmt-ongoing", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_MeasurementStatus(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 271 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     MeasurementStatus_bits, hf_index, ett_oep_MeasurementStatus, NULL);


  return offset;
}



static int
dissect_oep_FLOAT_Type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 341 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_float(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}


static const mder_sequence_t NuObsValue_sequence[] = {
  { &hf_oep_metric_id       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_state           , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_MeasurementStatus },
  { &hf_oep_unit_code       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_nuObsValue_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_NuObsValue(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   NuObsValue_sequence, hf_index, ett_oep_NuObsValue);

  return offset;
}


static const mder_sequence_t NuObsValueCmp_sequence_of[1] = {
  { &hf_oep_NuObsValueCmp_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_NuObsValue },
};

int
dissect_oep_NuObsValueCmp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      NuObsValueCmp_sequence_of, hf_index, ett_oep_NuObsValueCmp);

  return offset;
}


const value_string oep_OperationalState_vals[] = {
  {   0, "disabled" },
  {   1, "enabled" },
  {   2, "notAvailable" },
  { 0, NULL }
};


int
dissect_oep_OperationalState(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const asn_namedbit PowerStatus_bits[] = {
  {  0, &hf_oep_PowerStatus_onMains, -1, -1, "onMains", NULL },
  {  1, &hf_oep_PowerStatus_onBattery, -1, -1, "onBattery", NULL },
  {  8, &hf_oep_PowerStatus_chargingFull, -1, -1, "chargingFull", NULL },
  {  9, &hf_oep_PowerStatus_chargingTrickle, -1, -1, "chargingTrickle", NULL },
  { 10, &hf_oep_PowerStatus_chargingOff, -1, -1, "chargingOff", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

int
dissect_oep_PowerStatus(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 267 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     PowerStatus_bits, hf_index, ett_oep_PowerStatus, NULL);


  return offset;
}


static const value_string oep_SampleType_significantBits_vals[] = {
  { 255, "signed-samples" },
  { 0, NULL }
};


static int
dissect_oep_SampleType_significantBits(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t SampleType_sequence[] = {
  { &hf_oep_sample_size     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_sampleType_significantBits, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_SampleType_significantBits },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SampleType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SampleType_sequence, hf_index, ett_oep_SampleType);

  return offset;
}


static const asn_namedbit SaFlags_bits[] = {
  {  0, &hf_oep_SaFlags_smooth_curve, -1, -1, "smooth-curve", NULL },
  {  1, &hf_oep_SaFlags_delayed_curve, -1, -1, "delayed-curve", NULL },
  {  2, &hf_oep_SaFlags_static_scale, -1, -1, "static-scale", NULL },
  {  3, &hf_oep_SaFlags_sa_ext_val_range, -1, -1, "sa-ext-val-range", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_SaFlags(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 275 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     SaFlags_bits, hf_index, ett_oep_SaFlags, NULL);


  return offset;
}


static const mder_sequence_t SaSpec_sequence[] = {
  { &hf_oep_array_size      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_sample_type     , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SampleType },
  { &hf_oep_flags           , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_SaFlags },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_SaSpec(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SaSpec_sequence, hf_index, ett_oep_SaSpec);

  return offset;
}


static const mder_sequence_t ScaleRangeSpec16_sequence[] = {
  { &hf_oep_lower_absolute_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_upper_absolute_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_scaleRangeSpec16_lowerScaledValue, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_scaleRangeSpec16_upperScaledValue, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_ScaleRangeSpec16(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScaleRangeSpec16_sequence, hf_index, ett_oep_ScaleRangeSpec16);

  return offset;
}


static const mder_sequence_t ScaleRangeSpec32_sequence[] = {
  { &hf_oep_lower_absolute_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_upper_absolute_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_scaleRangeSpec32_lowerScaledValue, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U32 },
  { &hf_oep_scaleRangeSpec32_upperScaledValue, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U32 },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_ScaleRangeSpec32(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScaleRangeSpec32_sequence, hf_index, ett_oep_ScaleRangeSpec32);

  return offset;
}


static const mder_sequence_t ScaleRangeSpec8_sequence[] = {
  { &hf_oep_lower_absolute_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_upper_absolute_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_scaleRangeSpec8_lowerScaledValue, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_scaleRangeSpec8_upperScaledValue, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_ScaleRangeSpec8(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScaleRangeSpec8_sequence, hf_index, ett_oep_ScaleRangeSpec8);

  return offset;
}


static const mder_sequence_t AbsoluteTime_sequence[] = {
  { &hf_oep_century         , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_year            , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_month           , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_day             , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_hour            , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_minute          , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_second          , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_sec_fractions   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_AbsoluteTime(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 355 "../../asn1/oep/oep.cnf"
    gchar century, year, month, day, hour, minute, second, sec_fraction;
    int start_offset = offset;
    century      = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    year         = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    month        = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    day          = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    hour         = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    minute       = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    second       = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    sec_fraction = (gchar)tvb_get_guint8(tvb, offset);
    offset++;
    proto_registrar_get_nth(hf_index)->type = FT_STRING;
    proto_tree_add_string_format_value(tree, hf_index, tvb, start_offset, offset - start_offset, "", 
    "%02X%02X-%02X-%02XT%02X:%02X:%04X", century, year, month, 
    day, hour, minute, second, sec_fraction);


  return offset;
}


static const mder_sequence_t BatMeasure_sequence[] = {
  { &hf_oep_batMeasure_value, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { &hf_oep_unit            , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_BatMeasure(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   BatMeasure_sequence, hf_index, ett_oep_BatMeasure);

  return offset;
}


static const value_string oep_EnumVal_vals[] = {
  {   1, "enum-obj-id" },
  {   2, "enum-text-string" },
  {  16, "enum-bit-str" },
  { 0, NULL }
};

static const mder_choice_t EnumVal_choice[] = {
  {   1, &hf_oep_enum_obj_id     , MDER_CLASS_CON, 1, 0, dissect_oep_OID_Type },
  {   2, &hf_oep_enum_text_string, MDER_CLASS_CON, 2, 0, dissect_oep_OCTET_STRING },
  {  16, &hf_oep_enum_bit_str    , MDER_CLASS_CON, 16, 0, dissect_oep_BITS_16 },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_EnumVal(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_choice(actx, tree, tvb, offset,
                                 EnumVal_choice, hf_index, ett_oep_EnumVal,
                                 NULL);

  return offset;
}


static const mder_sequence_t EnumObsValue_sequence[] = {
  { &hf_oep_metric_id       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_state           , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_MeasurementStatus },
  { &hf_oep_enumObsValue_value, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_EnumVal },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_EnumObsValue(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EnumObsValue_sequence, hf_index, ett_oep_EnumObsValue);

  return offset;
}



static int
dissect_oep_HighResRelativeTime(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 291 "../../asn1/oep/oep.cnf"
guint32 size = 8;
offset = dissect_mder_octet_string_size(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL, &size);


  return offset;
}


static const value_string oep_ConfigId_vals[] = {
  {   0, "manager-config-response" },
  {   1, "standard-config-start" },
  { 16383, "standard-config-end" },
  { 16384, "extended-config-start" },
  { 32767, "extended-config-end" },
  { 32768, "reserved-start" },
  { 65535, "reserved-end" },
  { 0, NULL }
};


static int
dissect_oep_ConfigId(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const asn_namedbit MdsTimeCapState_bits[] = {
  {  0, &hf_oep_MdsTimeCapState_mds_time_capab_real_time_clock, -1, -1, "mds-time-capab-real-time-clock", NULL },
  {  1, &hf_oep_MdsTimeCapState_mds_time_capab_set_clock, -1, -1, "mds-time-capab-set-clock", NULL },
  {  2, &hf_oep_MdsTimeCapState_mds_time_capab_relative_time, -1, -1, "mds-time-capab-relative-time", NULL },
  {  3, &hf_oep_MdsTimeCapState_mds_time_capab_high_res_relative_time, -1, -1, "mds-time-capab-high-res-relative-time", NULL },
  {  4, &hf_oep_MdsTimeCapState_mds_time_capab_sync_abs_time, -1, -1, "mds-time-capab-sync-abs-time", NULL },
  {  5, &hf_oep_MdsTimeCapState_mds_time_capab_sync_rel_time, -1, -1, "mds-time-capab-sync-rel-time", NULL },
  {  6, &hf_oep_MdsTimeCapState_mds_time_capab_sync_hi_res_relative_time, -1, -1, "mds-time-capab-sync-hi-res-relative-time", NULL },
  {  7, &hf_oep_MdsTimeCapState_mds_time_capab_bo_time, -1, -1, "mds-time-capab-bo-time", NULL },
  {  8, &hf_oep_MdsTimeCapState_mds_time_state_abs_time_synced, -1, -1, "mds-time-state-abs-time-synced", NULL },
  {  9, &hf_oep_MdsTimeCapState_mds_time_state_rel_time_synced, -1, -1, "mds-time-state-rel-time-synced", NULL },
  { 10, &hf_oep_MdsTimeCapState_mds_time_state_hi_res_relative_time_synced, -1, -1, "mds-time-state-hi-res-relative-time-synced", NULL },
  { 11, &hf_oep_MdsTimeCapState_mds_time_mgr_set_time, -1, -1, "mds-time-mgr-set-time", NULL },
  { 12, &hf_oep_MdsTimeCapState_mds_time_capab_sync_bo_time, -1, -1, "mds-time-capab-sync-bo-time", NULL },
  { 13, &hf_oep_MdsTimeCapState_mds_time_state_bo_time_synced, -1, -1, "mds-time-state-bo-time-synced", NULL },
  { 14, &hf_oep_MdsTimeCapState_mds_time_state_bo_time_UTC_aligned, -1, -1, "mds-time-state-bo-time-UTC-aligned", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_MdsTimeCapState(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 239 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     MdsTimeCapState_bits, hf_index, ett_oep_MdsTimeCapState, NULL);


  return offset;
}



static int
dissect_oep_TimeProtocolId(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_OID_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t MdsTimeInfo_sequence[] = {
  { &hf_oep_mds_time_cap_state, MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_MdsTimeCapState },
  { &hf_oep_time_sync_protocol, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_TimeProtocolId },
  { &hf_oep_time_sync_accuracy, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RelativeTime },
  { &hf_oep_time_resolution_abs_time, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_time_resolution_rel_time, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_time_resolution_high_res_time, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U32 },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_MdsTimeInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   MdsTimeInfo_sequence, hf_index, ett_oep_MdsTimeInfo);

  return offset;
}


static const asn_namedbit MetricSpecSmall_bits[] = {
  {  0, &hf_oep_MetricSpecSmall_mss_avail_intermittent, -1, -1, "mss-avail-intermittent", NULL },
  {  1, &hf_oep_MetricSpecSmall_mss_avail_stored_data, -1, -1, "mss-avail-stored-data", NULL },
  {  2, &hf_oep_MetricSpecSmall_mss_upd_aperiodic, -1, -1, "mss-upd-aperiodic", NULL },
  {  3, &hf_oep_MetricSpecSmall_mss_msmt_aperiodic, -1, -1, "mss-msmt-aperiodic", NULL },
  {  4, &hf_oep_MetricSpecSmall_mss_msmt_phys_ev_id, -1, -1, "mss-msmt-phys-ev-id", NULL },
  {  5, &hf_oep_MetricSpecSmall_mss_msmt_btb_metric, -1, -1, "mss-msmt-btb-metric", NULL },
  {  8, &hf_oep_MetricSpecSmall_mss_acc_manager_initiated, -1, -1, "mss-acc-manager-initiated", NULL },
  {  9, &hf_oep_MetricSpecSmall_mss_acc_agent_initiated, -1, -1, "mss-acc-agent-initiated", NULL },
  { 12, &hf_oep_MetricSpecSmall_mss_cat_manual, -1, -1, "mss-cat-manual", NULL },
  { 13, &hf_oep_MetricSpecSmall_mss_cat_setting, -1, -1, "mss-cat-setting", NULL },
  { 14, &hf_oep_MetricSpecSmall_mss_cat_calculation, -1, -1, "mss-cat-calculation", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

int
dissect_oep_MetricSpecSmall(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 243 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     MetricSpecSmall_bits, hf_index, ett_oep_MetricSpecSmall, NULL);


  return offset;
}



static int
dissect_oep_EnumPrintableString(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const value_string oep_AuthBody_vals[] = {
  {   0, "auth-body-empty" },
  {   1, "auth-body-ieee-11073" },
  {   2, "auth-body-continua" },
  { 254, "auth-body-experimental" },
  { 255, "auth-body-reserved" },
  { 0, NULL }
};


static int
dissect_oep_AuthBody(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_oep_AuthBodyStrucType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_INT_U8(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t AuthBodyAndStrucType_sequence[] = {
  { &hf_oep_auth_body       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_AuthBody },
  { &hf_oep_auth_body_struc_type, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_AuthBodyStrucType },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_AuthBodyAndStrucType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AuthBodyAndStrucType_sequence, hf_index, ett_oep_AuthBodyAndStrucType);

  return offset;
}


static const mder_sequence_t RegCertData_sequence[] = {
  { &hf_oep_auth_body_and_struc_type, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AuthBodyAndStrucType },
  { &hf_oep_auth_body_data  , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AuthBodyAndStrucType },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_RegCertData(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   RegCertData_sequence, hf_index, ett_oep_RegCertData);

  return offset;
}


static const mder_sequence_t RegCertDataList_sequence_of[1] = {
  { &hf_oep_RegCertDataList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_RegCertData },
};

int
dissect_oep_RegCertDataList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      RegCertDataList_sequence_of, hf_index, ett_oep_RegCertDataList);

  return offset;
}



static int
dissect_oep_SFLOAT_Type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 344 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_sfloat(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}



static int
dissect_oep_BasicNuObsValue(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_SFLOAT_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const asn_namedbit SegmEntryHeader_bits[] = {
  {  0, &hf_oep_SegmEntryHeader_seg_elem_hdr_absolute_time, -1, -1, "seg-elem-hdr-absolute-time", NULL },
  {  1, &hf_oep_SegmEntryHeader_seg_elem_hdr_relative_time, -1, -1, "seg-elem-hdr-relative-time", NULL },
  {  2, &hf_oep_SegmEntryHeader_seg_elem_hdr_hires_relative_time, -1, -1, "seg-elem-hdr-hires-relative-time", NULL },
  {  3, &hf_oep_SegmEntryHeader_seg_elem_hdr_bo_time, -1, -1, "seg-elem-hdr-bo-time", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_SegmEntryHeader(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 259 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     SegmEntryHeader_bits, hf_index, ett_oep_SegmEntryHeader, NULL);


  return offset;
}


static const mder_sequence_t AttrValMapEntry_sequence[] = {
  { &hf_oep_attribute_id    , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_AVAType_attributeId },
  { &hf_oep_attribute_len   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_AttrValMapEntry(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AttrValMapEntry_sequence, hf_index, ett_oep_AttrValMapEntry);

  return offset;
}


static const mder_sequence_t AttrValMap_sequence_of[1] = {
  { &hf_oep_AttrValMap_item , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttrValMapEntry },
};

static int
dissect_oep_AttrValMap(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      AttrValMap_sequence_of, hf_index, ett_oep_AttrValMap);

  return offset;
}


static const mder_sequence_t SegmEntryElem_sequence[] = {
  { &hf_oep_class_id        , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_metric_type     , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_TYPE },
  { &hf_oep_handle          , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attr_val_map    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttrValMap },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmEntryElem(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SegmEntryElem_sequence, hf_index, ett_oep_SegmEntryElem);

  return offset;
}


static const mder_sequence_t SegmEntryElemList_sequence_of[1] = {
  { &hf_oep_SegmEntryElemList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmEntryElem },
};

static int
dissect_oep_SegmEntryElemList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SegmEntryElemList_sequence_of, hf_index, ett_oep_SegmEntryElemList);

  return offset;
}


static const mder_sequence_t PmSegmentEntryMap_sequence[] = {
  { &hf_oep_segm_entry_header, MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmEntryHeader },
  { &hf_oep_segm_entry_elem_list, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmEntryElemList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_PmSegmentEntryMap(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   PmSegmentEntryMap_sequence, hf_index, ett_oep_PmSegmentEntryMap);

  return offset;
}


static const value_string oep_PersonId_vals[] = {
  { 65535, "unknown-person-id" },
  { 0, NULL }
};


static int
dissect_oep_PersonId(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const value_string oep_SegmStatType_vals[] = {
  {   0, "segm-stat-type-undefined" },
  {   1, "segm-stat-type-minimum" },
  {   2, "segm-stat-type-maximum" },
  {   3, "segm-stat-type-average" },
  { 0, NULL }
};


static int
dissect_oep_SegmStatType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t SegmentStatisticEntry_sequence[] = {
  { &hf_oep_segm_stat_type  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmStatType },
  { &hf_oep_segm_stat_entry , MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmentStatisticEntry(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SegmentStatisticEntry_sequence, hf_index, ett_oep_SegmentStatisticEntry);

  return offset;
}


static const mder_sequence_t SegmentStatistics_sequence_of[1] = {
  { &hf_oep_SegmentStatistics_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmentStatisticEntry },
};

int
dissect_oep_SegmentStatistics(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SegmentStatistics_sequence_of, hf_index, ett_oep_SegmentStatistics);

  return offset;
}


static const mder_sequence_t HandleAttrValMapEntry_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attr_val_map    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttrValMap },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_HandleAttrValMapEntry(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   HandleAttrValMapEntry_sequence, hf_index, ett_oep_HandleAttrValMapEntry);

  return offset;
}


static const mder_sequence_t HandleAttrValMap_sequence_of[1] = {
  { &hf_oep_HandleAttrValMap_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_HandleAttrValMapEntry },
};

int
dissect_oep_HandleAttrValMap(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      HandleAttrValMap_sequence_of, hf_index, ett_oep_HandleAttrValMap);

  return offset;
}



static int
dissect_oep_SimpleNuObsValue(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_FLOAT_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t TypeVer_sequence[] = {
  { &hf_oep_type            , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_version         , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_TypeVer(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   TypeVer_sequence, hf_index, ett_oep_TypeVer);

  return offset;
}


static const mder_sequence_t TypeVerList_sequence_of[1] = {
  { &hf_oep_TypeVerList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_TypeVer },
};

int
dissect_oep_TypeVerList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      TypeVerList_sequence_of, hf_index, ett_oep_TypeVerList);

  return offset;
}


static const mder_sequence_t SupplementalTypeList_sequence_of[1] = {
  { &hf_oep_SupplementalTypeList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_TYPE },
};

int
dissect_oep_SupplementalTypeList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SupplementalTypeList_sequence_of, hf_index, ett_oep_SupplementalTypeList);

  return offset;
}



static int
dissect_oep_AbsoluteTimeAdjust(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 296 "../../asn1/oep/oep.cnf"
guint32 size = 6;
offset = dissect_mder_octet_string_size(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL, &size);


  return offset;
}


static const value_string oep_MetricStructureSmall_msStruct_vals[] = {
  {   0, "ms-struct-simple" },
  {   1, "ms-struct-compound" },
  {   2, "ms-struct-reserved" },
  {   3, "ms-struct-compound-fix" },
  { 0, NULL }
};


static int
dissect_oep_MetricStructureSmall_msStruct(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t MetricStructureSmall_sequence[] = {
  { &hf_oep_metricStructureSmall_msStruct, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_MetricStructureSmall_msStruct },
  { &hf_oep_ms_comp_no      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_MetricStructureSmall(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   MetricStructureSmall_sequence, hf_index, ett_oep_MetricStructureSmall);

  return offset;
}


static const mder_sequence_t SimpleNuObsValueCmp_sequence_of[1] = {
  { &hf_oep_SimpleNuObsValueCmp_item, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_SimpleNuObsValue },
};

int
dissect_oep_SimpleNuObsValueCmp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SimpleNuObsValueCmp_sequence_of, hf_index, ett_oep_SimpleNuObsValueCmp);

  return offset;
}


static const mder_sequence_t BasicNuObsValueCmp_sequence_of[1] = {
  { &hf_oep_BasicNuObsValueCmp_item, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_BasicNuObsValue },
};

int
dissect_oep_BasicNuObsValueCmp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      BasicNuObsValueCmp_sequence_of, hf_index, ett_oep_BasicNuObsValueCmp);

  return offset;
}


static const mder_sequence_t MetricIdList_sequence_of[1] = {
  { &hf_oep_MetricIdList_item, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
};

int
dissect_oep_MetricIdList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      MetricIdList_sequence_of, hf_index, ett_oep_MetricIdList);

  return offset;
}


static const mder_sequence_t HANDLEList_sequence_of[1] = {
  { &hf_oep_HANDLEList_item , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
};

static int
dissect_oep_HANDLEList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      HANDLEList_sequence_of, hf_index, ett_oep_HANDLEList);

  return offset;
}


const value_string oep_AVAAttributeValue_vals[] = {
  { 2323, "confirmMode" },
  { 2324, "confirmTimeout" },
  { 2337, "idHandle" },
  { 2338, "idInstNumber" },
  { 2343, "idLabelString" },
  { 2344, "idModel" },
  { 2347, "idPhysio" },
  { 2349, "idProdSpecn" },
  { 2351, "idType" },
  { 2372, "metricStoreUsageCount" },
  { 2375, "measurementStatus" },
  { 2378, "nuAccurMsmt" },
  { 2379, "nuObsValueCmp" },
  { 2384, "nuObsValue" },
  { 2385, "segmentNumber" },
  { 2387, "operationalState" },
  { 2389, "powerStatus" },
  { 2413, "saSpec" },
  { 2415, "scaleRangeSpec16" },
  { 2416, "scaleRangeSpec32" },
  { 2417, "scaleRangeSpec8" },
  { 2421, "scanRepPD" },
  { 2427, "segmentUsageCount" },
  { 2436, "systemId" },
  { 2438, "systemType" },
  { 2439, "absoluteTime" },
  { 2440, "batMeasure" },
  { 2442, "endTimeSegment" },
  { 2445, "pdTimeSample" },
  { 2447, "relativeTime" },
  { 2448, "timeStampAbsolute" },
  { 2449, "timeStampRelative" },
  { 2450, "startTimeSegment" },
  { 2453, "transmitWIndow" },
  { 2454, "unitCode" },
  { 2457, "unitLable" },
  { 2460, "batChargeValue" },
  { 2462, "enumObsValue" },
  { 2536, "highResRelativeTime" },
  { 2537, "timeStampRelativeHighRes" },
  { 2627, "authChallenge" },
  { 2627, "authResponse" },
  { 2628, "configId" },
  { 2629, "mdsTimeInfo" },
  { 2630, "metricSpecSmall" },
  { 2631, "sourceHandleReference" },
  { 2632, "simpleSaObsValue" },
  { 2633, "observedValueSimpleOID" },
  { 2634, "observedValueSimpleString" },
  { 2635, "regCertDataList" },
  { 2636, "basicNuObsValue" },
  { 2638, "pmSegmentEntryMap" },
  { 2639, "personId" },
  { 2640, "segmentStatistics" },
  { 2643, "scanHandleAttrValMap" },
  { 2644, "scanRepPDMin" },
  { 2645, "attrValMap" },
  { 2646, "simpleNuObsValue" },
  { 2647, "pmStoreLabel" },
  { 2648, "pmSegmentLabel" },
  { 2649, "pdTimeMsmtActive" },
  { 2650, "systemTypeVerList" },
  { 2655, "metricIdPartition" },
  { 2656, "observedValuePartition" },
  { 2657, "supplementalTypeList" },
  { 2658, "absoluteTimeAdjust" },
  { 2659, "clearTimeout" },
  { 2660, "transferTimeout" },
  { 2661, "observedValueSimple" },
  { 2662, "observedValueBasic" },
  { 2675, "metricStructureSmall" },
  { 2676, "simpleNuObsValueCmp" },
  { 2677, "basicNuObsValueCmp" },
  { 2678, "idPhysioList" },
  { 2679, "scanHandleList" },
  { 0, NULL }
};

static const mder_choice_t AVAAttributeValue_choice[] = {
  { 2323, &hf_oep_confirmMode     , MDER_CLASS_CON, 2323, 0, dissect_oep_ConfirmMode },
  { 2324, &hf_oep_confirmTimeout  , MDER_CLASS_CON, 2324, 0, dissect_oep_RelativeTime },
  { 2337, &hf_oep_idHandle        , MDER_CLASS_CON, 2337, 0, dissect_oep_HANDLE },
  { 2338, &hf_oep_idInstNumber    , MDER_CLASS_CON, 2338, 0, dissect_oep_InstNumber },
  { 2343, &hf_oep_idLabelString   , MDER_CLASS_CON, 2343, 0, dissect_oep_OCTET_STRING },
  { 2344, &hf_oep_idModel         , MDER_CLASS_CON, 2344, 0, dissect_oep_SystemModel },
  { 2347, &hf_oep_idPhysio        , MDER_CLASS_CON, 2347, 0, dissect_oep_OID_Type },
  { 2349, &hf_oep_idProdSpecn     , MDER_CLASS_CON, 2349, 0, dissect_oep_ProductionSpec },
  { 2351, &hf_oep_idType          , MDER_CLASS_CON, 2351, 0, dissect_oep_TYPE },
  { 2372, &hf_oep_metricStoreUsageCount, MDER_CLASS_CON, 2372, 0, dissect_oep_INT_U32 },
  { 2375, &hf_oep_measurementStatus, MDER_CLASS_CON, 2375, 0, dissect_oep_MeasurementStatus },
  { 2378, &hf_oep_nuAccurMsmt     , MDER_CLASS_CON, 2378, 0, dissect_oep_FLOAT_Type },
  { 2379, &hf_oep_nuObsValueCmp   , MDER_CLASS_CON, 2379, 0, dissect_oep_NuObsValueCmp },
  { 2384, &hf_oep_nuObsValue      , MDER_CLASS_CON, 2384, 0, dissect_oep_NuObsValue },
  { 2385, &hf_oep_segmentNumber   , MDER_CLASS_CON, 2385, 0, dissect_oep_INT_U16 },
  { 2387, &hf_oep_operationalState, MDER_CLASS_CON, 2387, 0, dissect_oep_OperationalState },
  { 2389, &hf_oep_powerStatus     , MDER_CLASS_CON, 2389, 0, dissect_oep_PowerStatus },
  { 2413, &hf_oep_saSpec          , MDER_CLASS_CON, 2413, 0, dissect_oep_SaSpec },
  { 2415, &hf_oep_scaleRangeSpec16, MDER_CLASS_CON, 2415, 0, dissect_oep_ScaleRangeSpec16 },
  { 2416, &hf_oep_scaleRangeSpec32, MDER_CLASS_CON, 2416, 0, dissect_oep_ScaleRangeSpec32 },
  { 2417, &hf_oep_scaleRangeSpec8 , MDER_CLASS_CON, 2417, 0, dissect_oep_ScaleRangeSpec8 },
  { 2421, &hf_oep_scanRepPD       , MDER_CLASS_CON, 2421, 0, dissect_oep_RelativeTime },
  { 2427, &hf_oep_segmentUsageCount, MDER_CLASS_CON, 2427, 0, dissect_oep_INT_U32 },
  { 2436, &hf_oep_systemId        , MDER_CLASS_CON, 2436, 0, dissect_oep_OCTET_STRING },
  { 2438, &hf_oep_systemType      , MDER_CLASS_CON, 2438, 0, dissect_oep_TYPE },
  { 2439, &hf_oep_absoluteTime    , MDER_CLASS_CON, 2439, 0, dissect_oep_AbsoluteTime },
  { 2440, &hf_oep_batMeasure      , MDER_CLASS_CON, 2440, 0, dissect_oep_BatMeasure },
  { 2442, &hf_oep_endTimeSegment  , MDER_CLASS_CON, 2442, 0, dissect_oep_AbsoluteTime },
  { 2445, &hf_oep_pdTimeSample    , MDER_CLASS_CON, 2445, 0, dissect_oep_RelativeTime },
  { 2447, &hf_oep_relativeTime    , MDER_CLASS_CON, 2447, 0, dissect_oep_RelativeTime },
  { 2448, &hf_oep_timeStampAbsolute, MDER_CLASS_CON, 2448, 0, dissect_oep_AbsoluteTime },
  { 2449, &hf_oep_timeStampRelative, MDER_CLASS_CON, 2449, 0, dissect_oep_RelativeTime },
  { 2450, &hf_oep_startTimeSegment, MDER_CLASS_CON, 2450, 0, dissect_oep_AbsoluteTime },
  { 2453, &hf_oep_transmitWIndow  , MDER_CLASS_CON, 2453, 0, dissect_oep_INT_U16 },
  { 2454, &hf_oep_unitCode        , MDER_CLASS_CON, 2454, 0, dissect_oep_OID_Type },
  { 2457, &hf_oep_unitLable       , MDER_CLASS_CON, 2457, 0, dissect_oep_OCTET_STRING },
  { 2460, &hf_oep_batChargeValue  , MDER_CLASS_CON, 2460, 0, dissect_oep_INT_U16 },
  { 2462, &hf_oep_enumObsValue    , MDER_CLASS_CON, 2462, 0, dissect_oep_EnumObsValue },
  { 2536, &hf_oep_highResRelativeTime, MDER_CLASS_CON, 2536, 0, dissect_oep_HighResRelativeTime },
  { 2537, &hf_oep_timeStampRelativeHighRes, MDER_CLASS_CON, 2537, 0, dissect_oep_HighResRelativeTime },
  { 2627, &hf_oep_authChallenge   , MDER_CLASS_CON, 2627, 0, dissect_oep_OCTET_STRING },
  { 2627, &hf_oep_authResponse    , MDER_CLASS_CON, 2627, 0, dissect_oep_OCTET_STRING },
  { 2628, &hf_oep_configId        , MDER_CLASS_CON, 2628, 0, dissect_oep_ConfigId },
  { 2629, &hf_oep_mdsTimeInfo     , MDER_CLASS_CON, 2629, 0, dissect_oep_MdsTimeInfo },
  { 2630, &hf_oep_metricSpecSmall , MDER_CLASS_CON, 2630, 0, dissect_oep_MetricSpecSmall },
  { 2631, &hf_oep_sourceHandleReference, MDER_CLASS_CON, 2631, 0, dissect_oep_HANDLE },
  { 2632, &hf_oep_simpleSaObsValue, MDER_CLASS_CON, 2632, 0, dissect_oep_OCTET_STRING },
  { 2633, &hf_oep_observedValueSimpleOID, MDER_CLASS_CON, 2633, 0, dissect_oep_OID_Type },
  { 2634, &hf_oep_observedValueSimpleString, MDER_CLASS_CON, 2634, 0, dissect_oep_EnumPrintableString },
  { 2635, &hf_oep_regCertDataList , MDER_CLASS_CON, 2635, 0, dissect_oep_RegCertDataList },
  { 2636, &hf_oep_basicNuObsValue , MDER_CLASS_CON, 2636, 0, dissect_oep_BasicNuObsValue },
  { 2638, &hf_oep_pmSegmentEntryMap, MDER_CLASS_CON, 2638, 0, dissect_oep_PmSegmentEntryMap },
  { 2639, &hf_oep_personId        , MDER_CLASS_CON, 2639, 0, dissect_oep_PersonId },
  { 2640, &hf_oep_segmentStatistics, MDER_CLASS_CON, 2640, 0, dissect_oep_SegmentStatistics },
  { 2643, &hf_oep_scanHandleAttrValMap, MDER_CLASS_CON, 2643, 0, dissect_oep_HandleAttrValMap },
  { 2644, &hf_oep_scanRepPDMin    , MDER_CLASS_CON, 2644, 0, dissect_oep_RelativeTime },
  { 2645, &hf_oep_attrValMap      , MDER_CLASS_CON, 2645, 0, dissect_oep_AttrValMap },
  { 2646, &hf_oep_simpleNuObsValue, MDER_CLASS_CON, 2646, 0, dissect_oep_SimpleNuObsValue },
  { 2647, &hf_oep_pmStoreLabel    , MDER_CLASS_CON, 2647, 0, dissect_oep_OCTET_STRING },
  { 2648, &hf_oep_pmSegmentLabel  , MDER_CLASS_CON, 2648, 0, dissect_oep_OCTET_STRING },
  { 2649, &hf_oep_pdTimeMsmtActive, MDER_CLASS_CON, 2649, 0, dissect_oep_FLOAT_Type },
  { 2650, &hf_oep_systemTypeVerList, MDER_CLASS_CON, 2650, 0, dissect_oep_TypeVerList },
  { 2655, &hf_oep_metricIdPartition, MDER_CLASS_CON, 2655, 0, dissect_oep_NomPartition },
  { 2656, &hf_oep_observedValuePartition, MDER_CLASS_CON, 2656, 0, dissect_oep_NomPartition },
  { 2657, &hf_oep_supplementalTypeList, MDER_CLASS_CON, 2657, 0, dissect_oep_SupplementalTypeList },
  { 2658, &hf_oep_absoluteTimeAdjust, MDER_CLASS_CON, 2658, 0, dissect_oep_AbsoluteTimeAdjust },
  { 2659, &hf_oep_clearTimeout    , MDER_CLASS_CON, 2659, 0, dissect_oep_RelativeTime },
  { 2660, &hf_oep_transferTimeout , MDER_CLASS_CON, 2660, 0, dissect_oep_RelativeTime },
  { 2661, &hf_oep_observedValueSimple, MDER_CLASS_CON, 2661, 0, dissect_oep_BITS_32 },
  { 2662, &hf_oep_observedValueBasic, MDER_CLASS_CON, 2662, 0, dissect_oep_BITS_16 },
  { 2675, &hf_oep_metricStructureSmall, MDER_CLASS_CON, 2675, 0, dissect_oep_MetricStructureSmall },
  { 2676, &hf_oep_simpleNuObsValueCmp, MDER_CLASS_CON, 2676, 0, dissect_oep_SimpleNuObsValueCmp },
  { 2677, &hf_oep_basicNuObsValueCmp, MDER_CLASS_CON, 2677, 0, dissect_oep_BasicNuObsValueCmp },
  { 2678, &hf_oep_idPhysioList    , MDER_CLASS_CON, 2678, 0, dissect_oep_MetricIdList },
  { 2679, &hf_oep_scanHandleList  , MDER_CLASS_CON, 2679, 0, dissect_oep_HANDLEList },
  { 0, NULL, 0, 0, 0, NULL }
};

int
dissect_oep_AVAAttributeValue(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 220 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset, (mder_choice_t*)AVAAttributeValue_choice, 
                                   hf_index, ett_oep_AVAAttributeValue, NULL);


  return offset;
}


static const mder_sequence_t AVA_Type_sequence[] = {
  { &hf_oep_attribute_id    , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_AVAType_attributeId },
  { &hf_oep_attribute_value , MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_AVAAttributeValue },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_AVA_Type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AVA_Type_sequence, hf_index, ett_oep_AVA_Type);

  return offset;
}


static const mder_sequence_t AttributeList_sequence_of[1] = {
  { &hf_oep_AttributeList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AVA_Type },
};

static int
dissect_oep_AttributeList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 310 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                  (mder_sequence_t*)AttributeList_sequence_of, hf_index, ett_oep_AttributeList);


  return offset;
}


static const mder_sequence_t AttributeIdList_sequence_of[1] = {
  { &hf_oep_AttributeIdList_item, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
};

static int
dissect_oep_AttributeIdList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      AttributeIdList_sequence_of, hf_index, ett_oep_AttributeIdList);

  return offset;
}


static const mder_sequence_t BaseOffsetTime_sequence[] = {
  { &hf_oep_bo_seconds      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U32 },
  { &hf_oep_bo_fraction     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_bo_time_offset  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_I16 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_BaseOffsetTime(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   BaseOffsetTime_sequence, hf_index, ett_oep_BaseOffsetTime);

  return offset;
}


const value_string oep_StoSampleAlg_vals[] = {
  {   0, "st-alg-nos" },
  {   1, "st-alg-moving-average" },
  {   2, "st-alg-recursive" },
  {   3, "st-alg-min-pick" },
  {   4, "st-alg-max-pick" },
  {   5, "st-alg-median" },
  { 512, "st-alg-trended" },
  { 1024, "st-alg-no-downsampling" },
  { 61440, "st-alg-manuf-specific-start" },
  { 65535, "st-alg-manuf-specific-end" },
  { 0, NULL }
};


int
dissect_oep_StoSampleAlg(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t SetTimeInvoke_sequence[] = {
  { &hf_oep_setTimeInvoke_dateTime, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AbsoluteTime },
  { &hf_oep_accuracy        , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_FLOAT_Type },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SetTimeInvoke(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SetTimeInvoke_sequence, hf_index, ett_oep_SetTimeInvoke);

  return offset;
}


static const mder_sequence_t SetBOTimeInvoke_sequence[] = {
  { &hf_oep_setBOTimeInvoke_dateTime, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_BaseOffsetTime },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SetBOTimeInvoke(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SetBOTimeInvoke_sequence, hf_index, ett_oep_SetBOTimeInvoke);

  return offset;
}


static const mder_sequence_t SegmIdList_sequence_of[1] = {
  { &hf_oep_SegmIdList_item , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_InstNumber },
};

static int
dissect_oep_SegmIdList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SegmIdList_sequence_of, hf_index, ett_oep_SegmIdList);

  return offset;
}


static const mder_sequence_t AbsTimeRange_sequence[] = {
  { &hf_oep_absTimeRange_fromTime, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AbsoluteTime },
  { &hf_oep_absTimeRange_toTime, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AbsoluteTime },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_AbsTimeRange(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AbsTimeRange_sequence, hf_index, ett_oep_AbsTimeRange);

  return offset;
}


static const mder_sequence_t BOTimeRange_sequence[] = {
  { &hf_oep_bOTimeRange_fromTime, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_BaseOffsetTime },
  { &hf_oep_bOTimeRange_toTime, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_BaseOffsetTime },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_BOTimeRange(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   BOTimeRange_sequence, hf_index, ett_oep_BOTimeRange);

  return offset;
}


static const value_string oep_SegmSelection_vals[] = {
  {   1, "all-segments" },
  {   2, "segm-id-list" },
  {   3, "abs-time-range" },
  {   4, "bo-time-range" },
  { 0, NULL }
};

static const mder_choice_t SegmSelection_choice[] = {
  {   1, &hf_oep_all_segments    , MDER_CLASS_CON, 1, 0, dissect_oep_INT_U16 },
  {   2, &hf_oep_segm_id_list    , MDER_CLASS_CON, 2, 0, dissect_oep_SegmIdList },
  {   3, &hf_oep_abs_time_range  , MDER_CLASS_CON, 3, 0, dissect_oep_AbsTimeRange },
  {   4, &hf_oep_bo_time_range   , MDER_CLASS_CON, 4, 0, dissect_oep_BOTimeRange },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmSelection(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_choice(actx, tree, tvb, offset,
                                 SegmSelection_choice, hf_index, ett_oep_SegmSelection,
                                 NULL);

  return offset;
}


static const mder_sequence_t SegmentInfo_sequence[] = {
  { &hf_oep_seg_inst_no     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_InstNumber },
  { &hf_oep_seg_info        , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmentInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SegmentInfo_sequence, hf_index, ett_oep_SegmentInfo);

  return offset;
}


static const mder_sequence_t SegmentInfoList_sequence_of[1] = {
  { &hf_oep_SegmentInfoList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmentInfo },
};

static int
dissect_oep_SegmentInfoList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SegmentInfoList_sequence_of, hf_index, ett_oep_SegmentInfoList);

  return offset;
}


static const mder_sequence_t ObservationScan_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attributes      , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ObservationScan(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ObservationScan_sequence, hf_index, ett_oep_ObservationScan);

  return offset;
}


static const asn_namedbit AssociationVersion_bits[] = {
  {  0, &hf_oep_AssociationVersion_assoc_version1, -1, -1, "assoc-version1", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_AssociationVersion(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 182 "../../asn1/oep/oep.cnf"
offset = dissect_mder_bitstring(implicit_tag, actx, tree, tvb, offset,
                                 AssociationVersion_bits, hf_index, ett_oep_AssociationVersion,
                                 NULL);


  return offset;
}


static const value_string oep_DataProtoId_vals[] = {
  {   0, "data-proto-id-empty" },
  { 20601, "data-proto-id-20601" },
  { 65535, "data-proto-id-external" },
  { 0, NULL }
};


static int
dissect_oep_DataProtoId(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 224 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_intu16(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);


  return offset;
}


static const asn_namedbit ProtocolVersion_bits[] = {
  {  0, &hf_oep_ProtocolVersion_protocol_version1, -1, -1, "protocol-version1", NULL },
  {  1, &hf_oep_ProtocolVersion_protocol_version2, -1, -1, "protocol-version2", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_ProtocolVersion(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 231 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring(implicit_tag, actx, tree, tvb, offset,
                                    ProtocolVersion_bits, hf_index, ett_oep_ProtocolVersion, NULL);


  return offset;
}


static const asn_namedbit EncodingRules_bits[] = {
  {  0, &hf_oep_EncodingRules_mder, -1, -1, "mder", NULL },
  {  1, &hf_oep_EncodingRules_xer, -1, -1, "xer", NULL },
  {  2, &hf_oep_EncodingRules_per, -1, -1, "per", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_EncodingRules(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 235 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     EncodingRules_bits, hf_index, ett_oep_EncodingRules, NULL);


  return offset;
}


static const asn_namedbit NomenclatureVersion_bits[] = {
  {  0, &hf_oep_NomenclatureVersion_nom_version1, -1, -1, "nom-version1", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_NomenclatureVersion(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 279 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring(implicit_tag, actx, tree, tvb, offset,
                                    NomenclatureVersion_bits, hf_index, ett_oep_NomenclatureVersion, NULL);


  return offset;
}


static const asn_namedbit FunctionalUnits_bits[] = {
  {  0, &hf_oep_FunctionalUnits_fun_units_unidirectional, -1, -1, "fun-units-unidirectional", NULL },
  {  1, &hf_oep_FunctionalUnits_fun_units_havetestcap, -1, -1, "fun-units-havetestcap", NULL },
  {  2, &hf_oep_FunctionalUnits_fun_units_createtestassoc, -1, -1, "fun-units-createtestassoc", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_FunctionalUnits(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 283 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring(implicit_tag, actx, tree, tvb, offset,
                                    FunctionalUnits_bits, hf_index, ett_oep_FunctionalUnits, NULL);


  return offset;
}


static const asn_namedbit SystemType_bits[] = {
  {  0, &hf_oep_SystemType_sys_type_manager, -1, -1, "sys-type-manager", NULL },
  {  8, &hf_oep_SystemType_sys_type_agent, -1, -1, "sys-type-agent", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_SystemType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 287 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring(implicit_tag, actx, tree, tvb, offset,
                                    SystemType_bits, hf_index, ett_oep_SystemType, NULL);


  return offset;
}


static const asn_namedbit DataReqModeFlags_bits[] = {
  {  0, &hf_oep_DataReqModeFlags_data_req_supp_stop, -1, -1, "data-req-supp-stop", NULL },
  {  4, &hf_oep_DataReqModeFlags_data_req_supp_scope_all, -1, -1, "data-req-supp-scope-all", NULL },
  {  5, &hf_oep_DataReqModeFlags_data_req_supp_scope_class, -1, -1, "data-req-supp-scope-class", NULL },
  {  6, &hf_oep_DataReqModeFlags_data_req_supp_scope_handle, -1, -1, "data-req-supp-scope-handle", NULL },
  {  8, &hf_oep_DataReqModeFlags_data_req_supp_mode_single_rsp, -1, -1, "data-req-supp-mode-single-rsp", NULL },
  {  9, &hf_oep_DataReqModeFlags_data_req_supp_mode_time_period, -1, -1, "data-req-supp-mode-time-period", NULL },
  { 10, &hf_oep_DataReqModeFlags_data_req_supp_mode_time_no_limit, -1, -1, "data-req-supp-mode-time-no-limit", NULL },
  { 11, &hf_oep_DataReqModeFlags_data_req_supp_person_id, -1, -1, "data-req-supp-person-id", NULL },
  { 15, &hf_oep_DataReqModeFlags_data_req_supp_init_agent, -1, -1, "data-req-supp-init-agent", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_DataReqModeFlags(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 251 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     DataReqModeFlags_bits, hf_index, ett_oep_DataReqModeFlags, NULL);


  return offset;
}


static const mder_sequence_t DataReqModeCapab_sequence[] = {
  { &hf_oep_data_req_mode_flags, MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqModeFlags },
  { &hf_oep_data_req_init_agent_count, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { &hf_oep_data_req_init_manager_count, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U8 },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_DataReqModeCapab(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 306 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                  (mder_sequence_t*) DataReqModeCapab_sequence, hf_index, ett_oep_DataReqModeCapab);


  return offset;
}


static const mder_sequence_t PhdAssociationInformation_sequence[] = {
  { &hf_oep_protocol_version, MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_ProtocolVersion },
  { &hf_oep_encoding_rules  , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_EncodingRules },
  { &hf_oep_nomenclature_version, MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_NomenclatureVersion },
  { &hf_oep_functional_units, MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_FunctionalUnits },
  { &hf_oep_system_type     , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_SystemType },
  { &hf_oep_system_id       , MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_OCTET_STRING },
  { &hf_oep_dev_config_id   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigId },
  { &hf_oep_data_req_mode_capab, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqModeCapab },
  { &hf_oep_option_list     , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_PhdAssociationInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 227 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   (mder_sequence_t*)PhdAssociationInformation_sequence, hf_index, ett_oep_PhdAssociationInformation);


  return offset;
}



static int
dissect_oep_UuidIdent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 301 "../../asn1/oep/oep.cnf"
guint32 size = 16;
offset = dissect_mder_octet_string_size(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL, &size);


  return offset;
}


static const mder_sequence_t ManufSpecAssociationInformation_sequence[] = {
  { &hf_oep_data_proto_id_ext, MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_UuidIdent },
  { &hf_oep_data_proto_info_ext, MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_UuidIdent },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ManufSpecAssociationInformation(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ManufSpecAssociationInformation_sequence, hf_index, ett_oep_ManufSpecAssociationInformation);

  return offset;
}



static int
dissect_oep_NULL(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_null(implicit_tag, actx, tree, tvb, offset, hf_index);

  return offset;
}


static const value_string oep_DataProtoInfo_vals[] = {
  { 20601, "phdAssociationInformation" },
  { 65535, "manufSpecAssociationInformation" },
  {   0, "empty" },
  { 0, NULL }
};

static const mder_choice_t DataProtoInfo_choice[] = {
  { 20601, &hf_oep_phdAssociationInformation, MDER_CLASS_CON, 20601, 0, dissect_oep_PhdAssociationInformation },
  { 65535, &hf_oep_manufSpecAssociationInformation, MDER_CLASS_CON, 65535, 0, dissect_oep_ManufSpecAssociationInformation },
  {   0, &hf_oep_empty           , MDER_CLASS_CON, 0, 0, dissect_oep_NULL },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_DataProtoInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 195 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset,
                                 (mder_choice_t*)DataProtoInfo_choice, hf_index, ett_oep_DataProtoInfo,
                                 NULL);


  return offset;
}


static const mder_sequence_t DataProto_sequence[] = {
  { &hf_oep_data_proto_id   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataProtoId },
  { &hf_oep_data_proto_info , MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_DataProtoInfo },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_DataProto(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 191 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   (mder_sequence_t*)DataProto_sequence, hf_index, ett_oep_DataProto);


  return offset;
}


static const mder_sequence_t DataProtoList_sequence_of[1] = {
  { &hf_oep_DataProtoList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_DataProto },
};

static int
dissect_oep_DataProtoList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 187 "../../asn1/oep/oep.cnf"
offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      (mder_sequence_t*)DataProtoList_sequence_of, hf_index, ett_oep_DataProtoList);


  return offset;
}


static const mder_sequence_t AarqApdu_sequence[] = {
  { &hf_oep_assoc_version   , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_AssociationVersion },
  { &hf_oep_data_proto_list , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_DataProtoList },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_AarqApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 156 "../../asn1/oep/oep.cnf"
offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   (mder_sequence_t*)AarqApdu_sequence, hf_index, ett_oep_AarqApdu);
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Association Request");


  return offset;
}


static const value_string oep_AssociateResult_vals[] = {
  {   0, "accepted" },
  {   1, "rejected-permanent" },
  {   2, "rejected-transient" },
  {   3, "accepted-unknown-config" },
  {   4, "rejected-no-common-protocol" },
  {   5, "rejected-no-common-parameter" },
  {   6, "rejected-unknown" },
  {   7, "rejected-unauthorized" },
  {   8, "rejected-unsupported-assoc-version" },
  { 0, NULL }
};


static int
dissect_oep_AssociateResult(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t AareApdu_sequence[] = {
  { &hf_oep_result          , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_AssociateResult },
  { &hf_oep_selected_data_proto, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_DataProto },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_AareApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AareApdu_sequence, hf_index, ett_oep_AareApdu);

#line 162 "../../asn1/oep/oep.cnf"
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Association Response");

  return offset;
}


static const value_string oep_ReleaseRequestReason_vals[] = {
  {   0, "normal" },
  {   1, "no-more-configurations" },
  {   2, "configuration-changed" },
  { 0, NULL }
};


static int
dissect_oep_ReleaseRequestReason(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t RlrqApdu_sequence[] = {
  { &hf_oep_rlrqApdu_reason , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ReleaseRequestReason },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_RlrqApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   RlrqApdu_sequence, hf_index, ett_oep_RlrqApdu);

#line 165 "../../asn1/oep/oep.cnf"
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Association Release Request");

  return offset;
}


static const value_string oep_ReleaseResponseReason_vals[] = {
  {   0, "normal" },
  { 0, NULL }
};


static int
dissect_oep_ReleaseResponseReason(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t RlreApdu_sequence[] = {
  { &hf_oep_rlreApdu_reason , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ReleaseResponseReason },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_RlreApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   RlreApdu_sequence, hf_index, ett_oep_RlreApdu);

#line 168 "../../asn1/oep/oep.cnf"
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Association Release Response");

  return offset;
}


static const value_string oep_Abort_reason_vals[] = {
  {   0, "undefined" },
  {   1, "buffer-overflow" },
  {   2, "response-timeout" },
  {   3, "configuration-timeout" },
  { 0, NULL }
};


static int
dissect_oep_Abort_reason(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t AbrtApdu_sequence[] = {
  { &hf_oep_abrtApdu_reason , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_Abort_reason },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_AbrtApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AbrtApdu_sequence, hf_index, ett_oep_AbrtApdu);

#line 171 "../../asn1/oep/oep.cnf"
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Association Abort");

  return offset;
}



int
dissect_oep_PrstApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 347 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_octet_string_length(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);
  offset = dissect_oep_DataApdu(implicit_tag, tvb, offset, actx, tree, hf_index);


#line 174 "../../asn1/oep/oep.cnf"
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Presentation PDU");

  return offset;
}


static const mder_sequence_t AtrgApdu_sequence[] = {
  { &hf_oep_optionList      , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_AtrgApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AtrgApdu_sequence, hf_index, ett_oep_AtrgApdu);

#line 177 "../../asn1/oep/oep.cnf"
col_add_fstr(actx->pinfo->cinfo, COL_INFO, "Association Trigger");

  return offset;
}


const value_string oep_ApduType_vals[] = {
  { 57856, "aarq" },
  { 58112, "aare" },
  { 58368, "rlrq" },
  { 58624, "rlre" },
  { 58880, "abrt" },
  { 59136, "prst" },
  { 59392, "atrg" },
  { 0, NULL }
};

static const mder_choice_t ApduType_choice[] = {
  { 57856, &hf_oep_aarq            , MDER_CLASS_CON, 57856, 0, dissect_oep_AarqApdu },
  { 58112, &hf_oep_aare            , MDER_CLASS_CON, 58112, 0, dissect_oep_AareApdu },
  { 58368, &hf_oep_rlrq            , MDER_CLASS_CON, 58368, 0, dissect_oep_RlrqApdu },
  { 58624, &hf_oep_rlre            , MDER_CLASS_CON, 58624, 0, dissect_oep_RlreApdu },
  { 58880, &hf_oep_abrt            , MDER_CLASS_CON, 58880, 0, dissect_oep_AbrtApdu },
  { 59136, &hf_oep_prst            , MDER_CLASS_CON, 59136, 0, dissect_oep_PrstApdu },
  { 59392, &hf_oep_atrg            , MDER_CLASS_CON, 59392, 0, dissect_oep_AtrgApdu },
  { 0, NULL, 0, 0, 0, NULL }
};

int
dissect_oep_ApduType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 149 "../../asn1/oep/oep.cnf"
actx->private_data = NULL;
offset = dissect_mder_choice(actx, tree, tvb, offset,
                                 (mder_choice_t*)ApduType_choice, hf_index, ett_oep_ApduType,
                                 NULL);
actx->private_data = NULL;


  return offset;
}



static int
dissect_oep_InvokeIDType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_INT_U16(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}



static int
dissect_oep_EventReportArgumentSimplet_eventType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_OID_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}



static int
dissect_oep_ConfigObject_objClass(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_OID_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t ConfigObject_sequence[] = {
  { &hf_oep_obj_class       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigObject_objClass },
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attributes      , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ConfigObject(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ConfigObject_sequence, hf_index, ett_oep_ConfigObject);

  return offset;
}


static const mder_sequence_t ConfigObjectList_sequence_of[1] = {
  { &hf_oep_ConfigObjectList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigObject },
};

static int
dissect_oep_ConfigObjectList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      ConfigObjectList_sequence_of, hf_index, ett_oep_ConfigObjectList);

  return offset;
}


static const mder_sequence_t ConfigReport_sequence[] = {
  { &hf_oep_config_report_id, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigId },
  { &hf_oep_config_obj_list , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigObjectList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ConfigReport(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ConfigReport_sequence, hf_index, ett_oep_ConfigReport);

  return offset;
}


static const value_string oep_DataReqId_vals[] = {
  {   0, "data-req-id-manager-initiated-min" },
  { 61439, "data-req-id-manager-initiated-max" },
  { 61440, "data-req-id-agent-initiated" },
  { 0, NULL }
};


static int
dissect_oep_DataReqId(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t ObservationValueData_sequence[] = {
  { &hf_oep_observed_val    , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_SimpleNuObsValue },
  { &hf_oep_absolute_time   , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AbsoluteTime },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ObservationValueData(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 351 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_octet_string_length(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);
    offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ObservationValueData_sequence, hf_index, ett_oep_ObservationValueData);



  return offset;
}


static const mder_sequence_t ObservationScanFixed_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_obs_val_data    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ObservationValueData },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ObservationScanFixed(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ObservationScanFixed_sequence, hf_index, ett_oep_ObservationScanFixed);

  return offset;
}


static const mder_sequence_t SEQUENCE_OF_ObservationScanFixed_sequence_of[1] = {
  { &hf_oep_obs_scan_fixed_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ObservationScanFixed },
};

static int
dissect_oep_SEQUENCE_OF_ObservationScanFixed(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_ObservationScanFixed_sequence_of, hf_index, ett_oep_SEQUENCE_OF_ObservationScanFixed);

  return offset;
}


static const mder_sequence_t ScanReportInfoFixed_sequence[] = {
  { &hf_oep_scanReportInfoFixed_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_scan_report_no  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_obs_scan_fixed  , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ObservationScanFixed },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportInfoFixed(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportInfoFixed_sequence, hf_index, ett_oep_ScanReportInfoFixed);

  return offset;
}


static const mder_sequence_t SEQUENCE_OF_ObservationScan_sequence_of[1] = {
  { &hf_oep_obs_scan_var_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ObservationScan },
};

static int
dissect_oep_SEQUENCE_OF_ObservationScan(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_ObservationScan_sequence_of, hf_index, ett_oep_SEQUENCE_OF_ObservationScan);

  return offset;
}


static const mder_sequence_t ScanReportInfoVar_sequence[] = {
  { &hf_oep_scanReportInfoVar_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_scan_report_no  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_obs_scan_var    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ObservationScan },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportInfoVar(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportInfoVar_sequence, hf_index, ett_oep_ScanReportInfoVar);

  return offset;
}


static const mder_sequence_t ScanReportPerFixed_sequence[] = {
  { &hf_oep_person_id       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_PersonId },
  { &hf_oep_obs_scan_fixed  , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ObservationScanFixed },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportPerFixed(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportPerFixed_sequence, hf_index, ett_oep_ScanReportPerFixed);

  return offset;
}


static const mder_sequence_t SEQUENCE_OF_ScanReportPerFixed_sequence_of[1] = {
  { &hf_oep_scan_per_fixed_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportPerFixed },
};

static int
dissect_oep_SEQUENCE_OF_ScanReportPerFixed(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_ScanReportPerFixed_sequence_of, hf_index, ett_oep_SEQUENCE_OF_ScanReportPerFixed);

  return offset;
}


static const mder_sequence_t ScanReportInfoMPFixed_sequence[] = {
  { &hf_oep_scanReportInfoMPFixed_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_scan_report_no  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_scan_per_fixed  , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ScanReportPerFixed },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportInfoMPFixed(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportInfoMPFixed_sequence, hf_index, ett_oep_ScanReportInfoMPFixed);

  return offset;
}


static const mder_sequence_t ScanReportPerVar_sequence[] = {
  { &hf_oep_person_id       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_PersonId },
  { &hf_oep_obs_scan_var    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ObservationScan },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportPerVar(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportPerVar_sequence, hf_index, ett_oep_ScanReportPerVar);

  return offset;
}


static const mder_sequence_t SEQUENCE_OF_ScanReportPerVar_sequence_of[1] = {
  { &hf_oep_scan_per_var_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportPerVar },
};

static int
dissect_oep_SEQUENCE_OF_ScanReportPerVar(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_ScanReportPerVar_sequence_of, hf_index, ett_oep_SEQUENCE_OF_ScanReportPerVar);

  return offset;
}


static const mder_sequence_t ScanReportInfoMPVar_sequence[] = {
  { &hf_oep_scanReportInfoMPVar_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_scan_report_no  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_scan_per_var    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ScanReportPerVar },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportInfoMPVar(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportInfoMPVar_sequence, hf_index, ett_oep_ScanReportInfoMPVar);

  return offset;
}


static const asn_namedbit SegmEvtStatus_bits[] = {
  {  0, &hf_oep_SegmEvtStatus_sevtsta_first_entry, -1, -1, "sevtsta-first-entry", NULL },
  {  1, &hf_oep_SegmEvtStatus_sevtsta_last_entry, -1, -1, "sevtsta-last-entry", NULL },
  {  4, &hf_oep_SegmEvtStatus_sevtsta_agent_abort, -1, -1, "sevtsta-agent-abort", NULL },
  {  8, &hf_oep_SegmEvtStatus_sevtsta_manager_confirm, -1, -1, "sevtsta-manager-confirm", NULL },
  { 12, &hf_oep_SegmEvtStatus_sevtsta_manager_abort, -1, -1, "sevtsta-manager-abort", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_SegmEvtStatus(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 263 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     SegmEvtStatus_bits, hf_index, ett_oep_SegmEvtStatus, NULL);


  return offset;
}


static const mder_sequence_t SegmDataEventDescr_sequence[] = {
  { &hf_oep_segm_instance   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_InstNumber },
  { &hf_oep_segm_evt_entry_index, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U32 },
  { &hf_oep_segm_evt_entry_count, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U32 },
  { &hf_oep_segm_evt_status , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmEvtStatus },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmDataEventDescr(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SegmDataEventDescr_sequence, hf_index, ett_oep_SegmDataEventDescr);

  return offset;
}


static const mder_sequence_t SegmentDataEvent_sequence[] = {
  { &hf_oep_segm_data_event_descr, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmDataEventDescr },
  { &hf_oep_segm_data_event_entries, MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_OCTET_STRING },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmentDataEvent(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SegmentDataEvent_sequence, hf_index, ett_oep_SegmentDataEvent);

  return offset;
}


static const value_string oep_EventReportInfo_vals[] = {
  { 3356, "configReport" },
  { 3357, "scanReportInfoFixed" },
  { 3358, "scanReportInfoVar" },
  { 3359, "scanReportInfoMPFixed" },
  { 3360, "scanReportInfoMPVar" },
  { 3361, "segmentDataEvent" },
  { 0, NULL }
};

static const mder_choice_t EventReportInfo_choice[] = {
  { 3356, &hf_oep_configReport    , MDER_CLASS_CON, 3356, 0, dissect_oep_ConfigReport },
  { 3357, &hf_oep_scanReportInfoFixed, MDER_CLASS_CON, 3357, 0, dissect_oep_ScanReportInfoFixed },
  { 3358, &hf_oep_scanReportInfoVar, MDER_CLASS_CON, 3358, 0, dissect_oep_ScanReportInfoVar },
  { 3359, &hf_oep_scanReportInfoMPFixed, MDER_CLASS_CON, 3359, 0, dissect_oep_ScanReportInfoMPFixed },
  { 3360, &hf_oep_scanReportInfoMPVar, MDER_CLASS_CON, 3360, 0, dissect_oep_ScanReportInfoMPVar },
  { 3361, &hf_oep_segmentDataEvent, MDER_CLASS_CON, 3361, 0, dissect_oep_SegmentDataEvent },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_EventReportInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 204 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset, (mder_choice_t*)EventReportInfo_choice, 
                                   hf_index, ett_oep_EventReportInfo, NULL);


  return offset;
}



static int
dissect_oep_EventReportArgumentSimple_eventInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_EventReportInfo(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t EventReportArgumentSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_event_time      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RelativeTime },
  { &hf_oep_eventReportArgumentSimple_eventType, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_EventReportArgumentSimplet_eventType },
  { &hf_oep_eventReportArgumentSimple_eventInfo, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_EventReportArgumentSimple_eventInfo },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_EventReportArgumentSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EventReportArgumentSimple_sequence, hf_index, ett_oep_EventReportArgumentSimple);

  return offset;
}


static const mder_sequence_t GetArgumentSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attribute_id_list, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeIdList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_GetArgumentSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GetArgumentSimple_sequence, hf_index, ett_oep_GetArgumentSimple);

  return offset;
}


static const value_string oep_ModifyOperator_vals[] = {
  {   0, "replace" },
  {   1, "addValues" },
  {   2, "removeValues" },
  {   3, "setToDefault" },
  { 0, NULL }
};


static int
dissect_oep_ModifyOperator(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t AttributeModEntry_sequence[] = {
  { &hf_oep_modify_operator , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ModifyOperator },
  { &hf_oep_attribute       , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AVA_Type },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_AttributeModEntry(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   AttributeModEntry_sequence, hf_index, ett_oep_AttributeModEntry);

  return offset;
}


static const mder_sequence_t ModificationList_sequence_of[1] = {
  { &hf_oep_ModificationList_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeModEntry },
};

static int
dissect_oep_ModificationList(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      ModificationList_sequence_of, hf_index, ett_oep_ModificationList);

  return offset;
}


static const mder_sequence_t SetArgumentSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_modification_list, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ModificationList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SetArgumentSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SetArgumentSimple_sequence, hf_index, ett_oep_SetArgumentSimple);

  return offset;
}


static const asn_namedbit DataReqMode_bits[] = {
  {  0, &hf_oep_DataReqMode_data_req_start_stop, -1, -1, "data-req-start-stop", NULL },
  {  1, &hf_oep_DataReqMode_data_req_continuation, -1, -1, "data-req-continuation", NULL },
  {  4, &hf_oep_DataReqMode_data_req_scope_all, -1, -1, "data-req-scope-all", NULL },
  {  5, &hf_oep_DataReqMode_data_req_scope_class, -1, -1, "data-req-scope-class", NULL },
  {  6, &hf_oep_DataReqMode_data_req_scope_handle, -1, -1, "data-req-scope-handle", NULL },
  {  8, &hf_oep_DataReqMode_data_req_mode_single_rsp, -1, -1, "data-req-mode-single-rsp", NULL },
  {  9, &hf_oep_DataReqMode_data_req_mode_time_period, -1, -1, "data-req-mode-time-period", NULL },
  { 10, &hf_oep_DataReqMode_data_req_mode_time_no_limit, -1, -1, "data-req-mode-time-no-limit", NULL },
  { 12, &hf_oep_DataReqMode_data_req_person_id, -1, -1, "data-req-person-id", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

static int
dissect_oep_DataReqMode(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 247 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     DataReqMode_bits, hf_index, ett_oep_DataReqMode, NULL);


  return offset;
}


static const mder_sequence_t DataRequest_sequence[] = {
  { &hf_oep_dataRequest_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_data_req_mode   , MDER_CLASS_UNI, MDER_UNI_TAG_BITSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqMode },
  { &hf_oep_data_req_time   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RelativeTime },
  { &hf_oep_data_req_person_id, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_data_req_class  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_data_req_obj_handle_list, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLEList },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_DataRequest(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   DataRequest_sequence, hf_index, ett_oep_DataRequest);

  return offset;
}


static const mder_sequence_t TrigSegmDataXferReq_sequence[] = {
  { &hf_oep_seg_inst_no     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_InstNumber },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_TrigSegmDataXferReq(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   TrigSegmDataXferReq_sequence, hf_index, ett_oep_TrigSegmDataXferReq);

  return offset;
}


static const value_string oep_ActionArgumentSimpleInfoArgs_vals[] = {
  {   0, "dataRequest" },
  {   1, "setTimeInvoke" },
  {   2, "setBOTimeInvoke" },
  {   3, "segmSelection" },
  {   4, "trigSegmDataXferReq" },
  { 0, NULL }
};

static const mder_choice_t ActionArgumentSimpleInfoArgs_choice[] = {
  {   0, &hf_oep_dataRequest     , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_DataRequest },
  {   1, &hf_oep_setTimeInvoke   , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SetTimeInvoke },
  {   2, &hf_oep_setBOTimeInvoke , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SetBOTimeInvoke },
  {   3, &hf_oep_segmSelection   , MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmSelection },
  {   4, &hf_oep_trigSegmDataXferReq, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_TrigSegmDataXferReq },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ActionArgumentSimpleInfoArgs(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 200 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset, (mder_choice_t*)ActionArgumentSimpleInfoArgs_choice, 
                                   hf_index, ett_oep_ActionArgumentSimpleInfoArgs, NULL);


  return offset;
}



static int
dissect_oep_ActionArgumentSimple_actionInfoArgs(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_ActionArgumentSimpleInfoArgs(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t ActionArgumentSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_action_type     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_actionArgumentSimple_actionInfoArgs, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_ActionArgumentSimple_actionInfoArgs },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ActionArgumentSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ActionArgumentSimple_sequence, hf_index, ett_oep_ActionArgumentSimple);

  return offset;
}



static int
dissect_oep_EventReportResultSimple_eventType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_OID_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const value_string oep_ConfigResult_vals[] = {
  {   0, "accepted-config" },
  {   1, "unsupported-config" },
  {   2, "standard-config-unknown" },
  { 0, NULL }
};


static int
dissect_oep_ConfigResult(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t ConfigReportRsp_sequence[] = {
  { &hf_oep_config_report_id, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigId },
  { &hf_oep_config_result   , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_ConfigResult },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ConfigReportRsp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ConfigReportRsp_sequence, hf_index, ett_oep_ConfigReportRsp);

  return offset;
}


static const mder_sequence_t SegmentDataResult_sequence[] = {
  { &hf_oep_segm_data_event_descr, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmDataEventDescr },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SegmentDataResult(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SegmentDataResult_sequence, hf_index, ett_oep_SegmentDataResult);

  return offset;
}


static const value_string oep_EventReplyInfo_vals[] = {
  {   0, "empty" },
  { 3356, "configReportRsp" },
  { 3361, "segmentDataResult" },
  { 0, NULL }
};

static const mder_choice_t EventReplyInfo_choice[] = {
  {   0, &hf_oep_empty           , MDER_CLASS_CON, 0, 0, dissect_oep_NULL },
  { 3356, &hf_oep_configReportRsp , MDER_CLASS_CON, 3356, 0, dissect_oep_ConfigReportRsp },
  { 3361, &hf_oep_segmentDataResult, MDER_CLASS_CON, 3361, 0, dissect_oep_SegmentDataResult },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_EventReplyInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 208 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset, (mder_choice_t*)EventReplyInfo_choice, 
                                   hf_index, ett_oep_EventReplyInfo, NULL);


  return offset;
}


static const mder_sequence_t EventReportResultSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_currentTime     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RelativeTime },
  { &hf_oep_eventReportResultSimple_eventType, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_EventReportResultSimple_eventType },
  { &hf_oep_event_reply_info, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_EventReplyInfo },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_EventReportResultSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   EventReportResultSimple_sequence, hf_index, ett_oep_EventReportResultSimple);

  return offset;
}


static const mder_sequence_t GetResultSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attribute_list  , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_GetResultSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   GetResultSimple_sequence, hf_index, ett_oep_GetResultSimple);

  return offset;
}


static const mder_sequence_t SetResultSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_attribute_list  , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_AttributeList },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_SetResultSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   SetResultSimple_sequence, hf_index, ett_oep_SetResultSimple);

  return offset;
}


static const value_string oep_DataReqResult_vals[] = {
  {   0, "data-req-result-no-error" },
  {   1, "data-req-result-unspecific-error" },
  {   2, "data-req-result-no-stop-support" },
  {   3, "data-req-result-no-scope-all-support" },
  {   4, "data-req-result-no-scope-class-support" },
  {   5, "data-req-result-no-scope-handle-support" },
  {   6, "data-req-result-no-mode-single-rsp-support" },
  {   7, "data-req-result-no-mode-time-period-support" },
  {   8, "data-req-result-no-mode-time-no-limit-support" },
  {   9, "data-req-result-no-person-id-support" },
  {  11, "data-req-result-unknown-person-id" },
  {  12, "data-req-result-unknown-class" },
  {  13, "data-req-result-unknown-handle" },
  {  14, "data-req-result-unsupp-scope" },
  {  15, "data-req-result-unsupp-mode" },
  {  16, "data-req-result-init-manager-overflow" },
  {  17, "data-req-result-continuation-not-supported" },
  {  18, "data-req-result-invalid-req-id" },
  { 0, NULL }
};


static int
dissect_oep_DataReqResult(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}



static int
dissect_oep_DateResponse_eventType(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_OID_Type(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const value_string oep_DataResponseEventInfo_vals[] = {
  {   0, "scanReportInfoFixed" },
  {   1, "scanReportInfoVar" },
  {   2, "scanReportInfoMPFixed" },
  {   3, "scanReportInfoMPVar" },
  { 0, NULL }
};

static const mder_choice_t DataResponseEventInfo_choice[] = {
  {   0, &hf_oep_scanReportInfoFixed, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportInfoFixed },
  {   1, &hf_oep_scanReportInfoVar, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportInfoVar },
  {   2, &hf_oep_scanReportInfoMPFixed, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportInfoMPFixed },
  {   3, &hf_oep_scanReportInfoMPVar, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportInfoMPVar },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_DataResponseEventInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 216 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset, (mder_choice_t*)DataResponseEventInfo_choice, 
                                   hf_index, ett_oep_DataResponseEventInfo, NULL);


  return offset;
}



static int
dissect_oep_DataResponse_eventInfo(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_DataResponseEventInfo(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t DataResponse_sequence[] = {
  { &hf_oep_rel_time_stamp  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RelativeTime },
  { &hf_oep_data_req_result , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqResult },
  { &hf_oep_event_type      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DateResponse_eventType },
  { &hf_oep_dataResponse_eventInfo, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_DataResponse_eventInfo },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_DataResponse(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   DataResponse_sequence, hf_index, ett_oep_DataResponse);

  return offset;
}


static const value_string oep_TrigSegmXferRsp_vals[] = {
  {   0, "tsxr-successful" },
  {   1, "tsxr-fail-no-such-segment" },
  {   2, "tsxr-fail-clear-in-process" },
  {   3, "tsxr-fail-segm-empty" },
  { 512, "tsxr-fail-not-otherwise-specified" },
  { 0, NULL }
};


static int
dissect_oep_TrigSegmXferRsp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t TrigSegmDataXferRsp_sequence[] = {
  { &hf_oep_seg_inst_no     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_InstNumber },
  { &hf_oep_trig_segm_xfer_rsp, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_TrigSegmXferRsp },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_TrigSegmDataXferRsp(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   TrigSegmDataXferRsp_sequence, hf_index, ett_oep_TrigSegmDataXferRsp);

  return offset;
}


static const value_string oep_ActionResultSimpleInfoArgs_vals[] = {
  {   0, "empty" },
  {   1, "dataResponse" },
  {   2, "segmentInfoList" },
  {   3, "trigSegmDataXferRsp" },
  { 0, NULL }
};

static const mder_choice_t ActionResultSimpleInfoArgs_choice[] = {
  {   0, &hf_oep_empty           , MDER_CLASS_UNI, MDER_UNI_TAG_NULL, MDER_FLAGS_NOOWNTAG, dissect_oep_NULL },
  {   1, &hf_oep_dataResponse    , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_DataResponse },
  {   2, &hf_oep_segmentInfoList , MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SegmentInfoList },
  {   3, &hf_oep_trigSegmDataXferRsp, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_TrigSegmDataXferRsp },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ActionResultSimpleInfoArgs(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 212 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_any(actx, tree, tvb, offset, (mder_choice_t*)ActionResultSimpleInfoArgs_choice, 
                                   hf_index, ett_oep_ActionResultSimpleInfoArgs, NULL);


  return offset;
}



static int
dissect_oep_ActionResultSimple_actionInfoArgs(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_oep_ActionResultSimpleInfoArgs(implicit_tag, tvb, offset, actx, tree, hf_index);

  return offset;
}


static const mder_sequence_t ActionResultSimple_sequence[] = {
  { &hf_oep_obj_handle      , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_HANDLE },
  { &hf_oep_action_type     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_OID_Type },
  { &hf_oep_actionResultSimple_actionInfoArgs, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_ActionResultSimple_actionInfoArgs },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ActionResultSimple(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ActionResultSimple_sequence, hf_index, ett_oep_ActionResultSimple);

  return offset;
}


static const value_string oep_RoerErrorValue_vals[] = {
  {   1, "no-such-object-instance" },
  {   9, "no-such-action" },
  {  17, "invalid-object-instance" },
  {  23, "protocol-violation" },
  {  24, "not-allowed-by-object" },
  {  25, "action-timed-out" },
  {  26, "action-aborted" },
  { 0, NULL }
};


static int
dissect_oep_RoerErrorValue(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t ErrorResult_sequence[] = {
  { &hf_oep_error_value     , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RoerErrorValue },
  { &hf_oep_parameter       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RoerErrorValue },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ErrorResult(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ErrorResult_sequence, hf_index, ett_oep_ErrorResult);

  return offset;
}


static const value_string oep_RorjProblem_vals[] = {
  {   0, "unrecognized-apdu" },
  {   2, "badly-structured-apdu" },
  { 101, "unrecognized-operation" },
  { 103, "resource-limitation" },
  { 303, "unexpected-error" },
  { 0, NULL }
};


static int
dissect_oep_RorjProblem(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index,
                                                NULL);

  return offset;
}


static const mder_sequence_t RejectResult_sequence[] = {
  { &hf_oep_problem         , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_RorjProblem },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_RejectResult(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   RejectResult_sequence, hf_index, ett_oep_RejectResult);

  return offset;
}


static const value_string oep_DataApdu_message_vals[] = {
  { 256, "roiv-cmip-event-report" },
  { 257, "roiv-cmip-confirmed-event-report" },
  { 259, "roiv-cmip-get" },
  { 260, "roiv-cmip-set" },
  { 261, "roiv-cmip-confirmed-set" },
  { 262, "roiv-cmip-action" },
  { 263, "roiv-cmip-confirmed-action" },
  { 513, "rors-cmip-confirmed-event-report" },
  { 515, "rors-cmip-get" },
  { 517, "rors-cmip-confirmed-set" },
  { 519, "rors-cmip-confirmed-action" },
  { 768, "roer" },
  { 1024, "rorj" },
  { 0, NULL }
};

static const mder_choice_t DataApdu_message_choice[] = {
  { 256, &hf_oep_roiv_cmip_event_report, MDER_CLASS_CON, 256, 0, dissect_oep_EventReportArgumentSimple },
  { 257, &hf_oep_roiv_cmip_confirmed_event_report, MDER_CLASS_CON, 257, 0, dissect_oep_EventReportArgumentSimple },
  { 259, &hf_oep_roiv_cmip_get   , MDER_CLASS_CON, 259, 0, dissect_oep_GetArgumentSimple },
  { 260, &hf_oep_roiv_cmip_set   , MDER_CLASS_CON, 260, 0, dissect_oep_SetArgumentSimple },
  { 261, &hf_oep_roiv_cmip_confirmed_set, MDER_CLASS_CON, 261, 0, dissect_oep_SetArgumentSimple },
  { 262, &hf_oep_roiv_cmip_action, MDER_CLASS_CON, 262, 0, dissect_oep_ActionArgumentSimple },
  { 263, &hf_oep_roiv_cmip_confirmed_action, MDER_CLASS_CON, 263, 0, dissect_oep_ActionArgumentSimple },
  { 513, &hf_oep_rors_cmip_confirmed_event_report, MDER_CLASS_CON, 513, 0, dissect_oep_EventReportResultSimple },
  { 515, &hf_oep_rors_cmip_get   , MDER_CLASS_CON, 515, 0, dissect_oep_GetResultSimple },
  { 517, &hf_oep_rors_cmip_confirmed_set, MDER_CLASS_CON, 517, 0, dissect_oep_SetResultSimple },
  { 519, &hf_oep_rors_cmip_confirmed_action, MDER_CLASS_CON, 519, 0, dissect_oep_ActionResultSimple },
  { 768, &hf_oep_roer            , MDER_CLASS_CON, 768, 0, dissect_oep_ErrorResult },
  { 1024, &hf_oep_rorj            , MDER_CLASS_CON, 1024, 0, dissect_oep_RejectResult },
  { 0, NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_DataApdu_message(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_choice(actx, tree, tvb, offset,
                                 DataApdu_message_choice, hf_index, ett_oep_DataApdu_message,
                                 NULL);

  return offset;
}


static const mder_sequence_t DataApdu_sequence[] = {
  { &hf_oep_invoke_id       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_InvokeIDType },
  { &hf_oep_dataApdu_message, MDER_CLASS_ANY/*choice*/, -1/*choice*/, MDER_FLAGS_NOOWNTAG|MDER_FLAGS_NOTCHKTAG, dissect_oep_DataApdu_message },
  { NULL, 0, 0, 0, NULL }
};

int
dissect_oep_DataApdu(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   DataApdu_sequence, hf_index, ett_oep_DataApdu);

  return offset;
}



static int
dissect_oep_ObservationScanGrouped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_octet_string(implicit_tag, actx, tree, tvb, offset, hf_index,
                                       NULL);

  return offset;
}


static const mder_sequence_t SEQUENCE_OF_ObservationScanGrouped_sequence_of[1] = {
  { &hf_oep_scanReportInfoGrouped_obsScanGrouped_item, MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_ObservationScanGrouped },
};

static int
dissect_oep_SEQUENCE_OF_ObservationScanGrouped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_ObservationScanGrouped_sequence_of, hf_index, ett_oep_SEQUENCE_OF_ObservationScanGrouped);

  return offset;
}


static const mder_sequence_t ScanReportInfoGrouped_sequence[] = {
  { &hf_oep_scanReportInfoGrouped_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_scan_report_no  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_scanReportInfoGrouped_obsScanGrouped, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ObservationScanGrouped },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportInfoGrouped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportInfoGrouped_sequence, hf_index, ett_oep_ScanReportInfoGrouped);

  return offset;
}


static const mder_sequence_t ScanReportPerGrouped_sequence[] = {
  { &hf_oep_person_id       , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_PersonId },
  { &hf_oep_scanReportPerGrouped_obsScanGrouped, MDER_CLASS_UNI, MDER_UNI_TAG_OCTETSTRING, MDER_FLAGS_NOOWNTAG, dissect_oep_ObservationScanGrouped },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportPerGrouped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportPerGrouped_sequence, hf_index, ett_oep_ScanReportPerGrouped);

  return offset;
}


static const mder_sequence_t SEQUENCE_OF_ScanReportPerGrouped_sequence_of[1] = {
  { &hf_oep_scan_per_grouped_item, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_ScanReportPerGrouped },
};

static int
dissect_oep_SEQUENCE_OF_ScanReportPerGrouped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence_of(implicit_tag, actx, tree, tvb, offset,
                                      SEQUENCE_OF_ScanReportPerGrouped_sequence_of, hf_index, ett_oep_SEQUENCE_OF_ScanReportPerGrouped);

  return offset;
}


static const mder_sequence_t ScanReportInfoMPGrouped_sequence[] = {
  { &hf_oep_scanReportInfoMPGrouped_dataReqId, MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_DataReqId },
  { &hf_oep_scan_report_no  , MDER_CLASS_UNI, MDER_UNI_TAG_INTEGER, MDER_FLAGS_NOOWNTAG, dissect_oep_INT_U16 },
  { &hf_oep_scan_per_grouped, MDER_CLASS_UNI, MDER_UNI_TAG_SEQUENCE, MDER_FLAGS_NOOWNTAG, dissect_oep_SEQUENCE_OF_ScanReportPerGrouped },
  { NULL, 0, 0, 0, NULL }
};

static int
dissect_oep_ScanReportInfoMPGrouped(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
  offset = dissect_mder_sequence(implicit_tag, actx, tree, tvb, offset,
                                   ScanReportInfoMPGrouped_sequence, hf_index, ett_oep_ScanReportInfoMPGrouped);

  return offset;
}


static const asn_namedbit PmStoreCapab_bits[] = {
  {  0, &hf_oep_PmStoreCapab_pmsc_var_no_of_segm, -1, -1, "pmsc-var-no-of-segm", NULL },
  {  3, &hf_oep_PmStoreCapab_pmsc_segm_id_list_select, -1, -1, "pmsc-segm-id-list-select", NULL },
  {  4, &hf_oep_PmStoreCapab_pmsc_epi_seg_entries, -1, -1, "pmsc-epi-seg-entries", NULL },
  {  5, &hf_oep_PmStoreCapab_pmsc_peri_seg_entries, -1, -1, "pmsc-peri-seg-entries", NULL },
  {  6, &hf_oep_PmStoreCapab_pmsc_abs_time_select, -1, -1, "pmsc-abs-time-select", NULL },
  {  7, &hf_oep_PmStoreCapab_pmsc_clear_segm_by_list_sup, -1, -1, "pmsc-clear-segm-by-list-sup", NULL },
  {  8, &hf_oep_PmStoreCapab_pmsc_clear_segm_by_time_sup, -1, -1, "pmsc-clear-segm-by-time-sup", NULL },
  {  9, &hf_oep_PmStoreCapab_pmsc_clear_segm_remove, -1, -1, "pmsc-clear-segm-remove", NULL },
  { 10, &hf_oep_PmStoreCapab_pmsc_clear_segm_all_sup, -1, -1, "pmsc-clear-segm-all-sup", NULL },
  { 12, &hf_oep_PmStoreCapab_pmsc_multi_person, -1, -1, "pmsc-multi-person", NULL },
  { 0, NULL, 0, 0, NULL, NULL }
};

int
dissect_oep_PmStoreCapab(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_) {
#line 255 "../../asn1/oep/oep.cnf"
  offset = dissect_mder_bitstring16(implicit_tag, actx, tree, tvb, offset, 
                                     PmStoreCapab_bits, hf_index, ett_oep_PmStoreCapab, NULL);


  return offset;
}

/*--- PDUs ---*/

static void dissect_ApduType_PDU(tvbuff_t *tvb _U_, packet_info *pinfo _U_, proto_tree *tree _U_) {
  asn1_ctx_t asn1_ctx;
  asn1_ctx_init(&asn1_ctx, ASN1_ENC_MDER, TRUE, pinfo);
  dissect_oep_ApduType(FALSE, tvb, 0, &asn1_ctx, tree, hf_oep_ApduType_PDU);
}


/*--- End of included file: packet-oep-fn.c ---*/
#line 79 "../../asn1/oep/packet-oep-template.c"

static void
dissect_oep(tvbuff_t *tvb, packet_info *pinfo, proto_tree *parent_tree) {
    proto_item *item = NULL;
    proto_tree *oep_tree = NULL;
    asn1_ctx_t asn1_ctx;
    asn1_ctx_init(&asn1_ctx, ASN1_ENC_MDER, TRUE, pinfo);
    
    col_set_str(pinfo->cinfo, COL_PROTOCOL, PSNAME);
    /* Clear out stuff in the info column */
    col_clear(pinfo->cinfo,COL_INFO);

    if (parent_tree) { /* we are being asked for details */
        gint offset = 0;
        gint length = 1;
        
        item = proto_tree_add_item(parent_tree, proto_oep, tvb, 0, -1, ENC_NA);
        oep_tree = proto_item_add_subtree(item, ett_oep);
    } 
    dissect_ApduType_PDU(tvb, pinfo, oep_tree);
    
}

/*--- proto_register_oep ----------------------------------------------*/
void proto_register_oep(void) {

    module_t *oep_module;
    
  /* List of fields */
  static hf_register_info hf[] = {

/*--- Included file: packet-oep-hfarr.c ---*/
#line 1 "../../asn1/oep/packet-oep-hfarr.c"
    { &hf_oep_ApduType_PDU,
      { "ApduType", "oep.ApduType",
        FT_UINT16, BASE_HEX, VALS(oep_Apdu_vals), 0,
        NULL, HFILL }},
    { &hf_oep_partition,
      { "partition", "oep.partition",
        FT_UINT16, BASE_DEC, VALS(oep_NomPartition_vals), 0,
        "NomPartition", HFILL }},
    { &hf_oep_code,
      { "code", "oep.code",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_attribute_id,
      { "attribute-id", "oep.attribute_id",
        FT_UINT32, BASE_DEC, VALS(oep_AVAAttributeValue_vals), 0,
        "AVAType_attributeId", HFILL }},
    { &hf_oep_attribute_value,
      { "attribute-value", "oep.attribute_value",
        FT_UINT32, BASE_DEC, VALS(oep_AVAAttributeValue_vals), 0,
        "AVAAttributeValue", HFILL }},
    { &hf_oep_confirmMode,
      { "confirmMode", "oep.confirmMode",
        FT_UINT16, BASE_DEC, VALS(oep_ConfirmMode_vals), 0,
        NULL, HFILL }},
    { &hf_oep_confirmTimeout,
      { "confirmTimeout", "oep.confirmTimeout",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_idHandle,
      { "idHandle", "oep.idHandle",
        FT_UINT32, BASE_DEC, NULL, 0,
        "HANDLE", HFILL }},
    { &hf_oep_idInstNumber,
      { "idInstNumber", "oep.idInstNumber",
        FT_UINT32, BASE_DEC, NULL, 0,
        "InstNumber", HFILL }},
    { &hf_oep_idLabelString,
      { "idLabelString", "oep.idLabelString",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_idModel,
      { "idModel", "oep.idModel_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SystemModel", HFILL }},
    { &hf_oep_idPhysio,
      { "idPhysio", "oep.idPhysio",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_idProdSpecn,
      { "idProdSpecn", "oep.idProdSpecn",
        FT_UINT32, BASE_DEC, NULL, 0,
        "ProductionSpec", HFILL }},
    { &hf_oep_idType,
      { "idType", "oep.idType_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "TYPE", HFILL }},
    { &hf_oep_metricStoreUsageCount,
      { "metricStoreUsageCount", "oep.metricStoreUsageCount",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_measurementStatus,
      { "measurementStatus", "oep.measurementStatus",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_nuAccurMsmt,
      { "nuAccurMsmt", "oep.nuAccurMsmt",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_nuObsValueCmp,
      { "nuObsValueCmp", "oep.nuObsValueCmp",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_nuObsValue,
      { "nuObsValue", "oep.nuObsValue_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segmentNumber,
      { "segmentNumber", "oep.segmentNumber",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_operationalState,
      { "operationalState", "oep.operationalState",
        FT_UINT16, BASE_DEC, VALS(oep_OperationalState_vals), 0,
        NULL, HFILL }},
    { &hf_oep_powerStatus,
      { "powerStatus", "oep.powerStatus",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_saSpec,
      { "saSpec", "oep.saSpec_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scaleRangeSpec16,
      { "scaleRangeSpec16", "oep.scaleRangeSpec16_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scaleRangeSpec32,
      { "scaleRangeSpec32", "oep.scaleRangeSpec32_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scaleRangeSpec8,
      { "scaleRangeSpec8", "oep.scaleRangeSpec8_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanRepPD,
      { "scanRepPD", "oep.scanRepPD",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_segmentUsageCount,
      { "segmentUsageCount", "oep.segmentUsageCount",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_systemId,
      { "systemId", "oep.systemId",
        FT_STRING, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_systemType,
      { "systemType", "oep.systemType_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "TYPE", HFILL }},
    { &hf_oep_absoluteTime,
      { "absoluteTime", "oep.absoluteTime_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_batMeasure,
      { "batMeasure", "oep.batMeasure_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_endTimeSegment,
      { "endTimeSegment", "oep.endTimeSegment_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_pdTimeSample,
      { "pdTimeSample", "oep.pdTimeSample",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_relativeTime,
      { "relativeTime", "oep.relativeTime",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_timeStampAbsolute,
      { "timeStampAbsolute", "oep.timeStampAbsolute_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_timeStampRelative,
      { "timeStampRelative", "oep.timeStampRelative",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_startTimeSegment,
      { "startTimeSegment", "oep.startTimeSegment_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_transmitWIndow,
      { "transmitWIndow", "oep.transmitWIndow",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_unitCode,
      { "unitCode", "oep.unitCode",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_unitLable,
      { "unitLable", "oep.unitLable",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_batChargeValue,
      { "batChargeValue", "oep.batChargeValue",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_enumObsValue,
      { "enumObsValue", "oep.enumObsValue_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_highResRelativeTime,
      { "highResRelativeTime", "oep.highResRelativeTime",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_timeStampRelativeHighRes,
      { "timeStampRelativeHighRes", "oep.timeStampRelativeHighRes",
        FT_BYTES, BASE_NONE, NULL, 0,
        "HighResRelativeTime", HFILL }},
    { &hf_oep_authChallenge,
      { "authChallenge", "oep.authChallenge",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_authResponse,
      { "authResponse", "oep.authResponse",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_configId,
      { "configId", "oep.configId",
        FT_UINT16, BASE_DEC, VALS(oep_ConfigId_vals), 0,
        NULL, HFILL }},
    { &hf_oep_mdsTimeInfo,
      { "mdsTimeInfo", "oep.mdsTimeInfo_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_metricSpecSmall,
      { "metricSpecSmall", "oep.metricSpecSmall",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_sourceHandleReference,
      { "sourceHandleReference", "oep.sourceHandleReference",
        FT_UINT32, BASE_DEC, NULL, 0,
        "HANDLE", HFILL }},
    { &hf_oep_simpleSaObsValue,
      { "simpleSaObsValue", "oep.simpleSaObsValue",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_observedValueSimpleOID,
      { "observedValueSimpleOID", "oep.observedValueSimpleOID",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_observedValueSimpleString,
      { "observedValueSimpleString", "oep.observedValueSimpleString",
        FT_BYTES, BASE_NONE, NULL, 0,
        "EnumPrintableString", HFILL }},
    { &hf_oep_regCertDataList,
      { "regCertDataList", "oep.regCertDataList",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_basicNuObsValue,
      { "basicNuObsValue", "oep.basicNuObsValue",
        FT_FLOAT, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_pmSegmentEntryMap,
      { "pmSegmentEntryMap", "oep.pmSegmentEntryMap_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_personId,
      { "personId", "oep.personId",
        FT_UINT16, BASE_DEC, VALS(oep_PersonId_vals), 0,
        NULL, HFILL }},
    { &hf_oep_segmentStatistics,
      { "segmentStatistics", "oep.segmentStatistics",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanHandleAttrValMap,
      { "scanHandleAttrValMap", "oep.scanHandleAttrValMap",
        FT_UINT32, BASE_DEC, NULL, 0,
        "HandleAttrValMap", HFILL }},
    { &hf_oep_scanRepPDMin,
      { "scanRepPDMin", "oep.scanRepPDMin",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_attrValMap,
      { "attrValMap", "oep.attrValMap",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_simpleNuObsValue,
      { "simpleNuObsValue", "oep.simpleNuObsValue",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_pmStoreLabel,
      { "pmStoreLabel", "oep.pmStoreLabel",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_pmSegmentLabel,
      { "pmSegmentLabel", "oep.pmSegmentLabel",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_pdTimeMsmtActive,
      { "pdTimeMsmtActive", "oep.pdTimeMsmtActive",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_systemTypeVerList,
      { "systemTypeVerList", "oep.systemTypeVerList",
        FT_UINT32, BASE_DEC, NULL, 0,
        "TypeVerList", HFILL }},
    { &hf_oep_metricIdPartition,
      { "metricIdPartition", "oep.metricIdPartition",
        FT_UINT16, BASE_DEC, VALS(oep_NomPartition_vals), 0,
        "NomPartition", HFILL }},
    { &hf_oep_observedValuePartition,
      { "observedValuePartition", "oep.observedValuePartition",
        FT_UINT16, BASE_DEC, VALS(oep_NomPartition_vals), 0,
        "NomPartition", HFILL }},
    { &hf_oep_supplementalTypeList,
      { "supplementalTypeList", "oep.supplementalTypeList",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_absoluteTimeAdjust,
      { "absoluteTimeAdjust", "oep.absoluteTimeAdjust",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_clearTimeout,
      { "clearTimeout", "oep.clearTimeout",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_transferTimeout,
      { "transferTimeout", "oep.transferTimeout",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_observedValueSimple,
      { "observedValueSimple", "oep.observedValueSimple",
        FT_BYTES, BASE_NONE, NULL, 0,
        "BITS_32", HFILL }},
    { &hf_oep_observedValueBasic,
      { "observedValueBasic", "oep.observedValueBasic",
        FT_BYTES, BASE_NONE, NULL, 0,
        "BITS_16", HFILL }},
    { &hf_oep_metricStructureSmall,
      { "metricStructureSmall", "oep.metricStructureSmall_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_simpleNuObsValueCmp,
      { "simpleNuObsValueCmp", "oep.simpleNuObsValueCmp",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_basicNuObsValueCmp,
      { "basicNuObsValueCmp", "oep.basicNuObsValueCmp",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_idPhysioList,
      { "idPhysioList", "oep.idPhysioList",
        FT_UINT32, BASE_DEC, NULL, 0,
        "MetricIdList", HFILL }},
    { &hf_oep_scanHandleList,
      { "scanHandleList", "oep.scanHandleList",
        FT_UINT32, BASE_DEC, NULL, 0,
        "HANDLEList", HFILL }},
    { &hf_oep_AttributeList_item,
      { "AVA-Type", "oep.AVA_Type_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_AttributeIdList_item,
      { "OID-Type", "oep.OID_Type",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_century,
      { "century", "oep.century",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_year,
      { "year", "oep.year",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_month,
      { "month", "oep.month",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_day,
      { "day", "oep.day",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_hour,
      { "hour", "oep.hour",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_minute,
      { "minute", "oep.minute",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_second,
      { "second", "oep.second",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_sec_fractions,
      { "sec-fractions", "oep.sec_fractions",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_bo_seconds,
      { "bo-seconds", "oep.bo_seconds",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_bo_fraction,
      { "bo-fraction", "oep.bo_fraction",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_bo_time_offset,
      { "bo-time-offset", "oep.bo_time_offset",
        FT_INT32, BASE_DEC, NULL, 0,
        "INT_I16", HFILL }},
    { &hf_oep_manufacturer,
      { "manufacturer", "oep.manufacturer",
        FT_STRING, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_model_number,
      { "model-number", "oep.model_number",
        FT_STRING, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_ProductionSpec_item,
      { "ProdSpecEntry", "oep.ProdSpecEntry_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_prodSpecEntry_specType,
      { "spec-type", "oep.spec_type",
        FT_UINT16, BASE_DEC, VALS(oep_ProdSpecEntry_specType_vals), 0,
        "ProdSpecEntry_specType", HFILL }},
    { &hf_oep_component_id,
      { "component-id", "oep.component_id",
        FT_UINT32, BASE_DEC, NULL, 0,
        "PrivateOid", HFILL }},
    { &hf_oep_prod_spec,
      { "prod-spec", "oep.prod_spec",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_batMeasure_value,
      { "value", "oep.value",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_unit,
      { "unit", "oep.unit",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_metric_id,
      { "metric-id", "oep.metric_id",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_state,
      { "state", "oep.state",
        FT_BYTES, BASE_NONE, NULL, 0,
        "MeasurementStatus", HFILL }},
    { &hf_oep_unit_code,
      { "unit-code", "oep.unit_code",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_nuObsValue_value,
      { "value", "oep.value",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_NuObsValueCmp_item,
      { "NuObsValue", "oep.NuObsValue_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_array_size,
      { "array-size", "oep.array_size",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_sample_type,
      { "sample-type", "oep.sample_type_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SampleType", HFILL }},
    { &hf_oep_flags,
      { "flags", "oep.flags",
        FT_BYTES, BASE_NONE, NULL, 0,
        "SaFlags", HFILL }},
    { &hf_oep_sample_size,
      { "sample-size", "oep.sample_size",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_sampleType_significantBits,
      { "significant-bits", "oep.significant_bits",
        FT_UINT8, BASE_DEC, VALS(oep_SampleType_significantBits_vals), 0,
        "SampleType_significantBits", HFILL }},
    { &hf_oep_lower_absolute_value,
      { "lower-absolute-value", "oep.lower_absolute_value",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_upper_absolute_value,
      { "upper-absolute-value", "oep.upper_absolute_value",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_scaleRangeSpec8_lowerScaledValue,
      { "lower-scaled-value", "oep.lower_scaled_value",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_scaleRangeSpec8_upperScaledValue,
      { "upper-scaled-value", "oep.upper_scaled_value",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_scaleRangeSpec16_lowerScaledValue,
      { "lower-scaled-value", "oep.lower_scaled_value",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_scaleRangeSpec16_upperScaledValue,
      { "upper-scaled-value", "oep.upper_scaled_value",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_scaleRangeSpec32_lowerScaledValue,
      { "lower-scaled-value", "oep.lower_scaled_value",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_scaleRangeSpec32_upperScaledValue,
      { "upper-scaled-value", "oep.upper_scaled_value",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_enumObsValue_value,
      { "value", "oep.value",
        FT_UINT32, BASE_DEC, VALS(oep_EnumVal_vals), 0,
        "EnumVal", HFILL }},
    { &hf_oep_enum_obj_id,
      { "enum-obj-id", "oep.enum_obj_id",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_enum_text_string,
      { "enum-text-string", "oep.enum_text_string",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_enum_bit_str,
      { "enum-bit-str", "oep.enum_bit_str",
        FT_BYTES, BASE_NONE, NULL, 0,
        "BITS_16", HFILL }},
    { &hf_oep_setTimeInvoke_dateTime,
      { "date-time", "oep.date_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_accuracy,
      { "accuracy", "oep.accuracy",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "FLOAT_Type", HFILL }},
    { &hf_oep_setBOTimeInvoke_dateTime,
      { "date-time", "oep.date_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "BaseOffsetTime", HFILL }},
    { &hf_oep_all_segments,
      { "all-segments", "oep.all_segments",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_segm_id_list,
      { "segm-id-list", "oep.segm_id_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SegmIdList", HFILL }},
    { &hf_oep_abs_time_range,
      { "abs-time-range", "oep.abs_time_range_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsTimeRange", HFILL }},
    { &hf_oep_bo_time_range,
      { "bo-time-range", "oep.bo_time_range_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "BOTimeRange", HFILL }},
    { &hf_oep_SegmIdList_item,
      { "InstNumber", "oep.InstNumber",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_absTimeRange_fromTime,
      { "from-time", "oep.from_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_absTimeRange_toTime,
      { "to-time", "oep.to_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_bOTimeRange_fromTime,
      { "from-time", "oep.from_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "BaseOffsetTime", HFILL }},
    { &hf_oep_bOTimeRange_toTime,
      { "to-time", "oep.to_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "BaseOffsetTime", HFILL }},
    { &hf_oep_SegmentInfoList_item,
      { "SegmentInfo", "oep.SegmentInfo_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_seg_inst_no,
      { "seg-inst-no", "oep.seg_inst_no",
        FT_UINT32, BASE_DEC, NULL, 0,
        "InstNumber", HFILL }},
    { &hf_oep_seg_info,
      { "seg-info", "oep.seg_info",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttributeList", HFILL }},
    { &hf_oep_obj_handle,
      { "obj-handle", "oep.obj_handle",
        FT_UINT32, BASE_DEC, NULL, 0,
        "HANDLE", HFILL }},
    { &hf_oep_attributes,
      { "attributes", "oep.attributes",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttributeList", HFILL }},
    { &hf_oep_aarq,
      { "aarq", "oep.aarq_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AarqApdu", HFILL }},
    { &hf_oep_aare,
      { "aare", "oep.aare_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AareApdu", HFILL }},
    { &hf_oep_rlrq,
      { "rlrq", "oep.rlrq_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "RlrqApdu", HFILL }},
    { &hf_oep_rlre,
      { "rlre", "oep.rlre_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "RlreApdu", HFILL }},
    { &hf_oep_abrt,
      { "abrt", "oep.abrt_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbrtApdu", HFILL }},
    { &hf_oep_prst,
      { "prst", "oep.prst_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "PrstApdu", HFILL }},
    { &hf_oep_atrg,
      { "atrg", "oep.atrg_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AtrgApdu", HFILL }},
    { &hf_oep_assoc_version,
      { "assoc-version", "oep.assoc_version",
        FT_BYTES, BASE_NONE, NULL, 0,
        "AssociationVersion", HFILL }},
    { &hf_oep_data_proto_list,
      { "data-proto-list", "oep.data_proto_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "DataProtoList", HFILL }},
    { &hf_oep_DataProtoList_item,
      { "DataProto", "oep.DataProto_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_data_proto_id,
      { "data-proto-id", "oep.data_proto_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataProtoId_vals), 0,
        "DataProtoId", HFILL }},
    { &hf_oep_data_proto_info,
      { "data-proto-info", "oep.data_proto_info",
        FT_UINT32, BASE_DEC, VALS(oep_DataProtoInfo_vals), 0,
        "DataProtoInfo", HFILL }},
    { &hf_oep_phdAssociationInformation,
      { "phdAssociationInformation", "oep.phdAssociationInformation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_manufSpecAssociationInformation,
      { "manufSpecAssociationInformation", "oep.manufSpecAssociationInformation_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_empty,
      { "empty", "oep.empty_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_result,
      { "result", "oep.result",
        FT_UINT16, BASE_DEC, VALS(oep_AssociateResult_vals), 0,
        "AssociateResult", HFILL }},
    { &hf_oep_selected_data_proto,
      { "selected-data-proto", "oep.selected_data_proto_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "DataProto", HFILL }},
    { &hf_oep_rlrqApdu_reason,
      { "reason", "oep.reason",
        FT_UINT16, BASE_DEC, VALS(oep_ReleaseRequestReason_vals), 0,
        "ReleaseRequestReason", HFILL }},
    { &hf_oep_rlreApdu_reason,
      { "reason", "oep.reason",
        FT_UINT16, BASE_DEC, VALS(oep_ReleaseResponseReason_vals), 0,
        "ReleaseResponseReason", HFILL }},
    { &hf_oep_abrtApdu_reason,
      { "reason", "oep.reason",
        FT_UINT16, BASE_DEC, VALS(oep_Abort_reason_vals), 0,
        "Abort_reason", HFILL }},
    { &hf_oep_protocol_version,
      { "protocol-version", "oep.protocol_version",
        FT_BYTES, BASE_NONE, NULL, 0,
        "ProtocolVersion", HFILL }},
    { &hf_oep_encoding_rules,
      { "encoding-rules", "oep.encoding_rules",
        FT_BYTES, BASE_NONE, NULL, 0,
        "EncodingRules", HFILL }},
    { &hf_oep_nomenclature_version,
      { "nomenclature-version", "oep.nomenclature_version",
        FT_BYTES, BASE_NONE, NULL, 0,
        "NomenclatureVersion", HFILL }},
    { &hf_oep_functional_units,
      { "functional-units", "oep.functional_units",
        FT_BYTES, BASE_NONE, NULL, 0,
        "FunctionalUnits", HFILL }},
    { &hf_oep_system_type,
      { "system-type", "oep.system_type",
        FT_BYTES, BASE_NONE, NULL, 0,
        "SystemType", HFILL }},
    { &hf_oep_system_id,
      { "system-id", "oep.system_id",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_dev_config_id,
      { "dev-config-id", "oep.dev_config_id",
        FT_UINT16, BASE_DEC, VALS(oep_ConfigId_vals), 0,
        "ConfigId", HFILL }},
    { &hf_oep_data_req_mode_capab,
      { "data-req-mode-capab", "oep.data_req_mode_capab_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "DataReqModeCapab", HFILL }},
    { &hf_oep_option_list,
      { "option-list", "oep.option_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttributeList", HFILL }},
    { &hf_oep_data_proto_id_ext,
      { "data-proto-id-ext", "oep.data_proto_id_ext",
        FT_BYTES, BASE_NONE, NULL, 0,
        "UuidIdent", HFILL }},
    { &hf_oep_data_proto_info_ext,
      { "data-proto-info-ext", "oep.data_proto_info_ext",
        FT_BYTES, BASE_NONE, NULL, 0,
        "UuidIdent", HFILL }},
    { &hf_oep_optionList,
      { "optionList", "oep.optionList",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttributeList", HFILL }},
    { &hf_oep_invoke_id,
      { "invoke-id", "oep.invoke_id",
        FT_UINT16, BASE_HEX, NULL, 0,
        "InvokeIDType", HFILL }},
    { &hf_oep_dataApdu_message,
      { "message", "oep.message",
        FT_UINT16, BASE_DEC, VALS(oep_DataApdu_message_vals), 0,
        "DataApdu_message", HFILL }},
    { &hf_oep_roiv_cmip_event_report,
      { "roiv-cmip-event-report", "oep.roiv_cmip_event_report_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "EventReportArgumentSimple", HFILL }},
    { &hf_oep_roiv_cmip_confirmed_event_report,
      { "roiv-cmip-confirmed-event-report", "oep.roiv_cmip_confirmed_event_report_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "EventReportArgumentSimple", HFILL }},
    { &hf_oep_roiv_cmip_get,
      { "roiv-cmip-get", "oep.roiv_cmip_get_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "GetArgumentSimple", HFILL }},
    { &hf_oep_roiv_cmip_set,
      { "roiv-cmip-set", "oep.roiv_cmip_set_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SetArgumentSimple", HFILL }},
    { &hf_oep_roiv_cmip_confirmed_set,
      { "roiv-cmip-confirmed-set", "oep.roiv_cmip_confirmed_set_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SetArgumentSimple", HFILL }},
    { &hf_oep_roiv_cmip_action,
      { "roiv-cmip-action", "oep.roiv_cmip_action_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "ActionArgumentSimple", HFILL }},
    { &hf_oep_roiv_cmip_confirmed_action,
      { "roiv-cmip-confirmed-action", "oep.roiv_cmip_confirmed_action_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "ActionArgumentSimple", HFILL }},
    { &hf_oep_rors_cmip_confirmed_event_report,
      { "rors-cmip-confirmed-event-report", "oep.rors_cmip_confirmed_event_report_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "EventReportResultSimple", HFILL }},
    { &hf_oep_rors_cmip_get,
      { "rors-cmip-get", "oep.rors_cmip_get_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "GetResultSimple", HFILL }},
    { &hf_oep_rors_cmip_confirmed_set,
      { "rors-cmip-confirmed-set", "oep.rors_cmip_confirmed_set_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SetResultSimple", HFILL }},
    { &hf_oep_rors_cmip_confirmed_action,
      { "rors-cmip-confirmed-action", "oep.rors_cmip_confirmed_action_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "ActionResultSimple", HFILL }},
    { &hf_oep_roer,
      { "roer", "oep.roer_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "ErrorResult", HFILL }},
    { &hf_oep_rorj,
      { "rorj", "oep.rorj_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "RejectResult", HFILL }},
    { &hf_oep_error_value,
      { "error-value", "oep.error_value",
        FT_UINT16, BASE_DEC, VALS(oep_RoerErrorValue_vals), 0,
        "RoerErrorValue", HFILL }},
    { &hf_oep_parameter,
      { "parameter", "oep.parameter",
        FT_UINT16, BASE_DEC, VALS(oep_RoerErrorValue_vals), 0,
        "RoerErrorValue", HFILL }},
    { &hf_oep_problem,
      { "problem", "oep.problem",
        FT_UINT16, BASE_DEC, VALS(oep_RorjProblem_vals), 0,
        "RorjProblem", HFILL }},
    { &hf_oep_event_time,
      { "event-time", "oep.event_time",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_eventReportArgumentSimple_eventType,
      { "event-type", "oep.event_type",
        FT_UINT16, BASE_DEC, VALS(oep_EventReportInfo_vals), 0,
        "EventReportArgumentSimplet_eventType", HFILL }},
    { &hf_oep_eventReportArgumentSimple_eventInfo,
      { "event-info", "oep.event_info",
        FT_UINT32, BASE_DEC, VALS(oep_EventReportInfo_vals), 0,
        "EventReportArgumentSimple_eventInfo", HFILL }},
    { &hf_oep_configReport,
      { "configReport", "oep.configReport_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoFixed,
      { "scanReportInfoFixed", "oep.scanReportInfoFixed_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoVar,
      { "scanReportInfoVar", "oep.scanReportInfoVar_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoMPFixed,
      { "scanReportInfoMPFixed", "oep.scanReportInfoMPFixed_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoMPVar,
      { "scanReportInfoMPVar", "oep.scanReportInfoMPVar_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segmentDataEvent,
      { "segmentDataEvent", "oep.segmentDataEvent_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_currentTime,
      { "currentTime", "oep.currentTime",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_eventReportResultSimple_eventType,
      { "event-type", "oep.event_type",
        FT_UINT16, BASE_DEC, VALS(oep_EventReplyInfo_vals), 0,
        "EventReportResultSimple_eventType", HFILL }},
    { &hf_oep_event_reply_info,
      { "event-reply-info", "oep.event_reply_info",
        FT_UINT16, BASE_DEC, VALS(oep_EventReplyInfo_vals), 0,
        "EventReplyInfo", HFILL }},
    { &hf_oep_configReportRsp,
      { "configReportRsp", "oep.configReportRsp_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segmentDataResult,
      { "segmentDataResult", "oep.segmentDataResult_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_attribute_id_list,
      { "attribute-id-list", "oep.attribute_id_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttributeIdList", HFILL }},
    { &hf_oep_attribute_list,
      { "attribute-list", "oep.attribute_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttributeList", HFILL }},
    { &hf_oep_TypeVerList_item,
      { "TypeVer", "oep.TypeVer_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_type,
      { "type", "oep.type",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_version,
      { "version", "oep.version",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_modification_list,
      { "modification-list", "oep.modification_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "ModificationList", HFILL }},
    { &hf_oep_ModificationList_item,
      { "AttributeModEntry", "oep.AttributeModEntry_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_modify_operator,
      { "modify-operator", "oep.modify_operator",
        FT_UINT16, BASE_DEC, VALS(oep_ModifyOperator_vals), 0,
        "ModifyOperator", HFILL }},
    { &hf_oep_attribute,
      { "attribute", "oep.attribute_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AVA_Type", HFILL }},
    { &hf_oep_action_type,
      { "action-type", "oep.action_type",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_actionArgumentSimple_actionInfoArgs,
      { "action-info-args", "oep.action_info_args",
        FT_UINT32, BASE_DEC, VALS(oep_ActionArgumentSimpleInfoArgs_vals), 0,
        "ActionArgumentSimple_actionInfoArgs", HFILL }},
    { &hf_oep_dataRequest,
      { "dataRequest", "oep.dataRequest_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_setTimeInvoke,
      { "setTimeInvoke", "oep.setTimeInvoke_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_setBOTimeInvoke,
      { "setBOTimeInvoke", "oep.setBOTimeInvoke_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segmSelection,
      { "segmSelection", "oep.segmSelection",
        FT_UINT32, BASE_DEC, VALS(oep_SegmSelection_vals), 0,
        NULL, HFILL }},
    { &hf_oep_trigSegmDataXferReq,
      { "trigSegmDataXferReq", "oep.trigSegmDataXferReq_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_actionResultSimple_actionInfoArgs,
      { "action-info-args", "oep.action_info_args",
        FT_UINT32, BASE_DEC, VALS(oep_ActionResultSimpleInfoArgs_vals), 0,
        "ActionResultSimple_actionInfoArgs", HFILL }},
    { &hf_oep_dataResponse,
      { "dataResponse", "oep.dataResponse_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segmentInfoList,
      { "segmentInfoList", "oep.segmentInfoList",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_trigSegmDataXferRsp,
      { "trigSegmDataXferRsp", "oep.trigSegmDataXferRsp_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_AttrValMap_item,
      { "AttrValMapEntry", "oep.AttrValMapEntry_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_attribute_len,
      { "attribute-len", "oep.attribute_len",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_mds_time_cap_state,
      { "mds-time-cap-state", "oep.mds_time_cap_state",
        FT_BYTES, BASE_NONE, NULL, 0,
        "MdsTimeCapState", HFILL }},
    { &hf_oep_time_sync_protocol,
      { "time-sync-protocol", "oep.time_sync_protocol",
        FT_UINT32, BASE_DEC, NULL, 0,
        "TimeProtocolId", HFILL }},
    { &hf_oep_time_sync_accuracy,
      { "time-sync-accuracy", "oep.time_sync_accuracy",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_time_resolution_abs_time,
      { "time-resolution-abs-time", "oep.time_resolution_abs_time",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_time_resolution_rel_time,
      { "time-resolution-rel-time", "oep.time_resolution_rel_time",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_time_resolution_high_res_time,
      { "time-resolution-high-res-time", "oep.time_resolution_high_res_time",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_RegCertDataList_item,
      { "RegCertData", "oep.RegCertData_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_auth_body_and_struc_type,
      { "auth-body-and-struc-type", "oep.auth_body_and_struc_type_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AuthBodyAndStrucType", HFILL }},
    { &hf_oep_auth_body_data,
      { "auth-body-data", "oep.auth_body_data_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AuthBodyAndStrucType", HFILL }},
    { &hf_oep_auth_body,
      { "auth-body", "oep.auth_body",
        FT_UINT8, BASE_DEC, VALS(oep_AuthBody_vals), 0,
        "AuthBody", HFILL }},
    { &hf_oep_auth_body_struc_type,
      { "auth-body-struc-type", "oep.auth_body_struc_type",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AuthBodyStrucType", HFILL }},
    { &hf_oep_SupplementalTypeList_item,
      { "TYPE", "oep.TYPE_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_metricStructureSmall_msStruct,
      { "ms-struct", "oep.ms_struct",
        FT_UINT8, BASE_DEC, VALS(oep_MetricStructureSmall_msStruct_vals), 0,
        "MetricStructureSmall_msStruct", HFILL }},
    { &hf_oep_ms_comp_no,
      { "ms-comp-no", "oep.ms_comp_no",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_MetricIdList_item,
      { "OID-Type", "oep.OID_Type",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_HandleAttrValMap_item,
      { "HandleAttrValMapEntry", "oep.HandleAttrValMapEntry_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_attr_val_map,
      { "attr-val-map", "oep.attr_val_map",
        FT_UINT32, BASE_DEC, NULL, 0,
        "AttrValMap", HFILL }},
    { &hf_oep_HANDLEList_item,
      { "HANDLE", "oep.HANDLE",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoVar_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_scan_report_no,
      { "scan-report-no", "oep.scan_report_no",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_obs_scan_var,
      { "obs-scan-var", "oep.obs_scan_var",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_ObservationScan", HFILL }},
    { &hf_oep_obs_scan_var_item,
      { "ObservationScan", "oep.ObservationScan_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoFixed_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_obs_scan_fixed,
      { "obs-scan-fixed", "oep.obs_scan_fixed",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_ObservationScanFixed", HFILL }},
    { &hf_oep_obs_scan_fixed_item,
      { "ObservationScanFixed", "oep.ObservationScanFixed_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_obs_val_data,
      { "obs-val-data", "oep.obs_val_data_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "ObservationValueData", HFILL }},
    { &hf_oep_observed_val,
      { "observed-val", "oep.observed_val",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        "SimpleNuObsValue", HFILL }},
    { &hf_oep_absolute_time,
      { "absolute-time", "oep.absolute_time_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "AbsoluteTime", HFILL }},
    { &hf_oep_scanReportInfoGrouped_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_scanReportInfoGrouped_obsScanGrouped,
      { "obs-scan-grouped", "oep.obs_scan_grouped",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_ObservationScanGrouped", HFILL }},
    { &hf_oep_scanReportInfoGrouped_obsScanGrouped_item,
      { "ObservationScanGrouped", "oep.ObservationScanGrouped",
        FT_BYTES, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoMPVar_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_scan_per_var,
      { "scan-per-var", "oep.scan_per_var",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_ScanReportPerVar", HFILL }},
    { &hf_oep_scan_per_var_item,
      { "ScanReportPerVar", "oep.ScanReportPerVar_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_person_id,
      { "person-id", "oep.person_id",
        FT_UINT16, BASE_DEC, VALS(oep_PersonId_vals), 0,
        "PersonId", HFILL }},
    { &hf_oep_scanReportInfoMPFixed_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_scan_per_fixed,
      { "scan-per-fixed", "oep.scan_per_fixed",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_ScanReportPerFixed", HFILL }},
    { &hf_oep_scan_per_fixed_item,
      { "ScanReportPerFixed", "oep.ScanReportPerFixed_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportInfoMPGrouped_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_scan_per_grouped,
      { "scan-per-grouped", "oep.scan_per_grouped",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SEQUENCE_OF_ScanReportPerGrouped", HFILL }},
    { &hf_oep_scan_per_grouped_item,
      { "ScanReportPerGrouped", "oep.ScanReportPerGrouped_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_scanReportPerGrouped_obsScanGrouped,
      { "obs-scan-grouped", "oep.obs_scan_grouped",
        FT_BYTES, BASE_NONE, NULL, 0,
        "ObservationScanGrouped", HFILL }},
    { &hf_oep_config_report_id,
      { "config-report-id", "oep.config_report_id",
        FT_UINT16, BASE_DEC, VALS(oep_ConfigId_vals), 0,
        "ConfigId", HFILL }},
    { &hf_oep_config_obj_list,
      { "config-obj-list", "oep.config_obj_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "ConfigObjectList", HFILL }},
    { &hf_oep_ConfigObjectList_item,
      { "ConfigObject", "oep.ConfigObject_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_obj_class,
      { "obj-class", "oep.obj_class",
        FT_UINT32, BASE_DEC, NULL, 0,
        "ConfigObject_objClass", HFILL }},
    { &hf_oep_config_result,
      { "config-result", "oep.config_result",
        FT_UINT16, BASE_DEC, VALS(oep_ConfigResult_vals), 0,
        "ConfigResult", HFILL }},
    { &hf_oep_dataRequest_dataReqId,
      { "data-req-id", "oep.data_req_id",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqId_vals), 0,
        "DataReqId", HFILL }},
    { &hf_oep_data_req_mode,
      { "data-req-mode", "oep.data_req_mode",
        FT_BYTES, BASE_NONE, NULL, 0,
        "DataReqMode", HFILL }},
    { &hf_oep_data_req_time,
      { "data-req-time", "oep.data_req_time",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_data_req_person_id,
      { "data-req-person-id", "oep.data_req_person_id",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U16", HFILL }},
    { &hf_oep_data_req_class,
      { "data-req-class", "oep.data_req_class",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_data_req_obj_handle_list,
      { "data-req-obj-handle-list", "oep.data_req_obj_handle_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "HANDLEList", HFILL }},
    { &hf_oep_data_req_mode_flags,
      { "data-req-mode-flags", "oep.data_req_mode_flags",
        FT_BYTES, BASE_NONE, NULL, 0,
        "DataReqModeFlags", HFILL }},
    { &hf_oep_data_req_init_agent_count,
      { "data-req-init-agent-count", "oep.data_req_init_agent_count",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_data_req_init_manager_count,
      { "data-req-init-manager-count", "oep.data_req_init_manager_count",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U8", HFILL }},
    { &hf_oep_rel_time_stamp,
      { "rel-time-stamp", "oep.rel_time_stamp",
        FT_UINT32, BASE_DEC, NULL, 0,
        "RelativeTime", HFILL }},
    { &hf_oep_data_req_result,
      { "data-req-result", "oep.data_req_result",
        FT_UINT16, BASE_DEC, VALS(oep_DataReqResult_vals), 0,
        "DataReqResult", HFILL }},
    { &hf_oep_event_type,
      { "event-type", "oep.event_type",
        FT_UINT16, BASE_DEC, VALS(oep_DataResponseEventInfo_vals), 0,
        "DateResponse_eventType", HFILL }},
    { &hf_oep_dataResponse_eventInfo,
      { "event-info", "oep.event_info",
        FT_UINT32, BASE_DEC, VALS(oep_DataResponseEventInfo_vals), 0,
        "DataResponse_eventInfo", HFILL }},
    { &hf_oep_SimpleNuObsValueCmp_item,
      { "SimpleNuObsValue", "oep.SimpleNuObsValue",
        FT_DOUBLE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_BasicNuObsValueCmp_item,
      { "BasicNuObsValue", "oep.BasicNuObsValue",
        FT_FLOAT, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segm_entry_header,
      { "segm-entry-header", "oep.segm_entry_header",
        FT_BYTES, BASE_NONE, NULL, 0,
        "SegmEntryHeader", HFILL }},
    { &hf_oep_segm_entry_elem_list,
      { "segm-entry-elem-list", "oep.segm_entry_elem_list",
        FT_UINT32, BASE_DEC, NULL, 0,
        "SegmEntryElemList", HFILL }},
    { &hf_oep_SegmEntryElemList_item,
      { "SegmEntryElem", "oep.SegmEntryElem_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_class_id,
      { "class-id", "oep.class_id",
        FT_UINT32, BASE_DEC, NULL, 0,
        "OID_Type", HFILL }},
    { &hf_oep_metric_type,
      { "metric-type", "oep.metric_type_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "TYPE", HFILL }},
    { &hf_oep_handle,
      { "handle", "oep.handle",
        FT_UINT32, BASE_DEC, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_trig_segm_xfer_rsp,
      { "trig-segm-xfer-rsp", "oep.trig_segm_xfer_rsp",
        FT_UINT16, BASE_DEC, VALS(oep_TrigSegmXferRsp_vals), 0,
        "TrigSegmXferRsp", HFILL }},
    { &hf_oep_segm_data_event_descr,
      { "segm-data-event-descr", "oep.segm_data_event_descr_element",
        FT_NONE, BASE_NONE, NULL, 0,
        "SegmDataEventDescr", HFILL }},
    { &hf_oep_segm_data_event_entries,
      { "segm-data-event-entries", "oep.segm_data_event_entries",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_segm_instance,
      { "segm-instance", "oep.segm_instance",
        FT_UINT32, BASE_DEC, NULL, 0,
        "InstNumber", HFILL }},
    { &hf_oep_segm_evt_entry_index,
      { "segm-evt-entry-index", "oep.segm_evt_entry_index",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_segm_evt_entry_count,
      { "segm-evt-entry-count", "oep.segm_evt_entry_count",
        FT_UINT32, BASE_DEC, NULL, 0,
        "INT_U32", HFILL }},
    { &hf_oep_segm_evt_status,
      { "segm-evt-status", "oep.segm_evt_status",
        FT_BYTES, BASE_NONE, NULL, 0,
        "SegmEvtStatus", HFILL }},
    { &hf_oep_SegmentStatistics_item,
      { "SegmentStatisticEntry", "oep.SegmentStatisticEntry_element",
        FT_NONE, BASE_NONE, NULL, 0,
        NULL, HFILL }},
    { &hf_oep_segm_stat_type,
      { "segm-stat-type", "oep.segm_stat_type",
        FT_UINT16, BASE_DEC, VALS(oep_SegmStatType_vals), 0,
        "SegmStatType", HFILL }},
    { &hf_oep_segm_stat_entry,
      { "segm-stat-entry", "oep.segm_stat_entry",
        FT_BYTES, BASE_NONE, NULL, 0,
        "OCTET_STRING", HFILL }},
    { &hf_oep_PowerStatus_onMains,
      { "onMains", "oep.onMains",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_PowerStatus_onBattery,
      { "onBattery", "oep.onBattery",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_PowerStatus_chargingFull,
      { "chargingFull", "oep.chargingFull",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_PowerStatus_chargingTrickle,
      { "chargingTrickle", "oep.chargingTrickle",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_PowerStatus_chargingOff,
      { "chargingOff", "oep.chargingOff",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_invalid,
      { "invalid", "oep.invalid",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_questionable,
      { "questionable", "oep.questionable",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_not_available,
      { "not-available", "oep.not-available",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_calibration_ongoing,
      { "calibration-ongoing", "oep.calibration-ongoing",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_test_data,
      { "test-data", "oep.test-data",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_demo_data,
      { "demo-data", "oep.demo-data",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_validated_data,
      { "validated-data", "oep.validated-data",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_early_indication,
      { "early-indication", "oep.early-indication",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_MeasurementStatus_msmt_ongoing,
      { "msmt-ongoing", "oep.msmt-ongoing",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_SaFlags_smooth_curve,
      { "smooth-curve", "oep.smooth-curve",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_SaFlags_delayed_curve,
      { "delayed-curve", "oep.delayed-curve",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_SaFlags_static_scale,
      { "static-scale", "oep.static-scale",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_SaFlags_sa_ext_val_range,
      { "sa-ext-val-range", "oep.sa-ext-val-range",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_AssociationVersion_assoc_version1,
      { "assoc-version1", "oep.assoc-version1",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_ProtocolVersion_protocol_version1,
      { "protocol-version1", "oep.protocol-version1",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_ProtocolVersion_protocol_version2,
      { "protocol-version2", "oep.protocol-version2",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_EncodingRules_mder,
      { "mder", "oep.mder",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_EncodingRules_xer,
      { "xer", "oep.xer",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_EncodingRules_per,
      { "per", "oep.per",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_NomenclatureVersion_nom_version1,
      { "nom-version1", "oep.nom-version1",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_FunctionalUnits_fun_units_unidirectional,
      { "fun-units-unidirectional", "oep.fun-units-unidirectional",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_FunctionalUnits_fun_units_havetestcap,
      { "fun-units-havetestcap", "oep.fun-units-havetestcap",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_FunctionalUnits_fun_units_createtestassoc,
      { "fun-units-createtestassoc", "oep.fun-units-createtestassoc",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_SystemType_sys_type_manager,
      { "sys-type-manager", "oep.sys-type-manager",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_SystemType_sys_type_agent,
      { "sys-type-agent", "oep.sys-type-agent",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_real_time_clock,
      { "mds-time-capab-real-time-clock", "oep.mds-time-capab-real-time-clock",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_set_clock,
      { "mds-time-capab-set-clock", "oep.mds-time-capab-set-clock",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_relative_time,
      { "mds-time-capab-relative-time", "oep.mds-time-capab-relative-time",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_high_res_relative_time,
      { "mds-time-capab-high-res-relative-time", "oep.mds-time-capab-high-res-relative-time",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_sync_abs_time,
      { "mds-time-capab-sync-abs-time", "oep.mds-time-capab-sync-abs-time",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_sync_rel_time,
      { "mds-time-capab-sync-rel-time", "oep.mds-time-capab-sync-rel-time",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_sync_hi_res_relative_time,
      { "mds-time-capab-sync-hi-res-relative-time", "oep.mds-time-capab-sync-hi-res-relative-time",
        FT_BOOLEAN, 8, NULL, 0x02,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_bo_time,
      { "mds-time-capab-bo-time", "oep.mds-time-capab-bo-time",
        FT_BOOLEAN, 8, NULL, 0x01,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_state_abs_time_synced,
      { "mds-time-state-abs-time-synced", "oep.mds-time-state-abs-time-synced",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_state_rel_time_synced,
      { "mds-time-state-rel-time-synced", "oep.mds-time-state-rel-time-synced",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_state_hi_res_relative_time_synced,
      { "mds-time-state-hi-res-relative-time-synced", "oep.mds-time-state-hi-res-relative-time-synced",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_mgr_set_time,
      { "mds-time-mgr-set-time", "oep.mds-time-mgr-set-time",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_capab_sync_bo_time,
      { "mds-time-capab-sync-bo-time", "oep.mds-time-capab-sync-bo-time",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_state_bo_time_synced,
      { "mds-time-state-bo-time-synced", "oep.mds-time-state-bo-time-synced",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_MdsTimeCapState_mds_time_state_bo_time_UTC_aligned,
      { "mds-time-state-bo-time-UTC-aligned", "oep.mds-time-state-bo-time-UTC-aligned",
        FT_BOOLEAN, 8, NULL, 0x02,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_avail_intermittent,
      { "mss-avail-intermittent", "oep.mss-avail-intermittent",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_avail_stored_data,
      { "mss-avail-stored-data", "oep.mss-avail-stored-data",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_upd_aperiodic,
      { "mss-upd-aperiodic", "oep.mss-upd-aperiodic",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_msmt_aperiodic,
      { "mss-msmt-aperiodic", "oep.mss-msmt-aperiodic",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_msmt_phys_ev_id,
      { "mss-msmt-phys-ev-id", "oep.mss-msmt-phys-ev-id",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_msmt_btb_metric,
      { "mss-msmt-btb-metric", "oep.mss-msmt-btb-metric",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_acc_manager_initiated,
      { "mss-acc-manager-initiated", "oep.mss-acc-manager-initiated",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_acc_agent_initiated,
      { "mss-acc-agent-initiated", "oep.mss-acc-agent-initiated",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_cat_manual,
      { "mss-cat-manual", "oep.mss-cat-manual",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_cat_setting,
      { "mss-cat-setting", "oep.mss-cat-setting",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_MetricSpecSmall_mss_cat_calculation,
      { "mss-cat-calculation", "oep.mss-cat-calculation",
        FT_BOOLEAN, 8, NULL, 0x02,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_start_stop,
      { "data-req-start-stop", "oep.data-req-start-stop",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_continuation,
      { "data-req-continuation", "oep.data-req-continuation",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_scope_all,
      { "data-req-scope-all", "oep.data-req-scope-all",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_scope_class,
      { "data-req-scope-class", "oep.data-req-scope-class",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_scope_handle,
      { "data-req-scope-handle", "oep.data-req-scope-handle",
        FT_BOOLEAN, 8, NULL, 0x02,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_mode_single_rsp,
      { "data-req-mode-single-rsp", "oep.data-req-mode-single-rsp",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_mode_time_period,
      { "data-req-mode-time-period", "oep.data-req-mode-time-period",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_mode_time_no_limit,
      { "data-req-mode-time-no-limit", "oep.data-req-mode-time-no-limit",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_DataReqMode_data_req_person_id,
      { "data-req-person-id", "oep.data-req-person-id",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_stop,
      { "data-req-supp-stop", "oep.data-req-supp-stop",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_scope_all,
      { "data-req-supp-scope-all", "oep.data-req-supp-scope-all",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_scope_class,
      { "data-req-supp-scope-class", "oep.data-req-supp-scope-class",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_scope_handle,
      { "data-req-supp-scope-handle", "oep.data-req-supp-scope-handle",
        FT_BOOLEAN, 8, NULL, 0x02,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_mode_single_rsp,
      { "data-req-supp-mode-single-rsp", "oep.data-req-supp-mode-single-rsp",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_mode_time_period,
      { "data-req-supp-mode-time-period", "oep.data-req-supp-mode-time-period",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_mode_time_no_limit,
      { "data-req-supp-mode-time-no-limit", "oep.data-req-supp-mode-time-no-limit",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_person_id,
      { "data-req-supp-person-id", "oep.data-req-supp-person-id",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_DataReqModeFlags_data_req_supp_init_agent,
      { "data-req-supp-init-agent", "oep.data-req-supp-init-agent",
        FT_BOOLEAN, 8, NULL, 0x01,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_var_no_of_segm,
      { "pmsc-var-no-of-segm", "oep.pmsc-var-no-of-segm",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_segm_id_list_select,
      { "pmsc-segm-id-list-select", "oep.pmsc-segm-id-list-select",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_epi_seg_entries,
      { "pmsc-epi-seg-entries", "oep.pmsc-epi-seg-entries",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_peri_seg_entries,
      { "pmsc-peri-seg-entries", "oep.pmsc-peri-seg-entries",
        FT_BOOLEAN, 8, NULL, 0x04,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_abs_time_select,
      { "pmsc-abs-time-select", "oep.pmsc-abs-time-select",
        FT_BOOLEAN, 8, NULL, 0x02,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_clear_segm_by_list_sup,
      { "pmsc-clear-segm-by-list-sup", "oep.pmsc-clear-segm-by-list-sup",
        FT_BOOLEAN, 8, NULL, 0x01,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_clear_segm_by_time_sup,
      { "pmsc-clear-segm-by-time-sup", "oep.pmsc-clear-segm-by-time-sup",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_clear_segm_remove,
      { "pmsc-clear-segm-remove", "oep.pmsc-clear-segm-remove",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_clear_segm_all_sup,
      { "pmsc-clear-segm-all-sup", "oep.pmsc-clear-segm-all-sup",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_PmStoreCapab_pmsc_multi_person,
      { "pmsc-multi-person", "oep.pmsc-multi-person",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_SegmEntryHeader_seg_elem_hdr_absolute_time,
      { "seg-elem-hdr-absolute-time", "oep.seg-elem-hdr-absolute-time",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_SegmEntryHeader_seg_elem_hdr_relative_time,
      { "seg-elem-hdr-relative-time", "oep.seg-elem-hdr-relative-time",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_SegmEntryHeader_seg_elem_hdr_hires_relative_time,
      { "seg-elem-hdr-hires-relative-time", "oep.seg-elem-hdr-hires-relative-time",
        FT_BOOLEAN, 8, NULL, 0x20,
        NULL, HFILL }},
    { &hf_oep_SegmEntryHeader_seg_elem_hdr_bo_time,
      { "seg-elem-hdr-bo-time", "oep.seg-elem-hdr-bo-time",
        FT_BOOLEAN, 8, NULL, 0x10,
        NULL, HFILL }},
    { &hf_oep_SegmEvtStatus_sevtsta_first_entry,
      { "sevtsta-first-entry", "oep.sevtsta-first-entry",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_SegmEvtStatus_sevtsta_last_entry,
      { "sevtsta-last-entry", "oep.sevtsta-last-entry",
        FT_BOOLEAN, 8, NULL, 0x40,
        NULL, HFILL }},
    { &hf_oep_SegmEvtStatus_sevtsta_agent_abort,
      { "sevtsta-agent-abort", "oep.sevtsta-agent-abort",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},
    { &hf_oep_SegmEvtStatus_sevtsta_manager_confirm,
      { "sevtsta-manager-confirm", "oep.sevtsta-manager-confirm",
        FT_BOOLEAN, 8, NULL, 0x80,
        NULL, HFILL }},
    { &hf_oep_SegmEvtStatus_sevtsta_manager_abort,
      { "sevtsta-manager-abort", "oep.sevtsta-manager-abort",
        FT_BOOLEAN, 8, NULL, 0x08,
        NULL, HFILL }},

/*--- End of included file: packet-oep-hfarr.c ---*/
#line 110 "../../asn1/oep/packet-oep-template.c"
  };

  /* List of subtrees */
  static gint *ett[] = {
    &ett_oep,

/*--- Included file: packet-oep-ettarr.c ---*/
#line 1 "../../asn1/oep/packet-oep-ettarr.c"
    &ett_oep_TYPE,
    &ett_oep_AVA_Type,
    &ett_oep_AVAAttributeValue,
    &ett_oep_AttributeList,
    &ett_oep_AttributeIdList,
    &ett_oep_AbsoluteTime,
    &ett_oep_BaseOffsetTime,
    &ett_oep_SystemModel,
    &ett_oep_ProductionSpec,
    &ett_oep_ProdSpecEntry,
    &ett_oep_PowerStatus,
    &ett_oep_BatMeasure,
    &ett_oep_MeasurementStatus,
    &ett_oep_NuObsValue,
    &ett_oep_NuObsValueCmp,
    &ett_oep_SaSpec,
    &ett_oep_SampleType,
    &ett_oep_SaFlags,
    &ett_oep_ScaleRangeSpec8,
    &ett_oep_ScaleRangeSpec16,
    &ett_oep_ScaleRangeSpec32,
    &ett_oep_EnumObsValue,
    &ett_oep_EnumVal,
    &ett_oep_SetTimeInvoke,
    &ett_oep_SetBOTimeInvoke,
    &ett_oep_SegmSelection,
    &ett_oep_SegmIdList,
    &ett_oep_AbsTimeRange,
    &ett_oep_BOTimeRange,
    &ett_oep_SegmentInfoList,
    &ett_oep_SegmentInfo,
    &ett_oep_ObservationScan,
    &ett_oep_ApduType,
    &ett_oep_AarqApdu,
    &ett_oep_DataProtoList,
    &ett_oep_DataProto,
    &ett_oep_DataProtoInfo,
    &ett_oep_AareApdu,
    &ett_oep_RlrqApdu,
    &ett_oep_RlreApdu,
    &ett_oep_AbrtApdu,
    &ett_oep_PhdAssociationInformation,
    &ett_oep_ManufSpecAssociationInformation,
    &ett_oep_AssociationVersion,
    &ett_oep_ProtocolVersion,
    &ett_oep_EncodingRules,
    &ett_oep_NomenclatureVersion,
    &ett_oep_FunctionalUnits,
    &ett_oep_SystemType,
    &ett_oep_AtrgApdu,
    &ett_oep_DataApdu,
    &ett_oep_DataApdu_message,
    &ett_oep_ErrorResult,
    &ett_oep_RejectResult,
    &ett_oep_EventReportArgumentSimple,
    &ett_oep_EventReportInfo,
    &ett_oep_EventReportResultSimple,
    &ett_oep_EventReplyInfo,
    &ett_oep_GetArgumentSimple,
    &ett_oep_GetResultSimple,
    &ett_oep_TypeVerList,
    &ett_oep_TypeVer,
    &ett_oep_SetArgumentSimple,
    &ett_oep_ModificationList,
    &ett_oep_AttributeModEntry,
    &ett_oep_SetResultSimple,
    &ett_oep_ActionArgumentSimple,
    &ett_oep_ActionArgumentSimpleInfoArgs,
    &ett_oep_ActionResultSimple,
    &ett_oep_ActionResultSimpleInfoArgs,
    &ett_oep_AttrValMap,
    &ett_oep_AttrValMapEntry,
    &ett_oep_MdsTimeInfo,
    &ett_oep_MdsTimeCapState,
    &ett_oep_RegCertDataList,
    &ett_oep_RegCertData,
    &ett_oep_AuthBodyAndStrucType,
    &ett_oep_SupplementalTypeList,
    &ett_oep_MetricSpecSmall,
    &ett_oep_MetricStructureSmall,
    &ett_oep_MetricIdList,
    &ett_oep_HandleAttrValMap,
    &ett_oep_HandleAttrValMapEntry,
    &ett_oep_HANDLEList,
    &ett_oep_ScanReportInfoVar,
    &ett_oep_SEQUENCE_OF_ObservationScan,
    &ett_oep_ScanReportInfoFixed,
    &ett_oep_SEQUENCE_OF_ObservationScanFixed,
    &ett_oep_ObservationScanFixed,
    &ett_oep_ObservationValueData,
    &ett_oep_ScanReportInfoGrouped,
    &ett_oep_SEQUENCE_OF_ObservationScanGrouped,
    &ett_oep_ScanReportInfoMPVar,
    &ett_oep_SEQUENCE_OF_ScanReportPerVar,
    &ett_oep_ScanReportPerVar,
    &ett_oep_ScanReportInfoMPFixed,
    &ett_oep_SEQUENCE_OF_ScanReportPerFixed,
    &ett_oep_ScanReportPerFixed,
    &ett_oep_ScanReportInfoMPGrouped,
    &ett_oep_SEQUENCE_OF_ScanReportPerGrouped,
    &ett_oep_ScanReportPerGrouped,
    &ett_oep_ConfigReport,
    &ett_oep_ConfigObjectList,
    &ett_oep_ConfigObject,
    &ett_oep_ConfigReportRsp,
    &ett_oep_DataRequest,
    &ett_oep_DataReqMode,
    &ett_oep_DataReqModeCapab,
    &ett_oep_DataReqModeFlags,
    &ett_oep_DataResponse,
    &ett_oep_DataResponseEventInfo,
    &ett_oep_SimpleNuObsValueCmp,
    &ett_oep_BasicNuObsValueCmp,
    &ett_oep_PmStoreCapab,
    &ett_oep_PmSegmentEntryMap,
    &ett_oep_SegmEntryHeader,
    &ett_oep_SegmEntryElemList,
    &ett_oep_SegmEntryElem,
    &ett_oep_TrigSegmDataXferReq,
    &ett_oep_TrigSegmDataXferRsp,
    &ett_oep_SegmentDataEvent,
    &ett_oep_SegmentDataResult,
    &ett_oep_SegmDataEventDescr,
    &ett_oep_SegmEvtStatus,
    &ett_oep_SegmentStatistics,
    &ett_oep_SegmentStatisticEntry,

/*--- End of included file: packet-oep-ettarr.c ---*/
#line 116 "../../asn1/oep/packet-oep-template.c"
  };

  /* Register protocol */
  proto_oep = proto_register_protocol(PNAME, PSNAME, PFNAME);

  /* Register fields and subtrees */
  proto_register_field_array(proto_oep, hf, array_length(hf));
  proto_register_subtree_array(ett, array_length(ett));

  oep_module = prefs_register_protocol(proto_oep, proto_reg_handoff_oep);
  prefs_register_uint_preference(oep_module, "tcp.port",
                                   "TCP Port",
                                   "Set the TCP port for OEP",
                                   10,
                                   &oep_port);
}


/*--- proto_reg_handoff_oep -------------------------------------------*/
void proto_reg_handoff_oep(void) {

    static gboolean initialized = FALSE;

    static int port = 0;

    static dissector_handle_t oep_handle;

    if (initialized)
    {
        dissector_delete_uint("tcp.port", port, oep_handle);
    }
    else
    {
        oep_handle = create_dissector_handle(dissect_oep, proto_oep);
        initialized = TRUE;
    }

    port = oep_port;
    dissector_add_uint("tcp.port", port, oep_handle);
}

/*
 * Editor modelines  -  http://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
 