/* c-basic-offset: 4; tab-width: 4; indent-tabs-mode: nil
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
/* packet-mder.c
 * Helpers for ASN.1/MDER dissection
 * Sebastian Boegl (C) 2013
 * Originally created for BER by Ronnie Sahlmderg in 2004
 *
 * $Id$
 *
 * Wireshark - Network traffic analyzer
 * By Gerald Combs <gerald@wireshark.org>
 * Copyright 1998 Gerald Combs
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/* actx->private_data is used to indicate if only the length of the current packet is to dissected.
 * TODO: change #.REGISTER signature to new_dissector_t and
 * update call_mder_oid_callback() accordingly.
 */

#define DEBUG_MDER 1

#include "config.h"

#include <stdio.h>

#include <glib.h>

#include <epan/packet.h>
#include <epan/exceptions.h>
#include <epan/strutil.h>
#include <epan/to_str.h>
#include <epan/prefs.h>
#include <epan/reassemble.h>
#include <epan/oids.h>
#include <epan/expert.h>
#include <epan/uat.h>
#include <epan/asn1.h>
#include <epan/ftypes/ftypes.h>
#include <epan/wmem/wmem.h>
#include <wiretap/wtap.h>

#include "packet-mder.h"

/*
 * Set a limit on recursion so we don't blow away the stack. Another approach
 * would be to remove recursion completely but then we'd exhaust CPU+memory
 * trying to read a hellabyte of nested indefinite lengths.

 * XXX - Max nesting in the ASN.1 plugin is 32. Should they match?
 */
#define MDER_MAX_NESTING 500

#define PNAME  "Medical Device Encoding Rules (ASN.1 IEEE 11073-20601:Annex F)"
#define PSNAME "MDER"
#define PFNAME "mder"

void proto_register_mder(void);
void proto_reg_handoff_mder(void);

static gint proto_mder = -1;
static gint hf_mder_id_class = -1;
static gint hf_mder_id_pc = -1;
static gint hf_mder_id_uni_tag = -1;
static gint hf_mder_id_uni_tag_ext = -1;
static gint hf_mder_id_tag = -1;
static gint hf_mder_id_tag_ext = -1;
static gint hf_mder_length = -1;
static gint hf_mder_string_length = -1;
static gint hf_mder_string_value = -1;
static gint hf_mder_count = -1;
static gint hf_mder_bitstring_padding = -1;
static gint hf_mder_bitstring_empty = -1;
static gint hf_mder_unknown_OCTETSTRING = -1;
static gint hf_mder_unknown_MDER_OCTETSTRING = -1;
static gint hf_mder_unknown_INTEGER = -1;
static gint hf_mder_unknown_BITSTRING = -1;
static gint hf_mder_error = -1;
static gint hf_mder_direct_reference = -1;         /* OBJECT_IDENTIFIER */
static gint hf_mder_indirect_reference = -1;       /* INTEGER */
static gint hf_mder_data_value_descriptor = -1;    /* ObjectDescriptor */
static gint hf_mder_single_ASN1_type = -1;         /* T_single_ASN1_type */
static gint hf_mder_octet_aligned = -1;            /* OCTET_STRING */
static gint hf_mder_arbitrary = -1;                /* BIT_STRING */

static int hf_mder_fragments = -1;
static int hf_mder_fragment = -1;
static int hf_mder_fragment_overlap = -1;
static int hf_mder_fragment_overlap_conflicts = -1;
static int hf_mder_fragment_multiple_tails = -1;
static int hf_mder_fragment_too_long_fragment = -1;
static int hf_mder_fragment_error = -1;
static int hf_mder_fragment_count = -1;
static int hf_mder_reassembled_in = -1;
static int hf_mder_reassembled_length = -1;

static gint ett_mder_octet_string = -1;
static gint ett_mder_reassembled_octet_string = -1;
static gint ett_mder_primitive = -1;
static gint ett_mder_unknown = -1;
static gint ett_mder_SEQUENCE = -1;
static gint ett_mder_EXTERNAL = -1;
static gint ett_mder_T_encoding = -1;
static gint ett_mder_fragment = -1;
static gint ett_mder_fragments = -1;

static expert_field ei_mder_size_constraint_string = EI_INIT;
static expert_field ei_mder_size_constraint_value = EI_INIT;
static expert_field ei_mder_size_constraint_items = EI_INIT;
static expert_field ei_mder_sequence_field_wrong = EI_INIT;
static expert_field ei_mder_expected_octet_string = EI_INIT;
static expert_field ei_mder_expected_null = EI_INIT;
static expert_field ei_mder_expected_null_zero_length = EI_INIT;
static expert_field ei_mder_expected_sequence = EI_INIT;
static expert_field ei_mder_expected_set = EI_INIT;
static expert_field ei_mder_expected_string = EI_INIT;
static expert_field ei_mder_expected_object_identifier = EI_INIT;
static expert_field ei_mder_expected_generalized_time = EI_INIT;
static expert_field ei_mder_expected_utc_time = EI_INIT;
static expert_field ei_mder_expected_bitstring = EI_INIT;
static expert_field ei_mder_error_length = EI_INIT;
static expert_field ei_mder_wrong_tag_in_tagged_type = EI_INIT;
static expert_field ei_mder_universal_tag_unknown = EI_INIT;
static expert_field ei_mder_no_oid = EI_INIT;
static expert_field ei_mder_syntax_not_implemented = EI_INIT;
static expert_field ei_mder_oid_not_implemented = EI_INIT;
static expert_field ei_mder_value_too_many_bytes = EI_INIT;
static expert_field ei_mder_unknown_field_sequence = EI_INIT;
static expert_field ei_mder_unknown_field_set = EI_INIT;
static expert_field ei_mder_missing_field_set = EI_INIT;
static expert_field ei_mder_empty_choice = EI_INIT;
static expert_field ei_mder_choice_not_found = EI_INIT;
static expert_field ei_mder_bits_unknown = EI_INIT;
static expert_field ei_mder_bits_set_padded = EI_INIT;
static expert_field ei_mder_illegal_padding = EI_INIT;
static expert_field ei_mder_invalid_format_generalized_time = EI_INIT;
static expert_field ei_mder_invalid_format_utctime = EI_INIT;

static dissector_handle_t mder_handle;

static gboolean show_internal_mder_fields         = FALSE;
static gboolean decode_octetstring_as_mder        = FALSE;

static gchar *decode_as_syntax = NULL;
static gchar *mder_filename     = NULL;

static dissector_table_t mder_oid_dissector_table    = NULL;
static dissector_table_t mder_syntax_dissector_table = NULL;

static GHashTable *syntax_table = NULL;

static gint8    last_class;
static gboolean last_pc;
static gint32   last_tag;
guint32  last_length;
guint32  last_element = -1;
gboolean set_last_element   = FALSE;

static const value_string mder_class_codes[] = {
    { MDER_CLASS_UNI,    "UNIVERSAL" },
    { MDER_CLASS_CON,    "CONTEXT" },
    { 0, NULL }
};

static const true_false_string mder_pc_codes = {
    "Constructed Encoding",
    "Primitive Encoding"
};

static const true_false_string mder_pc_codes_short = {
    "constructed",
    "primitive"
};

static const value_string mder_uni_tag_codes[] = {
    { MDER_UNI_TAG_INTEGER,          "INTEGER" },
    { MDER_UNI_TAG_BITSTRING,        "BIT STRING" },
    { MDER_UNI_TAG_OCTETSTRING,      "OCTET STRING" },
    { MDER_UNI_TAG_NULL,             "NULL" },
    { MDER_UNI_TAG_SEQUENCE,         "SEQUENCE" },
    { 0, NULL }
};
static value_string_ext mder_uni_tag_codes_ext = VALUE_STRING_EXT_INIT(mder_uni_tag_codes);


static const value_string mdnf_float_names[] = {
    { MDNF_FLOAT_NAN,               "NaN" },
    { MDNF_FLOAT_NRES,              "NRes" },
    { MDNF_FLOAT_INFINITY_PLUS,     "+Infinity" },
    { MDNF_FLOAT_INFINITY_MINUS,    "-Infinity" },
    { MDNF_FLOAT_RESERVED,          "Reserved for further use" },
    { 0, NULL }
};

static const value_string mdnf_sfloat_names[] = {

    { MDNF_SFLOAT_NAN,              "NaN" },
    { MDNF_SFLOAT_NRES,             "NRes" },
    { MDNF_SFLOAT_INFINITY_PLUS,    "+Infinity" },
    { MDNF_SFLOAT_INFINITY_MINUS,   "-Infinity" },
    { MDNF_SFLOAT_RESERVED,         "Reserved for further use" },
    { 0, NULL }
};

typedef struct _da_data {
    GHFunc   func;
    gpointer user_data;
} da_data;

typedef struct _oid_user_t {
    gchar *oid;
    gchar *name;
    gchar *syntax;
} oid_user_t;

UAT_CSTRING_CB_DEF(oid_users, oid, oid_user_t)
UAT_CSTRING_CB_DEF(oid_users, name, oid_user_t)
UAT_VS_CSTRING_DEF(oid_users, syntax, oid_user_t, 0, "")

static oid_user_t *oid_users;
static guint num_oid_users;

#define MAX_SYNTAX_NAMES 128
/* Define non_const_value_string as a hack to prevent chackAPIs.pl from complaining */
#define non_const_value_string value_string
static non_const_value_string syntax_names[MAX_SYNTAX_NAMES+1] = {
    {0, ""},
    {0, NULL}
};

static void *
oid_copy_cb(void *dest, const void *orig, size_t len _U_)
{
    oid_user_t       *u = (oid_user_t *)dest;
    const oid_user_t *o = (const oid_user_t *)orig;

    u->oid = g_strdup(o->oid);
    u->name = g_strdup(o->name);
    u->syntax = o->syntax;

    return dest;
}

static void
oid_free_cb(void *r)
{
    oid_user_t *u = (oid_user_t *)r;

    g_free(u->oid);
    g_free(u->name);
}

static int
cmp_value_string(const void *v1, const void *v2)
{
    const value_string *vs1 = (const value_string *)v1;
    const value_string *vs2 = (const value_string *)v2;

    return strcmp(vs1->strptr, vs2->strptr);
}

static uat_field_t users_flds[] = {
    UAT_FLD_OID(oid_users, oid, "OID", "Object Identifier"),
    UAT_FLD_CSTRING(oid_users, name, "Name", "Human readable name for the OID"),
    UAT_FLD_VS(oid_users, syntax, "Syntax", syntax_names, "Syntax of values associated with the OID"),
    UAT_END_FIELDS
};

void
dissect_mder_oid_NULL_callback(tvbuff_t *tvb _U_, packet_info *pinfo _U_, proto_tree *tree _U_)
{
    return;
}

void
register_mder_oid_dissector_handle(const gchar *oid, dissector_handle_t dissector, int proto _U_, const gchar *name)
{
    dissector_add_string("mder.oid", oid, dissector);
    oid_add_from_string(name, oid);
}

void
register_mder_oid_dissector(const gchar *oid, dissector_t dissector, int proto, const gchar *name)
{
    dissector_handle_t dissector_handle;

    dissector_handle = create_dissector_handle(dissector, proto);
    dissector_add_string("mder.oid", oid, dissector_handle);
    oid_add_from_string(name, oid);
}

void
register_mder_syntax_dissector(const gchar *syntax, int proto, dissector_t dissector)
{
    dissector_handle_t dissector_handle;

    dissector_handle = create_dissector_handle(dissector, proto);
    dissector_add_string("mder.syntax", syntax, dissector_handle);
}

void
register_mder_oid_syntax(const gchar *oid, const gchar *name, const gchar *syntax)
{
    if (syntax && *syntax)
        g_hash_table_insert(syntax_table, (gpointer)g_strdup(oid), (gpointer)g_strdup(syntax));

    if (name && *name)
        register_mder_oid_name(oid, name);
}

/* Register the oid name to get translation in proto dissection */
void
register_mder_oid_name(const gchar *oid, const gchar *name)
{
    oid_add_from_string(name, oid);
}

static void
mder_add_syntax_name(gpointer key, gpointer value _U_, gpointer user_data)
{
    guint *i = (guint*)user_data;

    if (*i < MAX_SYNTAX_NAMES) {
        syntax_names[*i].value = *i;
        syntax_names[*i].strptr = (const gchar*)key;

        (*i)++;
    }
}

static void
mder_decode_as_dt(const gchar *table_name _U_, ftenum_t selector_type _U_, gpointer key, gpointer value, gpointer user_data)
{
    da_data *decode_as_data;

    decode_as_data = (da_data *)user_data;

    decode_as_data->func(key, value, decode_as_data->user_data);
}

void
mder_decode_as_foreach(GHFunc func, gpointer user_data)
{
    da_data decode_as_data;

    decode_as_data.func = func;
    decode_as_data.user_data = user_data;

    dissector_table_foreach("mder.syntax",  mder_decode_as_dt, &decode_as_data);
}

void
mder_decode_as(const gchar *syntax)
{
    if (decode_as_syntax) {
        g_free(decode_as_syntax);
        decode_as_syntax = NULL;
    }

    if (syntax)
        decode_as_syntax = g_strdup(syntax);
}

/* Get oid syntax from hash table to get translation in proto dissection(packet-per.c) */
static const gchar *
get_mder_oid_syntax(const gchar *oid)
{
    return (const gchar *)g_hash_table_lookup(syntax_table, oid);
}

void
mder_set_filename(gchar *filename)
{
    gchar      *ptr;

    if (mder_filename) {
        g_free(mder_filename);
        mder_filename = NULL;
    }

    if (filename) {

        mder_filename = g_strdup(filename);

        if ((ptr = strrchr(mder_filename, '.')) != NULL) {
            mder_decode_as(get_mder_oid_syntax(ptr));
        }
    }
}


static void
mder_update_oids(void)
{
    guint       i;

    for (i = 0; i < num_oid_users; i++)
        register_mder_oid_syntax(oid_users[i].oid, oid_users[i].name, oid_users[i].syntax);
}

static void
mder_check_length (guint32 length, gint32 min_len, gint32 max_len, asn1_ctx_t *actx, proto_item *item, gboolean bit)
{
    if ((min_len != -1) && (length < (guint32)min_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_string,
            "Size constraint: %sstring too short: %d (%d .. %d)",
            bit ? "bit " : "", length, min_len, max_len);
    } else if ((max_len != -1) && (length > (guint32)max_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_string,
            "Size constraint: %sstring too long: %d (%d .. %d)",
            bit ? "bit " : "", length, min_len, max_len);
    }
}

static void
mder_check_value64 (gint64 value, gint64 min_len, gint64 max_len, asn1_ctx_t *actx, proto_item *item)
{
    if ((min_len != -1) && (value < min_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_value,
            "Size constraint: value too small: %" G_GINT64_MODIFIER "d (%" G_GINT64_MODIFIER "d .. %" G_GINT64_MODIFIER "d)",
            value, min_len, max_len);
    } else if ((max_len != -1) && (value > max_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_value,
            "Size constraint: value too big: %" G_GINT64_MODIFIER "d (%" G_GINT64_MODIFIER "d .. %" G_GINT64_MODIFIER "d)",
            value, min_len, max_len);
    }
}

static void
mder_check_value (guint32 value, gint32 min_len, gint32 max_len, asn1_ctx_t *actx, proto_item *item)
{
    if ((min_len != -1) && (value < (guint32)min_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_value,
            "Size constraint: value too small: %d (%d .. %d)",
            value, min_len, max_len);
    } else if ((max_len != -1) && (value > (guint32)max_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_value,
            "Size constraint: value too big: %d (%d .. %d)",
            value, min_len, max_len);
    }
}

static void
mder_check_items (int cnt, gint32 min_len, gint32 max_len, asn1_ctx_t *actx, proto_item *item)
{
    if ((min_len != -1) && (cnt < min_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_items,
            "Size constraint: too few items: %d (%d .. %d)",
            cnt, min_len, max_len);
    } else if ((max_len != -1) && (cnt > max_len)) {
        expert_add_info_format(
            actx->pinfo, item, &ei_mder_size_constraint_items,
            "Size constraint: too many items: %d (%d .. %d)",
            cnt, min_len, max_len);
    }
}

static int
call_mder_syntax_callback(const gchar *syntax, tvbuff_t *tvb, int offset, packet_info *pinfo, proto_tree *tree)
{
    tvbuff_t   *next_tvb;

    next_tvb = tvb_new_subset_remaining(tvb, offset);
    if (syntax == NULL ||
       !dissector_try_string(mder_syntax_dissector_table, syntax, next_tvb, pinfo, tree, NULL)) {
        proto_item *item      = NULL;
        proto_tree *next_tree = NULL;

        if (syntax == NULL) {
            item = proto_tree_add_expert_format(
                    tree, pinfo, &ei_mder_no_oid, next_tvb, 0, tvb_length_remaining(tvb, offset),
                    "MDER: No syntax supplied to call_mder_syntax_callback");
        } else {
            item = proto_tree_add_expert_format(
                    tree, pinfo, &ei_mder_syntax_not_implemented, next_tvb, 0, tvb_length_remaining(tvb, offset),
                    "MDER: Dissector for syntax:%s not implemented."
                    " Contact Wireshark developers if you want this supported",
                    syntax);
        }
        if (item) {
            next_tree = proto_item_add_subtree(item, ett_mder_unknown);
        }
        // dissect_unknown_mder(pinfo, next_tvb, 0, next_tree);
    }

    /*XXX until we change the #.REGISTER signature for _PDU()s
     * into new_dissector_t   we have to do this kludge with
     * manually step past the content in the ANY type.
     */
    offset+=tvb_length_remaining(tvb, offset);

    return offset;
}


/* 8.1 General rules for encoding */

int
get_mder_intu32(tvbuff_t *tvb, int offset, guint32 *value)
{
    guint32     val;
    guint8      i;
    
    val = 0;
    if (tvb_length_remaining(tvb, offset) > 0) {
        val = tvb_get_guint8(tvb, offset);
        offset++;
    }
    for (i = 1; i<4 && tvb_length_remaining(tvb, offset) > 0; i++) {
        val = (val << 8) | tvb_get_guint8(tvb, offset);
        offset++;
    }
    if (value)
        *value = val;
    
    return offset;
}

int
get_mder_intu16(tvbuff_t *tvb, int offset, guint32 *value)
{
    guint32     val;
    
    val = 0;
    if (tvb_length_remaining(tvb, offset) > 0) {
        val = tvb_get_guint8(tvb, offset);
        offset++;
    }
    if (tvb_length_remaining(tvb, offset) > 0) {
        val = (val << 8) | tvb_get_guint8(tvb, offset);
        offset++;
    }
    if (value)
        *value = val;
    
    return offset;
}

int
get_mder_tag(tvbuff_t *tvb, int offset, guint32 *tag)
{
    offset = get_mder_intu16(tvb, offset, tag);

#ifdef DEBUG_MDER
        printf("get MDER tag %d, offset %d (remaining %d)\n", *tag, offset, tvb_length_remaining(tvb, offset));
#endif
    return offset;
}

int
get_mder_length(tvbuff_t *tvb, int offset, guint32 *length)
{
    offset = get_mder_intu16(tvb, offset, length);

#ifdef DEBUG_MDER
        printf("get MDER length %d, offset %d (remaining %d)\n", *length, offset, tvb_length_remaining(tvb, offset));
#endif
    
    return offset;
}

static void
get_last_mder_length(guint32 *length)
{
    if (length)
        *length = last_length;
}

static void
get_last_mder_sq_element(guint32 *element) {

    if (element && last_element != -1) {
        *element = last_element;
        last_element = -1;
    }
}

void
get_mder_sequence_length(asn1_ctx_t *actx, tvbuff_t *tvb, int offset, const mder_sequence_t *seq, guint32 *length) {
    guint32     len;
    guint32     length_remaining;
    tvbuff_t   *next_tvb;

    len = 0;
    
    while(seq->func != NULL) {
        int count;
        length_remaining = tvb_length_remaining(tvb, offset);
        next_tvb = tvb_new_subset(tvb, offset, length_remaining, length_remaining);

        if (next_tvb == NULL) {
            /* Assume that we have a malformed packet. */
            THROW(ReportedBoundsError);
        }
        
        actx->private_data = GUINT_TO_POINTER(TRUE); // dissect length only
        count = seq->func(FALSE, next_tvb, 0, actx, NULL, *seq->p_id);
        actx->private_data = NULL;                  // don't dissect length
        
        seq++;
        offset += count;
        len += count;
    }

    if (length) {
        *length = len;
    }
}

int
get_mder_count(tvbuff_t *tvb, int offset, guint32 *length)
{
    offset = get_mder_intu16(tvb, offset, length);

#ifdef DEBUG_MDER
    printf("get MDER count %d, offset %d (remaining %d)\n", length, offset, tvb_length_remaining(tvb, offset));
#endif
    
    return offset;
}

/* this function dissects the length octets of the MDER TLV.
 * We only handle LENGTHs that fit inside 16 bit integers.
 */
int
dissect_mder_length(proto_tree *tree, tvbuff_t *tvb, int offset, guint32 *length)
{
    int         old_offset = offset;
    guint32     tmp_length;

    offset = get_mder_length(tvb, offset, &tmp_length);

    if (show_internal_mder_fields) {
        proto_tree_add_uint(tree, hf_mder_length, tvb, old_offset, offset - old_offset, tmp_length);
    }
    if (length)
        *length = tmp_length;

#ifdef DEBUG_MDER
printf("dissect MDER length %d, offset %d (remaining %d)\n", tmp_length, offset, tvb_length_remaining(tvb, offset));
#endif

    last_length = tmp_length;

    return offset;
}

int
dissect_mder_count(proto_tree *tree, tvbuff_t *tvb, int offset, guint32 *length)
{
    int         old_offset = offset;
    guint32     tmp_length;

    offset = get_mder_count(tvb, offset, &tmp_length);

    if (show_internal_mder_fields) {
        proto_tree_add_uint(tree, hf_mder_count, tvb, old_offset, offset - old_offset, tmp_length);
    }
    if (length)
        *length = tmp_length;

#ifdef DEBUG_MDER
    printf("dissect MDER count %d, offset %d (remaining %d)\n", tmp_length, offset, tvb_length_remaining(tvb, offset));
#endif

    last_length = tmp_length;

    return offset;
}

/* F.4.4 Encoding of a octet string value */
int
dissect_mder_octet_string(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, tvbuff_t **out_tvb) {
     return dissect_mder_octet_string_size(implicit_tag, actx, tree, tvb, offset, hf_id, out_tvb, NULL);
}

int
dissect_mder_octet_string_size(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, tvbuff_t **out_tvb, guint32 *size)
{
    guint32         len;
    int             hoffset;
    int             end_offset, start_offset;
    proto_item     *it, *cause;
    guint32         i;
    guint32         len_remain;
    gint            length_remaining;
    proto_tree     *sub_tree;
    gchar          *str = NULL, *str_tmp = NULL;
    ftenum_t        type;
    header_field_info *hfinfo;
    
    #define MAX_TMP_BUFFER 3
    str = wmem_alloc(wmem_packet_scope(), 1024);
    str[0] = '\0';
    str_tmp = wmem_alloc(wmem_packet_scope(), MAX_TMP_BUFFER);
    str_tmp[0] = '\0';

#ifdef DEBUG_MDER
{
    const gchar *name;
    if (hf_id >= 0) {
        hfinfo = proto_registrar_get_nth(hf_id);
        name = hfinfo->name;
    }
    else {
        name = "unnamed";
    }
    if (tvb_length_remaining(tvb, offset) > 3) {
        printf("OCTET STRING dissect_mder_octet string(%s) entered implicit_tag:%d offset:%d len:%d %02x:%02x:%02x\n", name, implicit_tag, offset, tvb_length_remaining(tvb, offset), tvb_get_guint8(tvb, offset), tvb_get_guint8(tvb, offset + 1), tvb_get_guint8(tvb, offset + 2));
    }
    else {
        printf("OCTET STRING dissect_mder_octet_string(%s) entered\n", name);
    }
}
#endif
    
    start_offset = offset;
    if (out_tvb)
        *out_tvb = NULL;
        
#ifdef DEBUG_MDER
    printf("OCTET STRING implicit_tag: %s\n", implicit_tag ? "true" : "false");
#endif

    if (!implicit_tag) {
        hoffset = offset;
        
#ifdef DEBUG_MDER
        printf("OCTET STRING offset:%d\n", offset);
#endif

        /* read header and len for the octet string */
        if (size) {
            len = (guint32)size;
        }
        else {
            offset = dissect_mder_length(tree, tvb, offset, &len);
        }
        end_offset = offset + len;


#ifdef DEBUG_MDER
        printf("OCTET STRING offset:%d, len:%d\n", offset, len);
#endif

    }
    else {
        /* implicit tag so get from last tag/length */
        get_last_mder_length(&len);

        end_offset = offset + len;

#ifdef DEBUG_MDER
        printf("OCTET STRING offset:%d, len:%d\n", offset, len);
#endif

        /* caller may have created new buffer for indefinite length data Verify via length */
        len_remain = (guint32)tvb_length_remaining(tvb, offset);
        if (len_remain == (len - 2)) {
            /* new buffer received so adjust length and indefinite flag */
            len -= 2;
            end_offset -= 2;
        }
        else if (len_remain < len) {
            /*
            * error - short frame, or this item runs past the
            * end of the item containing it
            */
            cause = proto_tree_add_string_format_value(
                tree, hf_mder_error, tvb, offset, len, "illegal_length",
                "length:%u longer than tvb_length_remaining:%d",
                len,
                len_remain);
            expert_add_info(actx->pinfo, cause, &ei_mder_error_length);
            return end_offset;
        }

    }
    
    if (actx->private_data) { // dissect length only
        return end_offset;
    }
    //actx->created_item = NULL;
    length_remaining = tvb_length_remaining(tvb, offset);
    
#if 0
    if (length_remaining < 1) {
        return end_offset;
    }
#endif

    if (len <= (guint32)length_remaining) {
        length_remaining = len;
    }
    if (hf_id >= 0) {
        if (tree) {
            if (size) {
                proto_tree_add_item(tree, hf_id, tvb, offset, length_remaining, ENC_BIG_ENDIAN);
            } else {
                hfinfo = proto_registrar_get_nth(hf_id);
                type = hfinfo->type;
                if (IS_FT_STRING(type)) {
                    g_snprintf(str, len+1, "%s", tvb_get_string(wmem_packet_scope(), tvb, offset, len));
                    it = proto_tree_add_string(tree, hf_id, tvb, start_offset, length_remaining + offset - start_offset, str); 
                } else { 
                    for (i = 0; i < len; i++) {
                        g_snprintf(str_tmp, MAX_TMP_BUFFER, "%02x", tvb_get_guint8(tvb, offset + i));
                        str[(MAX_TMP_BUFFER-1)*i]=str_tmp[0];
                        str[(MAX_TMP_BUFFER-1)*i+1]=str_tmp[1];
                    }
                    str[(MAX_TMP_BUFFER-1)*len] = 0;
                    hfinfo->type = FT_STRING;
                    it = proto_tree_add_string_format_value(tree, hf_id, tvb, start_offset, length_remaining + offset - start_offset, str, "%s", str); 
                }
                sub_tree = proto_item_add_subtree(it, ett_mder_octet_string);
                proto_tree_add_uint(sub_tree, hf_mder_string_length, tvb, start_offset, offset - start_offset, len);
                if (IS_FT_STRING(type)) {
                    proto_registrar_get_nth(hf_mder_string_value)->type = type;
                    proto_tree_add_string(sub_tree, hf_mder_string_value, tvb, offset, length_remaining, str);
                } else {
                    hfinfo->type = type;
                    proto_tree_add_item(sub_tree, hf_mder_string_value, tvb, offset, length_remaining, ENC_BIG_ENDIAN);
                }
            }
        }
    }
    else {
        proto_item *pi;

        pi = proto_tree_add_text(tree, tvb, offset, len, "Unknown OctetString: Length: 0x%02x, Value: 0x", len);
        if (pi) {
            for (i = 0; i<len; i++) {
                proto_item_append_text(pi, "%02x", tvb_get_guint8(tvb, offset));
                offset++;
            }
        }
    }

    if (out_tvb) {
        if (len >(guint32)length_remaining)
            len = length_remaining;
        *out_tvb = tvb_new_subset(tvb, offset, len, len);
    }
    
    return end_offset;
}

int
dissect_mder_octet_string_length(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint32 *length) 
{
    guint32     len;
    int         start_offset;

    start_offset = offset;
    offset = dissect_mder_length(tree, tvb, offset, &len);
    if (length)
        *length = len;

    if (hf_id >= 0) {
        if (offset - start_offset > 0) {
            proto_tree_add_uint(tree, hf_mder_string_length, tvb, start_offset, offset, len);
        }
    }
    
    return offset;
}    

/* Encoding of a null value */
int
dissect_mder_null(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id) {
    if (hf_id >= 0)
        proto_tree_add_item(tree, hf_id, tvb, offset, 0, ENC_BIG_ENDIAN);
    
    return offset;
}

/* F.4.2 Integer */
int
dissect_mder_integer64(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint64 *value, Size size)
{
    gint64      val;
    guint32     i, length = (guint32)size;
    header_field_info *hfinfo;

    val = 0;
    for (i = 0; i < length; i++) {
        val = (val << 8) | tvb_get_guint8(tvb, offset);
        offset++;
    }
    
    if (hf_id >= 0) {
        hfinfo = proto_registrar_get_nth(hf_id);
        switch (hfinfo->type) {
        case FT_UINT8:
        case FT_UINT16:
        case FT_UINT24:
        case FT_UINT32:
            actx->created_item = proto_tree_add_uint(tree, hf_id, tvb, offset - length, length, (guint32)val);
            break;
        case FT_INT8:
        case FT_INT16:
        case FT_INT24:
        case FT_INT32:
            actx->created_item = proto_tree_add_int(tree, hf_id, tvb, offset - length, length, (gint32)val);
            break;
        case FT_INT64:
            actx->created_item = proto_tree_add_int64(tree, hf_id, tvb, offset - length, length, val);
            break;
        case FT_UINT64:
            actx->created_item = proto_tree_add_uint64(tree, hf_id, tvb, offset - length, length, (guint64)val);
            break;
        default:
            DISSECTOR_ASSERT_NOT_REACHED();
        }
    }

    if (value) {
        *value = val;
    }
    if (set_last_element) {
        last_element = (guint32)val;
    }
    
    return offset;
}

int 
dissect_mder_intu32(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint32 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_32);
    if (value) {
        *value = (guint32)val;
    }

    return offset;
}

int 
dissect_mder_intu16(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint16 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_16);
    if (value) {
        *value = (guint16)val;
    }

    return offset;
}

int 
dissect_mder_intu8(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint8 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_8);
    if (value) {
        *value = (guint8)val;
    }

    return offset;
}

int 
dissect_mder_inti32(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint32 *value) 
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_32);
    if (value) {
        *value = (gint32)val;
    }

    return offset;
}

int 
dissect_mder_inti16(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint16 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_16);
    if (value) {
        *value = (gint16)val;
    }

    return offset;
}

int 
dissect_mder_inti8(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gint8 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_8);
    if (value) {
        *value = (gint8)val;
    }

    return offset;
}

int
dissect_mder_constrained_integer64(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint64 min_len, gint64 max_len, gint hf_id, gint64 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_64);
    if (value) {
        *value = val;
    }

    mder_check_value64 (val, min_len, max_len, actx, actx->created_item);

    return offset;
}

int
dissect_mder_integer(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, guint32 *value)
{
    gint64      val;
    guint8      length_header;
    Size        size = BIT_16;

    length_header = ftype_length(proto_registrar_get_nth(hf_id)->type);
    size = tvb_length_remaining(tvb, offset);
    if (length_header < size) {
        size = length_header;
    }
    if (size == BIT_8 || size == BIT_16 || size == BIT_32) {
        offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, size);
    } else {
        offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_16);
    }
    if (value) {
        *value = (guint32)val;
    }

    return offset;
}

int
dissect_mder_constrained_integer(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint32 min_len, gint32 max_len, gint hf_id, guint32 *value)
{
    gint64      val;

    offset = dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_32);
    if (value) {
        *value = (guint32)val;
    }

    mder_check_value ((guint32)val, min_len, max_len, actx, actx->created_item);

    return offset;
}


/* F.6/F.7 Encoding of a float value */

int
dissect_mder_float_size(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gdouble *value, Size size)
{
    gdouble     val = 0.0f;
    gint8       exponent;
    gint32      magnitude, number;
    gint32      base = 10, tmp = 0, j;
    guint32     length = (guint32)size, i;
    guint32     reader;
    gboolean    isSFloat = (BIT_16 == size);
    header_field_info *hfinfo;
    const gchar *error = NULL; 
    const gchar *unknown = "UNKNOWN";
    const gchar *val_str = NULL;

    if (isSFloat) {
        reader = tvb_get_guint8(tvb, offset);
        offset++;
        exponent = (gint8)(reader >> 4);
        reader = (reader << 4);
    } else {
        exponent = (gint32)tvb_get_guint8(tvb, offset);
        offset++;
        reader = 0;
    }
    for (i = 1; i < length; i++) {
        reader = (reader << 8) | tvb_get_guint8(tvb, offset);
        offset++;
    }
    magnitude = (gint32)reader;
    number = 1;
    tmp = (exponent >= 0 ? exponent : -exponent);
    for (j = 1; j <= tmp; j++) {
        number *= base;
    }
    if (exponent >= 0) {
        val = magnitude * number;
    } else {
        val = magnitude / number;
    }
    error = val_to_str_const((gint32)val, (isSFloat ? mdnf_sfloat_names : mdnf_float_names), unknown);
    
    if (hf_id >= 0) {
        if (strcmp(error, unknown) != 0) {
            actx->created_item = proto_tree_add_text(tree, tvb, offset - length, length, "%s: %s", (isSFloat ? "Float" : "Double"), error);
        } else {
            hfinfo = proto_registrar_get_nth(hf_id);
            hfinfo->type = FT_DOUBLE;
            if (exponent < 0) {
                proto_tree_add_double_format_value(tree, hf_id, tvb, offset - length, length, val, "%.*f", -exponent, val);
            } else {
                proto_tree_add_double_format_value(tree, hf_id, tvb, offset - length, length, val, "%.0f", val);
            }
            
        }
    }

    if (value) {
        *value = val;
    }
    if (set_last_element) {
        last_element = (guint32)val;
    }
    
    return offset;
}

/* F.6 Encoding of a float value */

int
dissect_mder_float(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gdouble *value)
{
    gdouble     val;

    offset = dissect_mder_float_size(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_32);
    if (value) {
        *value = val;
    }

    return offset;
}


/* F.7 Encoding of a sfloat value */
int
dissect_mder_sfloat(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *tree, tvbuff_t *tvb, int offset, gint hf_id, gfloat *value)
{
    gdouble     val;

    offset = dissect_mder_float_size(implicit_tag, actx, tree, tvb, offset, hf_id, &val, BIT_16);
    if (value) {
        *value = (gfloat)val;
    }

    return offset;
}

/* F.4.5 Encoding of a sequence */
int
dissect_mder_sequence(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_sequence_t *seq, gint hf_id, gint ett_id) {
    gboolean    imp_tag = FALSE;
    guint32     lenx;
    gint        length_remaining;
    proto_tree *tree       = parent_tree;
    proto_item *item       = NULL;
    proto_item *cause;
    int         end_offset = 0;
    tvbuff_t   *next_tvb;
    const gchar *name;
    header_field_info *hfinfo;

    if (hf_id >= 0) {
        hfinfo = proto_registrar_get_nth(hf_id);
        name = hfinfo->name;
    } else {
        name = "unnamed";
    }

#ifdef DEBUG_MDER
    if (tvb_length_remaining(tvb, offset) > 3) {
        printf("SEQUENCE dissect_mder_sequence(%s) entered offset:%d len:%d %02x:%02x:%02x\n", name, offset, tvb_length_remaining(tvb, offset), tvb_get_guint8(tvb, offset), tvb_get_guint8(tvb, offset+1), tvb_get_guint8(tvb, offset+2));
    } else {
        printf("SEQUENCE dissect_mder_sequence(%s) entered\n", name);
    }
#endif

    get_mder_sequence_length(actx, tvb, offset, seq, &lenx);
    end_offset = offset + lenx;
    /* create subtree */
    
#ifdef DEBUG_MDER_CHOICE
    printf("SEQUENCE create tree: offset:%d, end_offset: %d, hf_id:%d\n", offset, end_offset, hf_id);
#endif

    if (hf_id >= 0) {
        if (parent_tree) {
            item = proto_tree_add_item(parent_tree, hf_id, tvb, offset, lenx, ENC_BIG_ENDIAN);
            tree = proto_item_add_subtree(item, ett_id);
        }
    }
    if(offset == end_offset){
        proto_item_append_text(item, " [0 length]");
    }
    /* loop over all entries until we reach the end of the sequence */
    while (offset < end_offset) {
        int      count;

        length_remaining = tvb_length_remaining(tvb, offset);
        if (length_remaining >(end_offset - offset))
            length_remaining = end_offset - offset;
        next_tvb = tvb_new_subset(tvb, offset, length_remaining, length_remaining);

#ifdef DEBUG_MDER
    if (tvb_length_remaining(next_tvb, 0) > 3) {
        printf("SEQUENCE dissect_mder_sequence(%s) calling subdissector offset:%d len:%d %02x:%02x:%02x\n", name, offset, tvb_length_remaining(next_tvb, 0), tvb_get_guint8(next_tvb, 0), tvb_get_guint8(next_tvb, 1), tvb_get_guint8(next_tvb, 2));
    } else {
        printf("SEQUENCE dissect_mder_sequence(%s) calling subdissector\n", name);
    }
#endif

        if (next_tvb == NULL) {
            /* Assume that we have a malformed packet. */
            THROW(ReportedBoundsError);
        }
        imp_tag = FALSE;
        if (seq->flags & MDER_FLAGS_IMPLTAG) {
            imp_tag = TRUE;
        }
        set_last_element = TRUE;
        count = seq->func(imp_tag, next_tvb, 0, actx, tree, *seq->p_id);
        set_last_element = FALSE;

#ifdef DEBUG_MDER
    printf("SEQUENCE dissect_mder_sequence(%s) subdissector ate %d bytes\n", name, count);
#endif

        seq++;
        offset += count;
    }

    /* if we didnt end up at exactly offset, then we ate too many bytes */
    if (offset != end_offset) {
        tvb_ensure_bytes_exist(tvb, offset-2, 2);
        cause = proto_tree_add_string_format_value(
            tree, hf_mder_error, tvb, offset-2, 2, "illegal_length",
            "Sequence ate %d too many bytes",
            offset - end_offset);
        printf("Sequence ate %d too many bytes", offset - end_offset);
        expert_add_info_format(actx->pinfo, cause, &ei_mder_error_length,
            "MDER Error: too many bytes in Sequence");
    }
    
    return offset;
}


/* F.4.7 Encoding of a choice */
#ifdef DEBUG_MDER
#define DEBUG_MDER_CHOICE
#endif

int
dissect_mder_choice(asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_choice_t *choice, gint hf_id, gint ett_id, gint *branch_taken)
{
    gboolean    imp_tag = FALSE;
    gint32      tag;
    guint32     len;
    proto_tree *tree = parent_tree;
    proto_item *item = NULL;
    int         end_offset, start_offset, count;
    int         hoffset = offset;
    gint        length, length_remaining;
    tvbuff_t   *next_tvb;
    gboolean    first_pass;
    header_field_info   *hfinfo;
    const mder_choice_t *ch;

#ifdef DEBUG_MDER_CHOICE
{
const gchar *name;
if (hf_id >= 0) {
hfinfo = proto_registrar_get_nth(hf_id);
name = hfinfo->name;
} else {
name = "unnamed";
}
if (tvb_length_remaining(tvb, offset) > 3) {
printf("CHOICE dissect_mder_choice(%s) entered offset:%d len:%d %02x:%02x:%02x\n", name, offset, tvb_length_remaining(tvb, offset), tvb_get_guint8(tvb, offset), tvb_get_guint8(tvb, offset+1), tvb_get_guint8(tvb, offset+2));
} else {
printf("CHOICE dissect_mder_choice(%s) entered len:%d\n", name, tvb_length_remaining(tvb, offset));
}
}
#endif

    start_offset = offset;

    if (branch_taken) {
        *branch_taken = -1;
    }

    if (tvb_length_remaining(tvb, offset) == 0) {
        item = proto_tree_add_string_format_value(
            parent_tree, hf_mder_error, tvb, offset, 0, "empty_choice",
            "Empty choice was found");
        expert_add_info(actx->pinfo, item, &ei_mder_empty_choice);
        return offset;
    }

    /* read header and len for choice field */
    offset = get_mder_tag(tvb, offset, &tag);
    offset = dissect_mder_length(tree, tvb, offset, &len);
    end_offset = offset + len;
    if (actx->private_data) { // dissect length only
        return end_offset;
    }

    /* Some sanity checks.
     * The hf field passed to us MUST be an integer type
     */
    if (hf_id >= 0) {
        hfinfo = proto_registrar_get_nth(hf_id);
        switch (hfinfo->type) {
        case FT_UINT8:
        case FT_UINT16:
        case FT_UINT24:
        case FT_UINT32:
            break;
        default:
            proto_tree_add_text(
                tree, tvb, offset, len,
                "dissect_mder_choice(): Was passed a HF field that was not integer type : %s",
                hfinfo->abbrev);
            g_warning("dissect_mder_choice(): frame:%u offset:%d Was passed a HF field that was not integer type : %s",
                      actx->pinfo->fd->num, offset, hfinfo->abbrev);
            return end_offset;
        }
    }

    /* loop over all entries until we find the right choice or
       run out of entries */
    ch = choice;
    first_pass = TRUE;
    while (ch->func || first_pass) {
        if (branch_taken) {
            (*branch_taken)++;
        }
        /* we reset for a second pass when we will look for choices */
        if (!ch->func) {
            first_pass = FALSE;
            ch = choice; /* reset to the beginning */
            if (branch_taken) {
                *branch_taken = -1;
            }
        }

choice_try_again:
#ifdef DEBUG_MDER_CHOICE
printf("CHOICE testing potential subdissector class[%p]: tag:%d:(expected)%d flags:%d\n", ch, tag, ch->tag, ch->flags);
#endif

        if ( (first_pass
           && ((ch->tag == tag)
            || ((ch->tag == -1) && (ch->flags & MDER_FLAGS_NOOWNTAG))))
          || (!first_pass && ((ch->tag == -1))) /* we failed on the first pass so now try any choices */
        ) {
            
            /* create subtree */
            if (hf_id >= 0) {
                if (parent_tree) {
                    item = proto_tree_add_uint(parent_tree, hf_id, tvb, start_offset, end_offset - start_offset, ch->value);
                    tree = proto_item_add_subtree(item, ett_id);
                    proto_tree_add_uint(tree, hf_mder_id_tag_ext, tvb, start_offset, 2, tag);
                    proto_tree_add_uint(tree, hf_mder_length, tvb, start_offset + 2, 2, len);
                }
            }

            start_offset = offset;
            hoffset = offset;
            length = len;

            length_remaining = tvb_length_remaining(tvb, hoffset);
            if (length_remaining>length)
                length_remaining = length;

#ifdef DEBUG_MDER_CHOICE
{
printf("CHOICE tvb_new_subset(tvb, %d, %d, %d)\n", hoffset, length_remaining, length);
}
#endif

            next_tvb = tvb_new_subset(tvb, hoffset, length_remaining, length);


#ifdef DEBUG_MDER_CHOICE
{
const gchar *name;
if (hf_id >= 0) {
hfinfo = proto_registrar_get_nth(hf_id);
name = hfinfo->name;
} else {
name = "unnamed";
}
if (tvb_length_remaining(next_tvb, 0) > 3) {
printf("CHOICE dissect_mder_choice(%s) calling subdissector start_offset:%d offset:%d len:%d %02x:%02x:%02x\n", name, start_offset, offset, tvb_length_remaining(next_tvb, 0), tvb_get_guint8(next_tvb, 0), tvb_get_guint8(next_tvb, 1), tvb_get_guint8(next_tvb, 2));
} else {
printf("CHOICE dissect_mder_choice(%s) calling subdissector len:%d\n", name, tvb_length(next_tvb));
}
}
#endif

            if (next_tvb == NULL) {
            
#ifdef DEBUG_MDER_CHOICE
{
printf("CHOICE next_tvb is null\n");
}
#endif

                /* Assume that we have a malformed packet. */
                THROW(ReportedBoundsError);
            }
            imp_tag = FALSE;
            if ((ch->flags & MDER_FLAGS_IMPLTAG))
                imp_tag = TRUE;
            count = ch->func(imp_tag, next_tvb, 0, actx, tree, *ch->p_id);
            
#ifdef DEBUG_MDER_CHOICE
{
const gchar *name;
if (hf_id >= 0) {
hfinfo = proto_registrar_get_nth(hf_id);
name = hfinfo->name;
} else {
name = "unnamed";
}
printf("CHOICE dissect_mder_choice(%s) subdissector ate %d bytes\n", name, count);
}
#endif

            if ((count == 0) && (((ch->tag == -1) && (ch->flags & MDER_FLAGS_NOOWNTAG)) || !first_pass)) {
                /* wrong one, break and try again */
                ch++;
                
#ifdef DEBUG_MDER_CHOICE
{
const gchar *name;
if (hf_id >= 0) {
hfinfo = proto_registrar_get_nth(hf_id);
name = hfinfo->name;
} else {
name = "unnamed";
}
printf("CHOICE dissect_mder_choice(%s) trying again\n", name);
}
#endif

                goto choice_try_again;
            }
            if (!(ch->flags & MDER_FLAGS_NOOWNTAG)) 
            {
                /* we are traversing a indfinite length choice where we did not pass the tag length */
                /* we need to eat the EOC */
                    if (show_internal_mder_fields) {
                        proto_tree_add_text(tree, tvb, start_offset, count+2, "CHOICE EOC");
                    }
            }
            return end_offset;
        }
        ch++;
    }
    if (branch_taken) {
        /* none of the branches were taken so set the param
           back to -1 */
        *branch_taken = -1;
    }
    
    return start_offset;
}

/* F.4.8 Encoding of a any defined by and instance-of */

#ifdef DEBUG_MDER
#define DEBUG_MDER_ANY
#endif

int 
dissect_mder_any(asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_choice_t *choice, gint hf_id, gint ett_id, gint *branch_taken)
{
    gboolean        imp_tag = FALSE;
    guint32         tag;
    proto_tree     *tree = parent_tree;
    proto_item     *item = NULL;
    int             end_offset, start_offset, count, length;
    int             hoffset = offset;
    gint            length_remaining;
    tvbuff_t       *next_tvb;
    gboolean        first_pass;
    header_field_info   *hfinfo;
    const mder_choice_t *ch;

#ifdef DEBUG_MDER_ANY
{
    const gchar *name;
    if (hf_id >= 0) {
        hfinfo = proto_registrar_get_nth(hf_id);
        name = hfinfo->name;
    }
    else {
        name = "unnamed";
    }
    if (tvb_length_remaining(tvb, offset) > 3) {
        printf("ANY dissect_mder_any(%s) entered offset:%d len:%d %02x:%02x:%02x\n", name, offset, tvb_length_remaining(tvb, offset), tvb_get_guint8(tvb, offset), tvb_get_guint8(tvb, offset + 1), tvb_get_guint8(tvb, offset + 2));
    }
    else {
        printf("ANY dissect_mder_any(%s) entered len:%d\n", name, tvb_length_remaining(tvb, offset));
    }
}
#endif

    start_offset = offset;

    if (tvb_length_remaining(tvb, offset) == 0) {
        item = proto_tree_add_string_format_value(
            parent_tree, hf_mder_error, tvb, offset, 0, "empty_any",
            "Empty any was found");
        expert_add_info(actx->pinfo, item, &ei_mder_empty_choice);
        return offset;
    }

    /* read header and length for choice field */
    get_last_mder_sq_element(&tag); 
    offset = get_mder_length(tvb, offset, &length);
    end_offset = offset + length;
    if (actx->private_data) { // dissect length only
        return end_offset;
    }
    
    /* loop over all entries until we find the right choice or
    run out of entries */
    ch = choice;
    first_pass = TRUE;
    while (ch->func || first_pass) {
        
        /* we reset for a second pass when we will look for choices */
        if (!ch->func) {
            first_pass = FALSE;
            ch = choice; /* reset to the beginning */
        }

    choice_try_again:
    
#ifdef DEBUG_MDER_ANY
    printf("ANY testing potential subdissector class[%p]: tag:%d:(expected)%d flags:%d\n", ch, tag, ch->tag, ch->flags);
#endif

        if ((first_pass
            && ((ch->tag == tag)
            || ((ch->tag == -1) && (ch->flags & MDER_FLAGS_NOOWNTAG))))
            || (!first_pass && ((ch->tag == -1))) /* we failed on the first pass so now try any choices */
            ) {

                /* create subtree */
                if (hf_id >= 0) {
                    hfinfo = proto_registrar_get_nth(hf_id);
                    if (parent_tree) {
                        hfinfo->type = FT_NONE;
                        item = proto_tree_add_item(parent_tree, hf_id, tvb, hoffset, length + 2, ENC_BIG_ENDIAN);
                        tree = proto_item_add_subtree(item, ett_id);
                        proto_tree_add_uint(tree, hf_mder_length, tvb, start_offset, 2, length);
                    }
                }

                start_offset = offset;
                hoffset = offset;

                length_remaining = tvb_length_remaining(tvb, hoffset);
                if (length_remaining > length)
                    length_remaining = length;

#ifdef DEBUG_MDER_ANY
{
    printf("ANY tvb_new_subset(tvb, %d, %d, %d)\n", hoffset, length_remaining, length);
}
#endif

                next_tvb = tvb_new_subset(tvb, hoffset, length_remaining, length);


#ifdef DEBUG_MDER_ANY
{
const gchar *name;
if (hf_id >= 0) {
    hfinfo = proto_registrar_get_nth(hf_id);
    name = hfinfo->name;
}
else {
    name = "unnamed";
}
if (tvb_length_remaining(next_tvb, 0) > 3) {
    printf("ANY dissect_mder_any(%s) calling subdissector start_offset:%d offset:%d len:%d %02x:%02x:%02x\n", name, start_offset, offset, tvb_length_remaining(next_tvb, 0), tvb_get_guint8(next_tvb, 0), tvb_get_guint8(next_tvb, 1), tvb_get_guint8(next_tvb, 2));
}
else {
    printf("ANY dissect_mder_any(%s) calling subdissector len:%d\n", name, tvb_length(next_tvb));
}
}
#endif

                if (next_tvb == NULL) {

#ifdef DEBUG_MDER_ANY
{
    printf("ANY next_tvb is null\n");
}
#endif

                    /* Assume that we have a malformed packet. */
                    THROW(ReportedBoundsError);
                }
                imp_tag = FALSE;
                if ((ch->flags & MDER_FLAGS_IMPLTAG))
                    imp_tag = TRUE;
                count = ch->func(imp_tag, next_tvb, 0, actx, tree, *ch->p_id);
                
#ifdef DEBUG_MDER_ANY
{
const gchar *name;
if (hf_id >= 0) {
    hfinfo = proto_registrar_get_nth(hf_id);
    name = hfinfo->name;
}
else {
    name = "unnamed";
}
printf("ANY dissect_mder_any(%s) subdissector ate %d bytes\n", name, count);
}
#endif

                if ((count == 0) && (((ch->tag == -1) && (ch->flags & MDER_FLAGS_NOOWNTAG)) || !first_pass)) {
                    /* wrong one, break and try again */
                    ch++;
                    
#ifdef DEBUG_MDER_ANY
{
const gchar *name;
if (hf_id >= 0) {
    hfinfo = proto_registrar_get_nth(hf_id);
    name = hfinfo->name;
}
else {
    name = "unnamed";
}
printf("ANY dissect_mder_any(%s) trying again\n", name);
}
#endif

                    goto choice_try_again;
                }
                return end_offset;
        }
        ch++;
    }
    
    return end_offset;
}

/* F.4.6 Encoding of a sequence of */

#ifdef DEBUG_MDER
#define DEBUG_MDER_SQ_OF
#endif

static int
dissect_mder_sq_of(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, gint32 min_len, gint32 max_len, const mder_sequence_t *seq, gint hf_id, gint ett_id) {
    guint32     lenx, cnt, size;
    gint32              length_remaining;
    
    proto_tree         *tree     = parent_tree;
    proto_item         *item     = NULL;
    proto_item         *causex;
    int                 hoffset, end_offset;
    header_field_info  *hfi;
    tvbuff_t           *next_tvb;

#ifdef DEBUG_MDER_SQ_OF
{
const gchar *name;
header_field_info *hfinfo;
if (hf_id >= 0) {
hfinfo = proto_registrar_get_nth(hf_id);
name = hfinfo->name;
} else {
name = "unnamed";
}
if (tvb_length_remaining(tvb,offset) > 3) {
printf("SQ OF dissect_mder_sq_of(%s) entered implicit_tag:%d offset:%d len:%d %02x:%02x:%02x\n", name, implicit_tag, offset, tvb_length_remaining(tvb, offset), tvb_get_guint8(tvb, offset), tvb_get_guint8(tvb, offset+1), tvb_get_guint8(tvb, offset+2));
} else {
printf("SQ OF dissect_mder_sq_of(%s) entered\n", name);
}
}
#endif

    hoffset = offset;
    /* first we must read the sequence header */
    offset = dissect_mder_count(tree, tvb, offset, &cnt);
    offset = dissect_mder_length(tree, tvb, offset, &lenx);
    end_offset = offset + lenx;

    /* create subtree */
    if (hf_id >= 0) {
        hfi = proto_registrar_get_nth(hf_id);
        if (parent_tree) {
            if (hfi->type == FT_NONE) {
                item = proto_tree_add_item(parent_tree, hf_id, tvb, hoffset, lenx + offset - hoffset, ENC_BIG_ENDIAN);
                proto_item_append_text(item, ":");
            } else {
                item = proto_tree_add_uint(parent_tree, hf_id, tvb, hoffset, lenx + offset - hoffset, cnt);
                proto_item_append_text(item, plurality(cnt, " item", " items"));
            }
            tree = proto_item_add_subtree(item, ett_id);
            proto_tree_add_uint(tree, hf_mder_count, tvb, hoffset, 2, cnt);
            proto_tree_add_uint(tree, hf_mder_length, tvb, hoffset + 2, 2, lenx);
        }
    }

    /* loop over all entries until we reach the end of the sequence */
    while (offset < end_offset) {
        gboolean    imp_tag;
        
        length_remaining = tvb_length_remaining(tvb, offset);
        if (length_remaining >(end_offset - offset))
            length_remaining = end_offset - offset;
        next_tvb = tvb_new_subset(tvb, offset, length_remaining, length_remaining);

        imp_tag = FALSE;
        if (seq->flags == MDER_FLAGS_IMPLTAG)
            imp_tag = TRUE;
        /* call the dissector for this field */
        size = seq->func(imp_tag, next_tvb, 0, actx, tree, *seq->p_id);
        offset += size;
    }

    /* if we didnt end up at exactly offset, then we ate too many bytes */
    if (offset != end_offset) {
        tvb_ensure_bytes_exist(tvb, offset-2, 2);
        causex = proto_tree_add_string_format_value(
            tree, hf_mder_error, tvb, offset-2, 2, "illegal_length",
            "Sequence Of ate %d too many bytes",
            offset - end_offset);
        expert_add_info_format(actx->pinfo, causex, &ei_mder_error_length,
            "MDER Error:too many byte in Sequence");
    }

    return end_offset;
}


int
dissect_mder_sequence_of(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const mder_sequence_t *seq, gint hf_id, gint ett_id) {
    return dissect_mder_sq_of(implicit_tag, actx, parent_tree, tvb, offset, NO_BOUND, NO_BOUND, seq, hf_id, ett_id);
}

/* F.4.3 Encoding of a bitstring value */
int
dissect_mder_bitstring(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb)
{
    guint8  length;
    length = tvb_length_remaining(tvb, offset);
    if (length == BIT_8 || length == BIT_16 || length == BIT_32) {
        offset = dissect_mder_bitstring_size(implicit_tag, actx, parent_tree, tvb, offset, named_bits, hf_id, ett_id, out_tvb, (Size)length);
    } else {
        offset = dissect_mder_bitstring32(implicit_tag, actx, parent_tree, tvb, offset, named_bits, hf_id, ett_id, out_tvb);
    }
    
    return offset;
}

int
dissect_mder_bitstring_size(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb, Size size)
{
    tvbuff_t            *tmp_tvb = NULL;
    const gchar         *sep;
    gboolean             term;
    const asn_namedbit  *nb;
    guint64              value;
    guint32              len = (guint32)size, byteno;
    guint8               b0, b1, val, *bitstring;
    int                  end_offset;
    proto_item          *item = NULL;
    proto_tree          *tree = NULL;

    end_offset = offset + len;

    actx->created_item = NULL;

    if (hf_id >= 0) {
        item = proto_tree_add_item(parent_tree, hf_id, tvb, offset, len, ENC_BIG_ENDIAN);
        actx->created_item = item;
        if (ett_id != -1) {
            tree = proto_item_add_subtree(item, ett_id);
        }
    }
    dissect_mder_integer64(implicit_tag, actx, tree, tvb, offset, -1, &value, size);

    if (out_tvb) {
        if (len <= (guint32)tvb_length_remaining(tvb, offset)) {
            *out_tvb = tvb_new_subset(tvb, offset, len, len);
        }
        else {
            *out_tvb = tvb_new_subset_remaining(tvb, offset);
        }
    }


    if (named_bits) {
        sep = " (";
        term = FALSE;
        nb = named_bits;
        bitstring = tvb_get_string(wmem_packet_scope(), tvb, offset, len);

        while (nb->p_id) {
            if ((len > 0) && (nb->bit < (8 * len))) {
                val = tvb_get_guint8(tvb, offset + nb->bit / 8);
                bitstring[(nb->bit / 8)] &= ~(0x80 >> (nb->bit % 8));
                val &= 0x80 >> (nb->bit % 8);
                b0 = (nb->gb0 == -1) ? nb->bit / 8 :
                    ((guint32)nb->gb0) / 8;
                b1 = (nb->gb1 == -1) ? nb->bit / 8 :
                    ((guint32)nb->gb1) / 8;
                proto_tree_add_item(tree, *(nb->p_id), tvb, offset + b0, b1 - b0 + 1, ENC_BIG_ENDIAN);
            }
            else {  /* 8.6.2.4 */
                val = 0;
                proto_tree_add_boolean(tree, *(nb->p_id), tvb, offset + len, 0, 0x00);
            }
            if (val) {
                if (item && nb->tstr) {
                    proto_item_append_text(item, "%s%s", sep, nb->tstr);
                    sep = ", ";
                    term = TRUE;
                }
            }
            else {
                if (item && nb->fstr) {
                    proto_item_append_text(item, "%s%s", sep, nb->fstr);
                    sep = ", ";
                    term = TRUE;
                }
            }
            nb++;
        }
        if (term)
            proto_item_append_text(item, ")");

        for (byteno = 0; byteno < len; byteno++) {
            if (bitstring[byteno]) {
                expert_add_info_format(
                    actx->pinfo, item, &ei_mder_bits_unknown,
                    "Unknown bit(s): 0x%s", bytes_to_ep_str(bitstring, len));
                break;
            }
        }
    }
    
    return end_offset;
}

int
dissect_mder_bitstring32(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb)
{
    return dissect_mder_bitstring_size(implicit_tag, actx, parent_tree, tvb, offset, named_bits, hf_id, ett_id, out_tvb, BIT_32);
}

int
dissect_mder_bitstring16(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb)
{
    return dissect_mder_bitstring_size(implicit_tag, actx, parent_tree, tvb, offset, named_bits, hf_id, ett_id, out_tvb, BIT_16);
}

int
dissect_mder_bitstring8(gboolean implicit_tag, asn1_ctx_t *actx, proto_tree *parent_tree, tvbuff_t *tvb, int offset, const asn_namedbit *named_bits, gint hf_id, gint ett_id, tvbuff_t **out_tvb)
{
    return dissect_mder_bitstring_size(implicit_tag, actx, parent_tree, tvb, offset, named_bits, hf_id, ett_id, out_tvb, BIT_8);
}

/*
 *  8.18    Encoding of a value of the external type
 *  8.18.1  The encoding of a value of the external type shall be the MDER encoding of the following
 *          sequence type, assumed to be defined in an environment of EXPLICIT TAGS,
 *          with a value as specified in the subclauses below:
 *
 *  [UNIVERSAL 8] IMPLICIT SEQUENCE {
 *      direct-reference            OBJECT IDENTIFIER OPTIONAL,
 *      indirect-reference      INTEGER OPTIONAL,
 *      data-value-descriptor       ObjectDescriptor OPTIONAL,
 *      encoding                CHOICE {
 *      single-ASN1-type                [0] ABSTRACT-SYNTAX.&Type,
 *      octet-aligned                   [1] IMPLICIT OCTEdissect_mder_integerT STRING,
 *      arbitrary                       [2] IMPLICIT BIT STRING } }
 *
 */

static int
dissect_mder_INTEGER(gboolean implicit_tag, tvbuff_t *tvb, int offset, asn1_ctx_t *actx, proto_tree *tree, int hf_index) {
    offset = dissect_mder_integer(implicit_tag, actx, tree, tvb, offset, hf_index, NULL);
    return offset;
}

static int
dissect_mder_T_single_ASN1_type(gboolean implicit_tag _U_, tvbuff_t *tvb _U_, int offset _U_, asn1_ctx_t *actx _U_, proto_tree *tree _U_, int hf_index _U_)
{
    if (actx->external.u.mder.mder_callback) {
        offset = actx->external.u.mder.mder_callback(FALSE, tvb, offset, actx, tree, hf_index);
    } else {
        offset = call_mder_oid_callback(actx->external.direct_reference, tvb, offset, actx->pinfo, tree);
    }

    return offset;
}

static void
dissect_mder(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    const gchar *name;

    col_set_str(pinfo->cinfo, COL_PROTOCOL, PSNAME);

    col_set_str(pinfo->cinfo, COL_DEF_SRC, "MDER encoded file");

    (void) call_mder_syntax_callback(decode_as_syntax, tvb, 0, pinfo, tree);

    /* see if we have a better name */
    name = get_mder_oid_syntax(decode_as_syntax);
    col_add_fstr(pinfo->cinfo, COL_INFO, "Decoded as %s", name ? name : decode_as_syntax);
}

void
proto_register_mder(void)
{
    static hf_register_info hf[] = {
        { &hf_mder_id_class, {
                "Class", "mder.id.class", FT_UINT8, BASE_DEC,
                VALS(mder_class_codes), 0xc0, "Class of MDER TLV Identifier", HFILL }},
        { &hf_mder_bitstring_padding, {
                "Padding", "mder.bitstring.padding", FT_UINT8, BASE_DEC,
                NULL, 0x0, "Nummder of unused bits in the last octet of the bitstring", HFILL }},
        { &hf_mder_bitstring_empty, {
                "Empty", "mder.bitstring.empty", FT_UINT8, BASE_DEC,
                NULL, 0x0, "This is an empty bitstring", HFILL }},
        { &hf_mder_id_pc, {
                "P/C", "mder.id.pc", FT_BOOLEAN, 8,
                TFS(&mder_pc_codes), 0x20, "Primitive or Constructed MDER encoding", HFILL }},
        { &hf_mder_id_uni_tag, {
                "Tag", "mder.id.uni_tag", FT_UINT8, BASE_DEC|BASE_EXT_STRING,
                &mder_uni_tag_codes_ext, 0x1f, "Universal tag type", HFILL }},
        { &hf_mder_id_uni_tag_ext, {
                "Tag", "mder.id.uni_tag", FT_UINT32, BASE_DEC,
                NULL, 0, "Universal tag type", HFILL }},
         { &hf_mder_id_tag, {
                "Tag", "mder.id.tag", FT_UINT8, BASE_DEC,
                NULL, 0x1f, "Tag value for non-Universal classes", HFILL }},
        { &hf_mder_id_tag_ext, {
                "Tag", "mder.id.tag", FT_UINT32, BASE_DEC,
                NULL, 0, "Tag value for non-Universal classes", HFILL }},
        { &hf_mder_length, {
                "Length", "mder.length", FT_UINT32, BASE_DEC,
                NULL, 0, "Length of contents", HFILL } },
        { &hf_mder_string_length, {
                "Length", "mder.string.length", FT_UINT32, BASE_DEC,
                NULL, 0, "Length of string", HFILL } }, 
        { &hf_mder_string_value, {
                "Value", "mder.string.value", FT_BYTES, BASE_NONE,
                NULL, 0, "Value of the string", HFILL } }, 
        { &hf_mder_count, {
                "Count", "mder.count", FT_UINT32, BASE_DEC,
                NULL, 0, "Number of Elements", HFILL } },
        { &hf_mder_unknown_OCTETSTRING, {
                "OCTETSTRING", "mder.unknown.OCTETSTRING", FT_BYTES, BASE_NONE,
                NULL, 0, "This is an unknown OCTETSTRING", HFILL }},
        { &hf_mder_unknown_MDER_OCTETSTRING, {
                "OCTETSTRING [MDER encoded]", "mder.unknown.OCTETSTRING", FT_NONE, BASE_NONE,
                NULL, 0, "This is an MDER encoded OCTETSTRING", HFILL }},
        { &hf_mder_unknown_BITSTRING, {
                "BITSTRING", "mder.unknown.BITSTRING", FT_BYTES, BASE_NONE,
                NULL, 0, "This is an unknown BITSTRING", HFILL }},
        { &hf_mder_error, {
                "MDER Error", "mder.error", FT_STRING, BASE_NONE,
                NULL, 0, NULL, HFILL }},
        { &hf_mder_direct_reference,
          { "direct-reference", "mder.direct_reference",
            FT_OID, BASE_NONE, NULL, 0,
            "mder.OBJECT_IDENTIFIER", HFILL }},
        { &hf_mder_indirect_reference,
          { "indirect-reference", "mder.indirect_reference",
            FT_INT32, BASE_DEC, NULL, 0,
            "mder.INTEGER", HFILL }},
        { &hf_mder_data_value_descriptor,
          { "data-value-descriptor", "mder.data_value_descriptor",
            FT_STRING, BASE_NONE, NULL, 0,
            "mder.ObjectDescriptor", HFILL }},
        { &hf_mder_octet_aligned,
          { "octet-aligned", "mder.octet_aligned",
            FT_BYTES, BASE_NONE, NULL, 0,
            "mder.T_octet_aligned", HFILL }},
        { &hf_mder_arbitrary,
          { "arbitrary", "mder.arbitrary",
            FT_BYTES, BASE_NONE, NULL, 0,
            "mder.T_arbitrary", HFILL }},
        { &hf_mder_single_ASN1_type,
          { "single-ASN1-type", "mder.single_ASN1_type",
            FT_NONE, BASE_NONE, NULL, 0,
            "mder.T_single_ASN1_type", HFILL }},

        /* Fragment entries */
        { &hf_mder_fragments,
          { "OCTET STRING fragments", "mder.octet_string.fragments", FT_NONE, BASE_NONE,
            NULL, 0x00, NULL, HFILL } },
        { &hf_mder_fragment,
          { "OCTET STRING fragment", "mder.octet_string.fragment", FT_FRAMENUM, BASE_NONE,
            NULL, 0x00, NULL, HFILL } },
        { &hf_mder_fragment_overlap,
          { "OCTET STRING fragment overlap", "mder.octet_string.fragment.overlap", FT_BOOLEAN,
            BASE_NONE, NULL, 0x0, NULL, HFILL } },
        { &hf_mder_fragment_overlap_conflicts,
          { "OCTET STRING fragment overlapping with conflicting data",
            "mder.octet_string.fragment.overlap.conflicts", FT_BOOLEAN, BASE_NONE, NULL,
            0x0, NULL, HFILL } },
        { &hf_mder_fragment_multiple_tails,
          { "OCTET STRING has multiple tail fragments",
            "mder.octet_string.fragment.multiple_tails", FT_BOOLEAN, BASE_NONE,
            NULL, 0x0, NULL, HFILL } },
        { &hf_mder_fragment_too_long_fragment,
          { "OCTET STRING fragment too long", "mder.octet_string.fragment.too_long_fragment",
            FT_BOOLEAN, BASE_NONE, NULL, 0x0, NULL,
            HFILL } },
        { &hf_mder_fragment_error,
          { "OCTET STRING defragmentation error", "mder.octet_string.fragment.error", FT_FRAMENUM,
            BASE_NONE, NULL, 0x00, NULL, HFILL } },
        { &hf_mder_fragment_count,
          { "OCTET STRING fragment count", "mder.octet_string.fragment.count", FT_UINT32, BASE_DEC,
            NULL, 0x00, NULL, HFILL } },
        { &hf_mder_reassembled_in,
          { "Reassembled in", "mder.octet_string.reassembled.in", FT_FRAMENUM, BASE_NONE,
            NULL, 0x00, NULL, HFILL } },
        { &hf_mder_reassembled_length,
          { "Reassembled OCTET STRING length", "mder.octet_string.reassembled.length", FT_UINT32, BASE_DEC,
            NULL, 0x00, NULL, HFILL } }
    };


    static gint *ett[] = {
        &ett_mder_octet_string,
        &ett_mder_reassembled_octet_string,
        &ett_mder_primitive,
        &ett_mder_unknown,
        &ett_mder_SEQUENCE,
        &ett_mder_EXTERNAL,
        &ett_mder_T_encoding,
        &ett_mder_fragment,
        &ett_mder_fragments
    };
    static ei_register_info ei[] = {
        { &ei_mder_size_constraint_string, { "mder.size_constraint.string", PI_PROTOCOL, PI_WARN, "Size constraint: string", EXPFILL }},
        { &ei_mder_size_constraint_value, { "mder.size_constraint.value", PI_PROTOCOL, PI_WARN, "Size constraint: values", EXPFILL }},
        { &ei_mder_size_constraint_items, { "mder.size_constraint.items", PI_PROTOCOL, PI_WARN, "Size constraint: items", EXPFILL }},
        { &ei_mder_sequence_field_wrong, { "mder.error.sequence.field_wrong", PI_MALFORMED, PI_WARN, "MDER Error: Wrong field in SEQUENCE", EXPFILL }},
        { &ei_mder_expected_octet_string, { "mder.error.expected.octet_string", PI_MALFORMED, PI_WARN, "MDER Error: OctetString expected", EXPFILL }},
        { &ei_mder_expected_null, { "mder.error.expected.null", PI_MALFORMED, PI_WARN, "MDER Error: NULL expected", EXPFILL }},
        { &ei_mder_expected_null_zero_length, { "mder.error.expected.null_zero_length", PI_MALFORMED, PI_WARN, "MDER Error: NULL expect zero length", EXPFILL }},
        { &ei_mder_expected_sequence, { "mder.error.expected.sequence", PI_MALFORMED, PI_WARN, "MDER Error: Sequence expected", EXPFILL }},
        { &ei_mder_expected_set, { "mder.error.expected.set", PI_MALFORMED, PI_WARN, "MDER Error: SET expected", EXPFILL }},
        { &ei_mder_expected_string, { "mder.error.expected.string", PI_MALFORMED, PI_WARN, "MDER Error: String expected", EXPFILL }},
        { &ei_mder_expected_object_identifier, { "mder.error.expected.object_identifier", PI_MALFORMED, PI_WARN, "MDER Error: Object Identifier expected", EXPFILL }},
        { &ei_mder_expected_generalized_time, { "mder.error.expected.generalized_time", PI_MALFORMED, PI_WARN, "MDER Error: GeneralizedTime expected", EXPFILL }},
        { &ei_mder_expected_utc_time, { "mder.error.expected.utc_time", PI_MALFORMED, PI_WARN, "MDER Error: UTCTime expected", EXPFILL }},
        { &ei_mder_expected_bitstring, { "mder.error.expected.bitstring", PI_MALFORMED, PI_WARN, "MDER Error: BitString expected", EXPFILL }},
        { &ei_mder_error_length, { "mder.error.length", PI_MALFORMED, PI_WARN, "MDER Error length", EXPFILL }},
        { &ei_mder_wrong_tag_in_tagged_type, { "mder.error.wrong_tag_in_tagged_type", PI_MALFORMED, PI_WARN, "MDER Error: Wrong tag in tagged type", EXPFILL }},
        { &ei_mder_universal_tag_unknown, { "mder.error.universal_tag_unknown", PI_MALFORMED, PI_WARN, "MDER Error: can not handle universal", EXPFILL }},
        { &ei_mder_no_oid, { "mder.error.no_oid", PI_MALFORMED, PI_WARN, "MDER Error: No OID supplied to call_mder_oid_callback", EXPFILL }},
        { &ei_mder_oid_not_implemented, { "mder.error.oid_not_implemented", PI_UNDECODED, PI_WARN, "MDER: Dissector for OID not implemented. Contact Wireshark developers if you want this supported", EXPFILL }},
        { &ei_mder_syntax_not_implemented, { "mder.error.syntax_not_implemented", PI_UNDECODED, PI_WARN, "MDER: Syntax not implemented", EXPFILL }},
        { &ei_mder_value_too_many_bytes, { "mder.error.value_too_many_bytes", PI_MALFORMED, PI_WARN, "Value is encoded with too many bytes", EXPFILL }},
        { &ei_mder_unknown_field_sequence, { "mder.error.unknown_field.sequence", PI_MALFORMED, PI_WARN, "MDER Error: Unknown field in Sequence", EXPFILL }},
        { &ei_mder_unknown_field_set, { "mder.error.unknown_field.set", PI_MALFORMED, PI_WARN, "MDER Error: Unknown field in SET", EXPFILL }},
        { &ei_mder_missing_field_set, { "mder.error.missing_field.set", PI_MALFORMED, PI_WARN, "MDER Error: Missing field in SET", EXPFILL }},
        { &ei_mder_empty_choice, { "mder.error.empty_choice", PI_MALFORMED, PI_WARN, "MDER Error: Empty choice was found", EXPFILL }},
        { &ei_mder_choice_not_found, { "mder.error.choice_not_found", PI_MALFORMED, PI_WARN, "MDER Error: This choice field was not found", EXPFILL }},
        { &ei_mder_bits_unknown, { "mder.error.bits_unknown", PI_UNDECODED, PI_WARN, "MDER Error: Bits unknown", EXPFILL }},
        { &ei_mder_bits_set_padded, { "mder.error.bits_set_padded", PI_UNDECODED, PI_WARN, "MDER Error: Bits set in padded area", EXPFILL }},
        { &ei_mder_illegal_padding, { "mder.error.illegal_padding", PI_UNDECODED, PI_WARN, "Illegal padding", EXPFILL }},
        { &ei_mder_invalid_format_generalized_time, { "mder.error.invalid_format.generalized_time", PI_MALFORMED, PI_WARN, "MDER Error: GeneralizedTime invalid format", EXPFILL }},
        { &ei_mder_invalid_format_utctime, { "mder.error.invalid_format.utctime", PI_MALFORMED, PI_WARN, "MDER Error: malformed UTCTime encoding", EXPFILL }},
    };

    module_t *mder_module;
    expert_module_t* expert_mder;
    uat_t* users_uat = uat_new("OID Tables",
                               sizeof(oid_user_t),
                               "oid",
                               FALSE,
                               (void**) &oid_users,
                               &num_oid_users,
                               UAT_AFFECTS_DISSECTION, /* affects dissection of packets, but not set of named fields */
                               "ChObjectIdentifiers",
                               oid_copy_cb,
                               NULL,
                               oid_free_cb,
                               mder_update_oids,
                               users_flds);

    proto_mder = proto_register_protocol(PNAME, PSNAME, PFNAME);

    mder_handle = register_dissector(PFNAME, dissect_mder, proto_mder);

    proto_register_field_array(proto_mder, hf, array_length(hf));
    proto_register_subtree_array(ett, array_length(ett));
    expert_mder = expert_register_protocol(proto_mder);
    expert_register_field_array(expert_mder, ei, array_length(ei));

    proto_set_cant_toggle(proto_mder);

    /* Register preferences */
    mder_module = prefs_register_protocol(proto_mder, NULL);

    prefs_register_bool_preference(mder_module, "show_internals",
                                   "Show internal MDER encapsulation tokens",
                                   "Whether the dissector should also display internal"
                                   " ASN.1 MDER details such as Identifier and Length fields", &show_internal_mder_fields);
                                   
    prefs_register_bool_preference(mder_module, "decode_octetstring",
                                   "Decode OCTET STRING as MDER encoded data",
                                   "Whether the dissector should try decoding OCTET STRINGs as"
                                   " constructed ASN.1 MDER encoded data", &decode_octetstring_as_mder);

    prefs_register_uat_preference(mder_module, "oid_table", "Object Identifiers",
                                  "A table that provides names for object identifiers"
                                  " and the syntax of any associated values",
                                  users_uat);

    mder_oid_dissector_table = register_dissector_table("mder.oid", "MDER OID Dissectors", FT_STRING, BASE_NONE);
    mder_syntax_dissector_table = register_dissector_table("mder.syntax", "MDER Syntax Dissectors", FT_STRING, BASE_NONE);
    syntax_table = g_hash_table_new(g_str_hash, g_str_equal); /* oid to syntax */
    
}

void
proto_reg_handoff_mder(void)
{
    guint i = 1;
    
    oid_add_from_string("asn1", "2.1");
    oid_add_from_string("medical device encoding", "1.2.840.10004.1");
    // iso(1) member-body(2) US(840) ieee1073(10004) mddl(1)

    dissector_add_uint("wtap_encap", WTAP_ENCAP_MDER, mder_handle);

    mder_decode_as_foreach(mder_add_syntax_name, &i);

    if (i > 1)
        qsort(&syntax_names[1], i - 1, sizeof(value_string), cmp_value_string);
    syntax_names[i].value = 0;
    syntax_names[i].strptr = NULL;

    /* allow the dissection of MDER/DER carried over a TCP transport
       by using "Decode As..." */
    dissector_add_handle("tcp.port", mder_handle);

    mder_update_oids();
}
/*
 * Editor modelines  -  http://www.wireshark.org/tools/modelines.html
 *
 * Local variables:
 * c-basic-offset: 4
 * tab-width: 4
 * indent-tabs-mode: nil
 * End:
 *
 * vi: set shiftwidth=4 tabstop=4 expandtab:
 * :indentSize=4:tabSize=4:noTabs=true:
 */
